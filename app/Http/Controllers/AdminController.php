<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Aggregate;
use App\Models\ChartOfAccount;
use DB;

class AdminController extends Controller
{



    public function balanceSheetYearly(Request $request) {
    	$maxYear = $year = date('Y');
		if ($request->has('year')) {
			$year = $request->get('year');
		}

		$minYear = DB::table('gl_trans')->min(DB::raw('year(transdate)'));

		$agg = new Aggregate();
		list($coa, $agg) = $agg->getYearlyBalanceSheetData($year);

		return view('admin.balancesheetyearly', compact('coa', 'agg', 'year', 'minYear', 'maxYear'));
	}
	
	public function balanceSheetMonthly(Request $request) {
		$month = date('m');
		if ($request->has('month')) {
			$month = $request->get('month');
		}

		$year = date('Y');
		if ($request->has('year')) {
			$year = $request->get('year');
		}

		$agg = new Aggregate();
		$bb = $agg->getMonthlyBalanceSheetData($month, $year);
		$coa = ChartOfAccount::all();
		return view('admin.balancesheetmonthly', compact('bb', 'coa', 'month', 'year'));
	}
	
	public function incomeStatementYearly(Request $request) {
		$maxYear = $year = date('Y');
		if ($request->has('year')) {
			$year = $request->get('year');
		}

		$minYear = DB::table('gl_trans')->min(DB::raw('year(transdate)'));

		$agg = new Aggregate();
//		list($coa, $agg) = $agg->getYearlyBalanceSheetData($year);
		return view('admin.incomestatementyearly', compact('coa', 'agg', 'year', 'minYear', 'maxYear')	);
	}
	
	public function incomeStatementMonthly(Request $request) {
		$month = date('m');
		if ($request->has('month')) {
			$month = $request->get('month');
		}

		$maxYear = $year = date('Y');
		if ($request->has('year')) {
			$year = $request->get('year');
		}
		$minYear = DB::table('gl_trans')->min(DB::raw('year(transdate)'));
		$agg = new Aggregate();
		$bb = $agg->getMonthlyBalanceSheetData($month, $year);
		$coa = ChartOfAccount::all();
		return view('admin.incomestatementmonthly', compact('bb', 'coa', 'month', 'year', 'minYear', 'maxYear'));
	}
}
