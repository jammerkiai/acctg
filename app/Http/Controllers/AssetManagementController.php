<?php

namespace App\Http\Controllers;

use App\InvoiceItem;
use App\Models\AssetDepreciation;
use App\Models\AssetDepreciationSchedule;
use App\Models\ChartOfAccount;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AssetManagementController extends Controller
{
    public function index(Request $request) {
        $assets=[];
        if ($request->has('code')) {
            $code = $request->get('code');
            $assets = AssetDepreciation::where('code', [$code])->paginate(30);
        }

        $coa = ChartOfAccount::where('subclass', ['PROPERTY, PLANT AND EQUIPMENT'])->get();
        return view('settings.assets.index', compact('coa', 'assets'));
    }

    public function create($id = null) {
        $itm = [];

        if (!is_null($id)) {
            $itm = InvoiceItem::find($id)->first();
        }

        $coa = ChartOfAccount::where('subclass', ['PROPERTY, PLANT AND EQUIPMENT'])->get();
        return view('settings.assets.add', compact('coa', 'itm'));
    }

    public function store(Request $request) {

        $input = $request->except(['_token', '_url']);

        if ($request->has('id')) {
            //update
            AssetDepreciation::find($input['id'])->update($input);
        } else {
            $am = AssetDepreciation::create($input);
            $am->save();

            $this->processSchedule($input, $am->id);
        }

        return redirect()->to('assetmanagement?code=' .  $request->code)->with('message', 'Record Saved.');
    }

    public function view($id) {
        $am = AssetDepreciation::find($id);
        $coa = ChartOfAccount::where('subclass', ['PROPERTY, PLANT AND EQUIPMENT'])->get();
        return view('settings.assets.view', compact('am', 'coa'));
    }

    protected function processSchedule($in, $ad_id) {

        $months = $in['useful_life'];
        if ($in['unit_interval'] === 'year') {
            $months = $months * 12;
        }

        $schedule = [];
        $start = Carbon::createFromFormat('Y-m-d', $in['start_depreciation']);

        $remainingValue = $in['purchase_amount'] - $in['monthly_depreciation'];

        $schedule = [
            'ad_id' => $ad_id,
            'interval_count' => 1,
            'entry_date' => $start->format('Y-m-d'),
            'remaining_value' => $remainingValue
        ];
        AssetDepreciationSchedule::create($schedule)->save();


        for ($i = 1; $i < $months; $i++) {
            $start->addMonth(1);
            $remainingValue = $remainingValue - $in['monthly_depreciation'];
            $schedule = [
                'ad_id' => $ad_id,
                'interval_count' => $i + 1,
                'entry_date' => $start->format('Y-m-d'),
                'remaining_value' => $remainingValue
            ];
            AssetDepreciationSchedule::create($schedule)->save();
        }

        return true;
    }
}
