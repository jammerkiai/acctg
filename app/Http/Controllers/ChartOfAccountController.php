<?php
/**
 * Created by PhpStorm.
 * User: macbookair
 * Date: 2/28/16
 * Time: 7:23 PM
 */

namespace App\Http\Controllers;


use App\Models\ChartOfAccount;
use Illuminate\Http\Request;

class ChartOfAccountController extends Controller {

    public function chartOfAccounts(Request $request) {

        $term = strtoupper($request->get('term'));
        $coa = ChartOfAccount::where('name', 'like', "%$term%")->pluck('name');
        return response()->json($coa);
    }

    public function index() {
    	$coa = ChartOfAccount::orderBy('code')->get();

    	return view('settings.coa.index', compact('coa'));
    }

    public function create() {
    	return view('settings.coa.create');
    }

    public function store(Request $request) {
    	ChartOfAccount::firstOrCreate($request->except(['_url', '_token']));
    	return redirect('coa')->with('message', 'New account created.');
    }

    public function show($id) {
    	$coa = ChartOfAccount::find($id)->get();
    	return view('settings.coa.edit', compact('coa'));
    }
}