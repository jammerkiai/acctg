<?php

namespace App\Http\Controllers;

use App\InvoiceItem;
use App\Models\ChartOfAccount;
use App\Models\CheckVoucher;
use App\Models\GlTrans;
use App\OfficialReceipt;
use App\OfficialReceiptItem;
use App\PurchaseItem;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\Supplier;
use App\Models\Settings;
use DB;

class DisbursementController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index() {

        $invoices = Invoice::where('type', '!=', 'others')->where('check_released', [0])->paginate(50);
        $ors = Invoice::where('type','others')->where('check_released', [0])->paginate(50);
    	return view('disbursement.index', compact('invoices', 'ors'));
    }

    public function disbursementDetails($invoice) {
        $coa = ChartOfAccount::all();
        $ii = InvoiceItem::with('purchaseItem')->where('invoice_id', $invoice)->get();
        $invoice = Invoice::find($invoice);

        if ($invoice->wht > 0) {
            $bank = 'BDO';
        } else {
            $bank = 'EWB';
        }

        $settings = Settings::all();
        $ctr = [];
        foreach($settings as $set) {
            $ctr[$set->name] = number_format($set->counter, 0, '', '');
            if ($set->name == $bank) {
                $check_number = number_format($set->counter, 0, '', '');
            }
        }

        return view('disbursement.details', compact('coa', 'ii', 'invoice', 'ctr', 'check_number'));
    }

    public function disbursementForm() {

        $suppliers = Supplier::all();
        return view('disbursement.form', compact('suppliers'));
    }

    public function addInventory(Requests\AddInventoryRequest $data) {

        $inputs = $data->request->all();
        $items = json_decode($inputs['items'], true);
        $invoice_id = 0;

        foreach($items as $key => $value) {
            $glt = new GlTrans;
            $glt->transdate = date('Y-m-d');
            $glt->ref_id = $value['invoice_id'];
            $glt->src = 'disbursement';
            $glt->code = $inputs['entry'][$key];
            $glt->credit = 0.00;
            $glt->debit = $value['net'];
            $glt->amount = $value['net'];
            $glt->save();

            $invoice_id = $value['invoice_id'];

            $invItm = InvoiceItem::where('invoice_id', [$invoice_id])
                                    ->where('purchase_item_id', $value['purchase_item_id'])
                                    ->update(['code' => $inputs['entry'][$key], 'posted' => 1]);

        }

        $inv = Invoice::find($invoice_id);
        if ($inv) {
            $inv->posted = 1;
            $inv->save();
        }

        return redirect('disbursement/'.$invoice_id.'/details');

    }

    public function addToPayable(Request $request) {
        $inputs = $request->all();
        $particulars = json_decode($inputs['particulars'], true);
        $or_id = 0;

        $wht = 0;
        foreach($particulars as $key => $particular) {
            $ori = InvoiceItem::find($particular['id']);
            $ori->code = $inputs['entry'][$key];
            $ori->wht = $inputs['whtValue'][$key];
            $ori->vat = $inputs['vatValue'][$key];
            $ori->net = $inputs['netValue'][$key];
            $ori->wht_percent = $inputs['whtPercent'];
            $ori->vat_percent = $inputs['vatPercent'];
            $ori->posted = 1;

            if ($ori->wht_percent > 0 and $wht == 0) {
                $wht = 1;
            }

            
            if(isset($inputs['vatPercent']) && $inputs['vatPercent'] > 0) {
                $ori->is_vatable = 1;
            }
            else {
                $ori->is_vatable = 0;
            }

            $ori->save();
            $or_id = $particular['invoice_id'];
            $or = Invoice::find($or_id);

            $glt = new GlTrans;
            $glt->transdate = date('Y-m-d');
            $glt->ref_id = $or->or_number;
            $glt->src = 'disbmt_ors';
            $glt->code = $inputs['entry'][$key];
            $glt->debit = 0.00; // to be confirm
            $glt->credit = 0.00; // to be confirm
            $glt->amount = $inputs['netValue'][$key];
            $glt->save();
        }

        $or = Invoice::find($or_id);
        $or->posted = 1;
        $or->wht = $wht;
        $or->save();

        return redirect('disbursement/'.$or_id.'/details/others');
    }

    public function addDisbursement(Requests\AddDisbursement $data) {

        $inputs = $data->request->all();
        $particulars = $inputs['particulars'];
        $amounts = $inputs['amounts'];

        // Insert Official Receipt
        $or = new Invoice;
        $or->or_number = $inputs['orNumber'];
        $or->delivery_receipt_number = $inputs['deliveryReceiptNumber'];
        $or->billing_invoice_number = $inputs['billingInvoiceNumber'];
        $or->payee = $inputs['payee'];
        $or->total = $inputs['total_amount'];
        $or->delivery_date = $inputs['dateReceived'];
        $or->type = 'others';
        $or->save();

        // Insert OR Items
        for($p = 0; $p < count($particulars); $p++) {
            $ori = new InvoiceItem;
            $ori->invoice_id = $or->id;
            $ori->particular = $particulars[$p];
            $ori->price = $amounts[$p];
            $ori->type = 'others';
            $ori->save();

        }

        return redirect('disbursement');
    }

    public function disbursementOthers($or) {

        $coa = ChartOfAccount::all();
        $particulars = InvoiceItem::where('invoice_id', $or)->get();
        $ordata = Invoice::where('id', $or)->first();
        $bank = $ordata->wht > 0 ? 'BDO' : 'EWB';
        $settings = Settings::all();
        $ctr = [];
        foreach($settings as $set) {
            $ctr[$set->name] = number_format($set->counter, 0, '', '');
            if ($set->name == $bank) {
                $check_number = number_format($set->counter, 0, '', '');
            }
        }

        return view('disbursement.others', compact('coa', 'particulars', 'ordata', 'ctr', 'check_number'));

    }

    public function printCheck($vc, Request $request) {

        $cv = CheckVoucher::find($vc);

        return view('disbursement.checktpl' , compact('cv'));
    }

    public function cvshogun($cv, Request $request) {
        $st = 1;
        if ($request->has('st')) {
            $st = $request->st;
        }

        $cv = CheckVoucher::find($cv);
        return view('disbursement.cvshogun', compact('cv', 'st'));
    }

    public function cvsupplier($cv, Request $request) {
        $st = 1;
        if ($request->has('st')) {
            $st = $request->st;
        }

        $cv = CheckVoucher::find($cv);
        return view('disbursement.cvsupplier', compact('cv', 'st'));
    }

    public function saveCheck(Request $request) {

        $input = $request->except(["_url", "_token"]);

        $cv = CheckVoucher::find($input['id']);

        if (is_null($cv)) {
            $cv = CheckVoucher::create($input);
            Settings::where('name', [$input['bank']])->increment('counter');
            Settings::where('name', ['Voucher'])->increment('counter');
        } else {
            $cv->update($input);
        }
        $cv->save();

        return redirect()->back()->with('message', 'Check Voucher information saved.');
    }

}
