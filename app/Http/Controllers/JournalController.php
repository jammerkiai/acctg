<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ChartOfAccount;
use App\Models\GeneralJournal;
use App\Models\GlTrans;

class JournalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gj = GeneralJournal::all();
    	return view('journal.index', compact('gj'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$coa = ChartOfAccount::all();
        return view('journal.entry', compact('coa'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $dr = $request->get('debit');
        $cr = $request->get('credit');
        $code = $request->get('glcode');
        $entrydate = $request->get('entrydate');

        $type = 'm';
        if ($request->has('type')) {
            $type = $request->get('type');
        }

        $drtotal = 0;

        for($i=0; $i < count($dr); $i++) {
            $drtotal += $dr[$i];
        }

        $amount = $drtotal;

        $gj = GeneralJournal::firstOrCreate([
                'entrydate' => $entrydate,
                'reason' => $request->get('reason'),
                'amount' => $amount
            ]);

        $newid = $gj->id;

        for( $i=0; $i < count($dr); $i++) {

            GlTrans::firstOrCreate([
                'transdate' => $entrydate,
                'ref_id' => $newid,
                'src' => 'GJ',
                'code' => $code[$i],
                'debit' => $dr[$i],
                'credit' => $cr[$i],
                'amount' => $dr[$i] + $cr[$i],
                'type' => $type
            ]);

        }
        
        
        return redirect('/journal');
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $gj = GeneralJournal::find($id);
        return view('journal.show', compact('gj'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gj = GeneralJournal::find($id);
        $coa = ChartOfAccount::all();
        return view('journal.edit', compact('gj', 'coa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = $request->all();
        foreach($post as $key => $value) {
            if (stripos($key, 'glcode_') === 0) {
                $glid = str_replace('glcode_', '', $key);
                $gl = GlTrans::find($glid);
                $gl->code = $value;
                $gl->save();
            }
        }

        return redirect('/journal/' . $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
