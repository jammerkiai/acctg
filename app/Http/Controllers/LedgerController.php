<?php

namespace App\Http\Controllers;

use App\Models\ChartOfAccount;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Ledger;

class LedgerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request) {

        $dfr = $request->has('dfr') ? $request->get('dfr') : date('Y-m-d', strtotime('yesterday'));
        $dto = $request->has('dto') ? $request->get('dto') : date('Y-m-d', strtotime('now'));


        $raw = Ledger::getRootAccounts($dfr, $dto);

        $gl = [];
        foreach ($raw as $amnts) {
            $gl[ $amnts->root ] = $amnts->amt;
        }

    	return view('ledger.index', ['dfr' => $dfr, 'dto' => $dto, 'gl' => $gl]);
    }

    public function level2($root, $dfr, $dto) {
        $raw = Ledger::getRecordsByRoot($root, $dfr, $dto);

        $gl = [];

        if ($root=='Assets') {
            $tpl = 'ledger.level2ass';
            foreach ($raw as $itm) {
                $gl[trim($itm->classification)][] = ['name' => trim($itm->name), 'amt' => $itm->amt, 'subclass'=> $itm->subclass];
            }
        } elseif (in_array($root, ['Liabilities', 'Expenses'])) {

            foreach ($raw as $itm) {
                $gl[trim($itm->classification)][] = ['name' => trim($itm->name), 'amt' => $itm->amt, 'subclass'=> $itm->subclass];
            }
            $tpl = 'ledger.level2ex';

        } elseif ($root == 'Equity') {
            $tpl = 'ledger.level2eq';

            foreach ($raw as $itm) {
                $gl[trim($itm->name)] = ['amt' => $itm->amt, 'subclass' => $itm->subclass];
            }


        } elseif ($root == 'Income') {
            $tpl = 'ledger.level2in';

            foreach ($raw as $itm) {
                $gl[trim($itm->name)] = ['amt' => $itm->amt, 'subclass' => $itm->subclass];
            }

        }

        return view($tpl, ['gl' => $gl, 'root' => $root, 'dfr' => $dfr, 'dto' => $dto]);
    }

    public function level3($subclass, $dfr, $dto) {

        $coa = Ledger::getAccountsByKey($subclass);

        if (count($coa) <= 1) {
            return $this->getDetails($coa[0], $dfr, $dto);
        }

        return $this->getLevel3($coa, $dfr, $dto);

    }

    /**
     * @param $coa
     * @param $dfr
     * @param $dto
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDetails($coa, $dfr, $dto) {

        $gl = Ledger::getTransactionsByCode($coa->code, $dfr, $dto);
        return view('ledger.detail', ['gl' => $gl, 'dfr' => $dfr, 'dto' => $dto, 'coa' => $coa]);
    }

    public function getLevel3($coa, $dfr, $dto) {
        $root = $coa[0]->root;
        $subclass = $coa[0]->subclass;
        $class = $coa['0']->classification;

        if ($subclass == '') {

            $raw = Ledger::getRecordsByClassification($class, $dfr, $dto);

        } else {

            $raw = Ledger::getRecordsBySubClass($subclass, $dfr, $dto);

        }

        $gl = [];

        foreach($raw as $act) {
            $gl[$subclass][] = ['name' => $act->name, 'amt' => $act->amt];
        }

        return view('ledger.level3' , ['gl' => $gl, 'root' => $root, 'dfr' => $dfr, 'dto' => $dto]);
    }
}
