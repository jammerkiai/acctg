<?php
/**
 * Created by PhpStorm.
 * User: machemo
 * Date: 2/29/16
 * Time: 4:05 PM
 */

namespace App\Http\Controllers;


use App\Http\Requests\PurchaseItemUnitRequest;
use App\PurchaseItemUnit;
use Illuminate\Http\Request;

class PurchaseItemUnitController extends Controller
{
    public function add(PurchaseItemUnitRequest $data) {
        $input = $data->request->all();
        $piu = new PurchaseItemUnit;
        $piu->name = $input['name'];
        $piu->save();

        $success = $input['name'] . ' added successfully';

        return redirect('/settings/purchase-unit/create/form')->with('success', ['message'=>$success]);
    }

    public function addForm() {
        return view('settings.units.create');
    }

    public function updateForm($id) {
        $unit = PurchaseItemUnit::find($id);
        return view('settings.units.edit', compact('unit'));
    }

    public function update($id, PurchaseItemUnitRequest $data) {
        $input = $data->request->all();
        $piu = PurchaseItemUnit::find($id);
        $piu->name = $input['name'];
        $piu->save();

        $success = 'Updated Successfully';

        return redirect('/settings/purchase-unit/update/form/' . $id)->with('success', ['message'=>$success]);
    }

    public function remove($id) {
        $unit = PurchaseItemUnit::find($id);
        $unit->delete();
        $success = 'Deleted Successfully';

        return redirect('settings/purchase-units')->with('success', ['message'=>$success]);
    }

    public function get() {
        $units = PurchaseItemUnit::all();
        return view('settings.units.index', compact('units'));
    }
}