<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddPurchaseOrder;
use App\Http\Requests\AddToPayableRequest;
use App\Http\Requests\PostReceivedItems;
use App\Models\GlTrans;
use App\Models\Supplier;
use App\Models\Settings;
use App\Purchase;
use App\PurchaseItem;
use App\InvoiceItem;
use App\Models\Invoice;
use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class PurchasingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request) {
        $filter = 'pending';
        if ($request->has('status')) {
            $filter = $request->get('status');
        }

        $status = ['received' => 'Closed', 'pending' => 'Waiting for delivery'];

        $purchases = Purchase::where('status', [$filter])->with('suppliers')->get();
    	return view('purchasing.index', compact(
            'purchases', 'status', 'filter'
        ));
    }

    public function purchaseForm() {

        $suppliers = Supplier::all();
        $units = DB::table('purchase_item_units')->get();

        return view('purchasing.purchase-form', array(
            'suppliers' => $suppliers,
            'units' => $units
        ));
    }

    public function addPurchaseOrder(AddPurchaseOrder $data) {

//        dd($data->request->all());

        $aggregator = array();
        $itemNames = $data->request->get('item_names');
        $itemQuantity = $data->request->get('quantity');
        $itemUnit = $data->request->get('units');

        for($i = 0; $i < count($itemNames); $i++) {
            array_push($aggregator, array(
                'name' => $itemNames[$i],
                'quantity' => $itemQuantity[$i],
                'unit' => $itemUnit[$i]
            ));
        }

        // Insert Purchase
        $purchase = new Purchase;
        $purchase->supplier_id = $data->request->get('supplier');
        $purchase->ordered_date = $data->request->get('order_date');
        $purchase->remarks = $data->request->get('remarks');
        $purchase->status = 'pending';
        $purchase->item_count = count($aggregator);
        $purchase->save();

        // Insert Items
        foreach($aggregator as $item) {
            $purchaseItem = new PurchaseItem;
            $purchaseItem->purchase_id = $purchase->id;
            $purchaseItem->name = $item['name'];
            $purchaseItem->unit_id = $item['unit'];
            $purchaseItem->ordered_qty = $item['quantity'];
            $purchaseItem->status = 'pending';
            $purchaseItem->save();
        }

        return redirect('purchasing');
    }

    public function purchaseDetails($purchaseId) {

        $purchase = Purchase::with('purchaseItems')
                    ->where('id', $purchaseId)
                    ->first();

        $settings = Settings::all();

        $set = [];
        foreach ($settings as $setting) {
            $set[$setting->name] = $setting->counter;
        }

        if (is_null($purchase)) {

            return redirect('purchasing')->with('error', 'Could not find the purchase order.');
        }

        $purchaseReceive = PurchaseItem::with('unit')
                                        ->where('purchase_id', $purchaseId)
                                        //->where('status', 'received')
                                        ->get();

        $invoices = Invoice::where('purchase_id', $purchaseId)
                            ->get();


        return view('purchasing.purchase-details', array(
            'purchase' => $purchase,
            'purchaseReceive' => $purchaseReceive,
            'invoices' => $invoices,
            'set' => $set
        ));
    }

    public function postReceivedItems(PostReceivedItems $data) {
//        $purchaseItems = PurchaseItem::with('purchase')->where('purchase_id', $data->request->get('purchaseNo'))->get();
//
//        // move to received orders
//        if(isset($purchaseItems)) {
//            foreach($purchaseItems as $item) {
//                if(in_array($item->id, $data->request->get('items', array(-1)))) {
//                    $purchaseItem = PurchaseItem::find($item->id);
//                    $purchaseItem->invoice_number = $data->request->get('invoiceNumber');
//                    $purchaseItem->received_date = $data->request->get('receivedDate');
//                    $purchaseItem->status = 'received';
//                    $purchaseItem->save();
//                }
//            }
//        }
//
//        // Change the status of purchase
//        $purchase = Purchase::find($data->request->get('purchaseNo'));
//        $purchaseItemReceivedCount = PurchaseItem::where('purchase_id', $data->request->get('purchaseNo'))
//                                                    ->where('status', '!=', 'none')->count();
//        if($purchase->item_count == $purchaseItemReceivedCount) {
//            $purchase->status = 'received';
//            $purchase->save();
//        }
//
//        return redirect()->to('/purchase/' . $data->request->get('purchaseNo') . '/details');
    }

    public function addToPayable(AddToPayableRequest $data) {

        $input = $data->request->all();



        //first create an invoice
        $inv = new Invoice();
        $inv->purchase_id = $input['purchaseNo'];
        $inv->invoice_number = $input['invoiceNumber'];
        $inv->delivery_date = $input['receivedDate'];
        $inv->total = $input['hiddentotal'];
        $inv->wht = $input['hiddenwht'];
        $inv->net = $input['hiddennet'];
        $inv->save();

        //add invoice items to purchase_order

        $itemIDs = $input['itemIds'];
        $itemPrices = $input['itemUnitPrices'];
        $itemSerials = $input['itemSerials'];
        $itemModels = $input['itemModels'];
        $itemBrands = $input['itemBrands'];

        $itemRecQty = $input['itemRecQty'];
        $itemNetTotals = $input['itemNetTotals'];
        $itemWHTs = $input['itemWHTs'];

        for ($i=0; $i < count($itemIDs); $i++) {
            if ($itemPrices[$i] > 0 && $itemRecQty[$i] > 0) {

                $po = new InvoiceItem();
                $po->purchase_item_id = $itemIDs[$i];
                $po->invoice_id = $inv->id;
                $po->price = $itemPrices[$i];
                $po->serial_number = $itemSerials[$i];
                $po->model = $itemModels[$i];
                $po->brand = $itemBrands[$i];
                $po->quantity = $itemRecQty[$i];
                $po->wht = $itemWHTs[$i];
                $po->net = $itemNetTotals[$i];


                if(isset($input['itemAddVAT']) && in_array($i, $input['itemAddVAT'])) {
                    $po->is_vatable = 'yes';
                }
                else {
                    $po->is_vatable = 'no';
                }

                $po->save();

                $glt = new GlTrans;
                $glt->transdate = $input['receivedDate'];
                $glt->ref_id = $input['purchaseNo'];
                $glt->src = 'purchasing';
                $glt->code = '2001'; // Accounts Payable
                $glt->code = ($po->wht) ? '2002' : '2003';
                $glt->debit = 0.00;
                $glt->credit = $itemNetTotals[$i];
                $glt->amount = $itemNetTotals[$i]; // to be confirm
                $glt->save();

                $pi = PurchaseItem::find($itemIDs[$i]);
                $newReceived = $pi->received_qty + $itemRecQty[$i];
                if ($newReceived >= $pi->ordered_qty) {
                   $pi->status = 'completed';
                }else{
                    $pi->status = 'pending';
                }

                $pi->received_qty = $newReceived;
                $pi->save();

            }
        }


        //update purchases if all items have been received

        $purchases = Purchase::find($input['purchaseNo']);

        $isPending = false;
        foreach($purchases->purchaseItems as $item) {
            if ($item->status == 'pending') {
                $isPending = true;
                break;
            }
        }

        if ($isPending === false) {
            $purchases->status = 'received';
            $purchases->received_date = $input['receivedDate'];
            $purchases->save();
        }

        return redirect()->to('/purchase/' . $data->request->get('purchaseNo') . '/details');
    }

    public function cancelItemPurchase(Request $request) {
        $cancelItems = $request->get('cancelItems');

        foreach($cancelItems as $itemId) {
            $purchaseItem = PurchaseItem::find($itemId);
            $purchaseItem->status = 'canceled';
            $purchaseItem->save();
        }

        return redirect()->to('/purchase/' . $request->get('purchaseNo') . '/details');
    }

    public function restoreItemPurchase(Request $request) {
        $restoreItems = $request->get('restoreItems');

        foreach($restoreItems as $itemId) {
            $purchaseItem = PurchaseItem::find($itemId);
            $purchaseItem->status = 'canceled';
            $purchaseItem->save();
        }

        return redirect()->to('/purchase/' . $request->get('purchaseNo') . '/details');
    }

    public function deletePurchase($purchaseId) {

        $purchase = Purchase::find($purchaseId);
        $purchase->delete();

        return redirect()->to('/purchasing');
    }
}
