<?php

namespace App\Http\Controllers;

use App\Models\CheckVoucher;
use App\Models\Invoice;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;

class ReleasingController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request) {
        $status = ['for releasing', 'released', 'cleared', 'canceled'];

        $filter = 'for releasing';
        if ($request->has('status')) {
            $filter = $request->get('status');
        }
        $cv = CheckVoucher::where('bank', ['BDO'])->where('status', [$filter])->paginate(50);
        $cv2 = CheckVoucher::where('bank', ['EWB'])->where('status', [$filter])->paginate(50);

    	return view('releasing.index', compact('cv', 'cv2', 'status', 'filter'));
    }

    public function status(Request $request) {
        Log::info(print_r($request->all(), 1));

        $cv = CheckVoucher::find($request->get('id'));
        $cv->status = $request->get('status');
        $cv->save();

        if ($cv->invoice_number != '') {
            $inv = Invoice::find($cv->invoice->id);
        } elseif ($cv->or_number != '') {
            $inv = Invoice::find($cv->official_receipt->id);
        } elseif ($cv->dr_number != '' ) {
            $inv = Invoice::find($cv->delivery_receipt->id);
        }

        if ($inv) {
            $inv->check_released = 1;
            $inv->save();
        }

        return response()->json(['message' => 'Status saved']);
    }

    public function details($cv) {
        $cv = CheckVoucher::find($cv);
        return view('releasing.details', compact('cv'));
    }
}
