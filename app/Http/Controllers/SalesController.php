<?php

namespace App\Http\Controllers;

use App\Models\RoomSales;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Ledger;
use App\Models\ChartOfAccount;
use DB;

class SalesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index(Request $request) {

        if ($request->has('dfr') && $request->has('dto')) {
            $dfr = $request->get('dfr');
            $dto = $request->get('dto');
        } else {
            list($dfr, $dto) = Ledger::getDefaultDateRange();
        }

        $accts = Ledger::getRecordsByRoot('INCOME', $dfr, $dto);

        $total = Ledger::getTotal($accts);
    	return view('sales.index', ['accts' => $accts, 'dfr' => $dfr, 'dto' => $dto, 'total' => $total]);
    }

    public function import(Request $request) {

        $totals = $salesCA = $salesCC = $salesCB = $drills = [];

        if ($request->has('pdate')) {
            $pdate = $request->get('pdate');
            $rs = new RoomSales($pdate);

            $totals['Cash']['data'] = $rs->getRoomAndMiscSalesTotals('Cash', true);  
            $totals['Cash']['total'] = RoomSales::getAggregateTotals($totals['Cash']['data']);
            $totals['Cash']['debit'] = '1001';
            $totals['Cash']['book'] = 1;

            $totals['Cash (m)']['data'] = $rs->getRoomAndMiscSalesTotals('Cash', false); 
            $totals['Cash (m)']['total'] = RoomSales::getAggregateTotals($totals['Cash (m)']['data']);
            $totals['Cash (m)']['debit'] = '1001';
            $totals['Cash (m)']['book'] = 0;
            
            $totals['Credit Card']['data'] = $rs->getRoomAndMiscSalesTotals('Credit'); 
            $totals['Credit Card']['total'] = RoomSales::getAggregateTotals($totals['Credit Card']['data']);
            $totals['Credit Card']['debit'] = '1022';
            $totals['Credit Card']['book'] = 1;

            // $totals['Debit Card']['data'] = $rs->getRoomAndMiscSalesTotals('Debit');
            // $totals['Debit Card']['total'] = RoomSales::getAggregateTotals($totals['Debit Card']['data']);
            // $totals['Debit Card']['debit'] = '1022';
            
                    

        } else {
            $pdate = date('Y-m-d', strtotime('yesterday'));
        }

        return view('sales.preview', ['pdate' => $pdate, 'totals' => $totals]);

    }

    public function add(Request $request) {
        $data = $request->all();
        $pdate = $data['pdate'];

        $existingSL = DB::table('sales_journal')->where('postdate', $pdate)->get();
        if(count($existingSL)) {
            DB::delete('delete from sales_journal where postdate=?', [$pdate]);
        }


        $existingGL = DB::table('gl_trans')
            ->where('transdate', $pdate)
            ->where('ref_id', $pdate)
            ->where('src', 'SL')
            ->get();
        if(count($existingGL)) {
            DB::delete('delete from gl_trans where transdate=? and ref_id=? and src=?', [$pdate, $pdate, 'SL']);
        }

        for ($x=0; $x < count($data['credit']); $x++) {
            $dr = $data['debit'][$x];
            $cr = $data['credit'][$x];
            $amt = $data['amount'][$x];
            $book = $data['book'][$x];
            $type = ($book) ? 'l' : 'm';
            $src = 'SL';
            $refid = date('Ymd', strtotime($pdate));
            
            DB::insert('insert into sales_journal (postdate, dr_code, cr_code, amount, book) 
                    values (?,?,?,?,?)', [$pdate, $dr, $cr, $amt, $book]);

            DB::insert('insert into gl_trans (transdate, ref_id, src, code, debit, credit, amount, type) 
                    values (?,?,?,?,?, ?,?,?)', [$pdate, $refid, $src, $dr, $amt, 0, $amt, $type]);

            DB::insert('insert into gl_trans (transdate, ref_id, src, code, debit, credit, amount, type) 
                    values (?,?,?,?,?,?,?,?)', [$pdate, $refid, $src, $cr, 0, $amt, $amt, $type]);
        }

        return redirect('sales');
    }

}
