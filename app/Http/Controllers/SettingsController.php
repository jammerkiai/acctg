<?php

namespace App\Http\Controllers;

use App\Models\ChartOfAccount;
use App\Models\GlTrans;
use App\Models\Settings;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class SettingsController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index() {
    	return view('settings.index');
    }

    public function settings() {
        $settings = Settings::all();
        return view('settings.settings', compact('settings'));
    }

    public function get($name) {
        $setting = Settings::where('name', [$name])->get();
        return response()->json($setting);
    }

    public function setValue(Request $request) {

        $setting = Settings::find($request->get('id'));
        $setting->counter = $request->get('counter');
        $setting->save();

        return response()->json(['message' => 'Counter updated']);
    }

    public function store(Request $request) {
        $input = $request->except(['_token', '_url', 'submitCtr']);
        $st = Settings::create($input);
        $st->save();

        return redirect()->to('settings/settings')->with('message', 'New counter added');
    }

    public function glinit() {
        $coa = ChartOfAccount::all();
        return view('settings.init', compact('coa'));
    }

    public function glinitstore(Request $request) {

        if ($request->has('act')) {
            $act = $request->get('act');
        }

        if ($act === 'Start Over') {
            DB::table('gl_trans')->truncate();
            return redirect()->to('settings/glinit')->with('message', 'Deleted all entries. Starting over.');
        }

        $this->truncateAll();

        $inputs = $request->except(['_url', '_token']);

        $type = 'm';
        if ($request->has('type')) {
            $type = 'l';
        }

        GlTrans::where('type', '=', $type)->delete();

        $postdate = $inputs['postdate'];

        for ($i = 0; $i < count($inputs['code']); $i++) {
            $data = [
                'transdate' => $postdate,
                'ref_id' => 0,
                'src'    => 'init',
                'code'   => $inputs['code'][$i],
                'debit'  => $inputs['debit'][$i],
                'credit' => $inputs['credit'][$i],
                'amount' => $inputs['debit'][$i] + $inputs['credit'][$i],
                'remarks' => 'Starting balance',
                'type'   => $type
            ];
            GlTrans::create($data)->save();
        }

        return redirect()->to('settings/glinit')->with('message', 'Starting balance saved. [' . $type .']');

    }

    private function truncateAll() {
//        DB::table('gl_trans')->truncate();
        DB::table('purchases')->truncate();
        DB::table('purchase_items')->truncate();
        DB::table('official_receipts')->truncate();
        DB::table('official_receipt_items')->truncate();
        DB::table('invoices')->truncate();
        DB::table('invoice_items')->truncate();
        DB::table('check_vouchers')->truncate();
        DB::table('general_journal')->truncate();
        DB::table('general_journal_details')->truncate();
        DB::table('asset_depreciation')->truncate();
        DB::table('asset_depreciation_schedule')->truncate();
    }
}
