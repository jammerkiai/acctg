<?php
/**
 * Created by PhpStorm.
 * User: king james
 * Date: 1/23/16
 * Time: 12:28 AM
 */

namespace App\Http\Controllers;


use App\Http\Requests\AddUserRequest;
use App\Http\Requests\AssignRoleRequest;
use App\RoleUser;
use App\User;
use Auth;
use Dotenv\Validator;

class UserController extends Controller 
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index() {

        return view('users.index', array(
            'users' => User::with('roles')->get()
        ));
    }

    public function addUserForm() {
        return view('users.addUser');
    }

    public function addUser(AddUserRequest $data) {

        $user = new User;
        $user->name = $data->request->get('name');
        $user->email = $data->request->get('email');
        $user->password = bcrypt($data->request->get('password'));
        $user->save();

        foreach($data->request->get('roles') as $role) {
            $user->assignRole($role);
        }

        return redirect('/settings/users');
    }

    public function assignRole(AssignRoleRequest $data) {

        $user = User::find($data->request->get('user'));
        $user->name = $data->request->get('name');
        $user->email = $data->request->get('email');
        $user->save();

        $updatedRoles = $data->request->get('roles');
        $roleChecker = $data->request->get('roles');

        $currentRoles = array();
        foreach($user->roles()->get() as $role) {
            array_push($currentRoles, array(
                'name' => $role->name,
                'id' => $role->id
            ));
        }

        foreach($currentRoles as $myRole) {
            if(in_array($myRole['name'], $updatedRoles)) {
                $roleToDelete = array_search($myRole['name'], $roleChecker);
                unset($roleChecker[$roleToDelete]);
            }
            else {
                RoleUser::where('role_id', $myRole['id'])
                        ->where('user_id', $user->id)
                        ->delete();
            }
        }

        foreach($roleChecker as $newRule) {
            $user->assignRole($newRule);
        }

        return redirect('/settings/users');

    }

    public function assignRoleForm($userId) {

        $user = User::with('roles')->where('id', $userId)->first();

        $userRoles = array();
        foreach($user->roles as $role) {
            array_push($userRoles, $role->name);
        }

        return view('users.editUser', array(
            'user' => $user,
            'roleList' => array(
                'Root',
                'Administrator',
                'Manager',
                'Supervisor',
                'User'
            ),
            'userRoles' => $userRoles
        ));
    }
}