<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AddDisbursement extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'orNumber' => '',
            'deliveryReceiptNumber' => '',
            'billingInvoiceNumber' => '',
            'payee' => 'required',
            'total_amount' => 'required',
            'dateReceived' => 'required'
        ];
    }
}
