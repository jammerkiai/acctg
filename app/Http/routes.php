<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', 'HomeController@welcome');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

 Route::group(['middleware' => 'ajax'], function () {
     Route::post('/releasing/status', 'ReleasingController@status');
     Route::post('/settings/setValue', 'SettingsController@setValue');
 });


Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::get('/', 'HomeController@index');
	Route::get('/home', 'HomeController@index');


	Route::get('/purchasing', 'PurchasingController@index');
    Route::get('/purchase/form', 'PurchasingController@purchaseForm');
    Route::get('/purchase/{purchaseId}/details', 'PurchasingController@purchaseDetails');
    Route::post('/purchase/order/add', 'PurchasingController@addPurchaseOrder');
    Route::post('/purchase/receive', 'PurchasingController@postReceivedItems');
    Route::get('/purchase/{purchaseId}/delete', 'PurchasingController@deletePurchase');
    Route::post('/purchase/payable/add', 'PurchasingController@addToPayable');

	Route::get('/disbursement', 'DisbursementController@index');
    Route::get('/disbursement/{invoice}/details', 'DisbursementController@disbursementDetails');
    Route::get('/disbursement/form', 'DisbursementController@disbursementForm');
    Route::get('/disbursement/{invoice}/details/others', 'DisbursementController@disbursementOthers');
    Route::get('/disbursement/printCheck/{vc}', 'DisbursementController@printCheck');
    Route::get('/disbursement/cvsupplier/{cv}', 'DisbursementController@cvsupplier');
    Route::get('/disbursement/cvshogun/{cv}', 'DisbursementController@cvshogun');
    Route::post('/disbursement/savecheck', 'DisbursementController@saveCheck');
    Route::post('/disbursement/service/add', 'DisbursementController@addDisbursement');
    Route::post('/disbursement/inventory/add', 'DisbursementController@addInventory');
    Route::post('/disbursement/payable/add', 'DisbursementController@addToPayable');

	Route::get('/releasing', 'ReleasingController@index');
	Route::get('/releasing/{cv}', 'ReleasingController@details');


	Route::get('/settings', 'SettingsController@index');
	Route::get('/settings/settings', 'SettingsController@settings');
	Route::get('/settings/settings/{name}', 'SettingsController@get');
	Route::post('/settings/store', 'SettingsController@store');
    Route::get('/settings/users', 'UserController@index');
    Route::post('/settings/user/create', 'UserController@addUser');
    Route::get('/settings/user/create/form', 'UserController@addUserForm');
    Route::post('/settings/user/assign/role', 'UserController@assignRole');
    Route::get('/settings/user/{userId}/assign/role', 'UserController@assignRoleForm');
    Route::get('/settings/purchase-units', 'PurchaseItemUnitController@get');
    Route::get('/settings/purchase-unit/delete/{unitId}', 'PurchaseItemUnitController@remove');
    Route::post('/settings/purchase-unit/create', 'PurchaseItemUnitController@add');
    Route::get('/settings/purchase-unit/create/form', 'PurchaseItemUnitController@addForm');
    Route::post('/settings/purchase-unit/update/{unitId}', 'PurchaseItemUnitController@update');
    Route::get('/settings/purchase-unit/update/form/{unitId}', 'PurchaseItemUnitController@updateForm');

    /** GL initialization */
    Route::get('/settings/glinit', 'SettingsController@glinit');
    Route::post('/settings/glinit', 'SettingsController@glinitstore');


	Route::get('/sales', 'SalesController@index');
	Route::get('/sales/import', 'SalesController@import');
	Route::post('/sales', 'SalesController@add');

	Route::resource('/journal', 'JournalController');


	Route::get('/ledger', 'LedgerController@index');
	Route::get('/ledger/level2/{root}/{dfr}/{dto}', 'LedgerController@level2');
	Route::get('/ledger/level3/{subclass}/{dfr}/{dto}', 'LedgerController@level3');

	Route::get('/reports', 'ReportsController@index');
	Route::get('/profile', 'ProfileController@index');

    Route::get('/assetmanagement/create/{id?}', 'AssetManagementController@create');
    Route::post('/assetmanagement/store', 'AssetManagementController@store');
    Route::get('/assetmanagement', 'AssetManagementController@index');
    Route::get('/assetmanagement/{id}', 'AssetManagementController@view');


    Route::get('/chart-accounts', 'ChartOfAccountController@chartOfAccounts');
    Route::resource('/coa', 'ChartOfAccountController');
	
	
	/** admin **/
	Route::get('/balancesheetyearly', 'AdminController@balancesheetyearly');
	Route::get('/balancesheetmonthly', 'AdminController@balancesheetmonthly');
	Route::get('/incomestatementyearly', 'AdminController@incomestatementyearly');
	Route::get('/incomestatementmonthly', 'AdminController@incomestatementmonthly');
});
