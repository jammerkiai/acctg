<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceItem extends Model
{
    protected $table = 'invoice_items';

    public function purchaseItem() {
        return $this->belongsTo('App\PurchaseItem', 'purchase_item_id');
    }

    public function invoice() {
        return $this->belongsTo('App\Models\Invoice', 'invoice_id');
    }

}
