<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Aggregate extends Model
{
	public  $year;
	public  $month;

	public function __construct($year=null, $month=null) {
		$this->year = is_null($year) ? date('Y') : $year;
		$this->month = is_null($month) ? date('m') : $month;
	}


    public function getBeginBalance($lastYear) {

    	$bb = DB::table('gl_trans')->select('code', 'amount')
    				->where(DB::raw('year(transdate)'), '=', $lastYear)
					->where('src', '=', 'init')
    				->get();

		return $bb;
    }

	public function getCurrentBalance($month, $year) {
		$cb = DB::table('gl_trans')
					->select(DB::raw('code, sum(debit) as dr, sum(credit) as cr, sum(debit - credit) as totalAmt'))
					->where(DB::raw('year(transdate)'), '=', $year)
					->where(DB::raw('month(transdate)'), '=', $month)
					->groupBy('code')
					->get();
		return $cb;
	}

	public function getMonthlyBalanceSheetData($month, $year) {

		$bb = $this->getBeginBalance($year - 1);
		$cb = $this->getCurrentBalance($month, $year);

		$ret = [];
		foreach ($bb as $b) {

			$ret[$b->code]['bb'] = $b->amount;
			$ret[$b->code]['cb'] = 0;
			$ret[$b->code]['ytd'] = $b->amount;

		}

		foreach ($cb as $c) {
			$ret[$c->code]['cb'] = $c->totalAmt;
			$beginbal = isset($ret[$c->code]['bb']) ? $ret[$c->code]['bb'] : 0;
			$ret[$c->code]['ytd'] = $beginbal + $c->totalAmt;
		}

		return $ret;
	}

	public function getAnnualAmountByCode($code, $year) {
		
		$dr = DB::table('gl_trans')
					->where('code', '=', $code)
					->where(DB::raw('year(transdate)', '=', $year))
					->sum('debit');
		$cr = DB::table('gl_trans')
					->where('code', '=', $code)
					->where(DB::raw('year(transdate)', '=', $year))
					->sum('credit');
		$total = $dr - $cr;
		return $total;
	}

	public function getYearlyBalanceSheetData($year) {
		$lastYear = $this->getBalancesByYear($year - 1);
		$thisYear = $this->getBalancesByYear();

		$data = [];
		$agg = [];

		
		foreach ($thisYear as $idx => $ac) {
			
			if (!isset($agg[$ac->classification])) {
				$agg[$ac->classification]['thisYear'] = 0;
				$agg[$ac->classification]['lastYear'] = 0;
			}


			if (isset($lastYear[$idx]->classification)) {
				
				$tmp = [
					'code' => $ac->code,
					'name' => $ac->name,
					'classification' => $ac->classification,
					'lastYear' => $lastYear[$idx]->total,
					'thisYear' => $ac->total,
					'diff' => $ac->total - $lastYear[$idx]->total ,
					'pctDiff' => ($lastYear[$idx]->total > 0) ? (100 * ($lastYear[$idx]->total - $ac->total)) / $lastYear[$idx]->total : 0,
				];

				$agg[$ac->classification]['thisYear'] += $ac->total;
				$agg[$ac->classification]['lastYear'] += $lastYear[$idx]->total;

			} else {
				$tmp = [
					'code' => $ac->code,
					'name' => $ac->name,
					'classification' => $ac->classification,
					'lastYear' => '-',
					'thisYear' => $ac->total,
					'diff' => '-',
					'pctDiff' => '-',
				];

				$agg[$ac->classification]['thisYear'] += $ac->total;
				$agg[$ac->classification]['lastYear'] += 0;
			}

			array_push($data, json_decode(json_encode($tmp)) );
		}

		return [$data, $agg];
	}

	public function getBalancesByYear($year = null) {
		$sql = "select a.code, a.name, a.classification,
  					sum(b.debit - b.credit) as total
				from chart_of_accounts a
      			left join gl_trans b on a.code=b.code ";
      	
      	if (!is_null($year)) {
      		$sql .= " where year(b.transdate) = '$year' ";
      	}
      			
		$sql .= " group by b.code";

		return DB::select($sql);				
	}


}
