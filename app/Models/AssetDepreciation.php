<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssetDepreciation extends Model
{
    protected $table = 'asset_depreciation';

    protected $fillable = [
        'code', 'purchase_amount', 'unit_interval', 'start_depreciation', 'purchase_date',
        'monthly_depreciation', 'end_depreciation', 'name', 'useful_life', 'remarks'
    ];

    public function coa() {
        return $this->belongsTo('App\Models\ChartOfAccount', 'code', 'code');
    }

    public function schedule() {
    	return $this->hasMany('App\Models\AssetDepreciationSchedule', 'ad_id');
    }
}
