<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssetDepreciationSchedule extends Model
{
    protected $table = 'asset_depreciation_schedule';
    protected $fillable = ['ad_id', 'interval_count', 'remaining_value', 'entry_date'];
}
