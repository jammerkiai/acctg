<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CheckVoucher extends Model
{
    protected $table = 'check_vouchers';

    protected $fillable = [
        "voucher_date", "payee", "amount_in_words", "particulars", "voucher_number", "invoice_number",
        "prepared_by", "approved_by", "checked_by", "check_number", "bank", "or_number", "dr_number", 'invoice_id'
    ];

    public function invoice() {
        return $this->belongsTo('App\Models\Invoice', 'invoice_id');
    }

    public function delivery_receipt() {
        return $this->belongsTo('App\Models\Invoice', 'dr_number', 'dr_number');
    }

    public function official_receipt() {
        return $this->belongsTo('App\Models\Invoice', 'or_number', 'or_number');
    }
}