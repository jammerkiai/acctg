<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GeneralJournal extends Model
{
    protected $table = 'general_journal';

    protected $fillable = [
        'entrydate', 'reason', 'amount'
    ];


    public function gltrans() {
    	return $this->hasMany('App\Models\GlTrans', 'ref_id', 'id');
    }
}
