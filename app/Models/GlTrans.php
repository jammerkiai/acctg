<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GlTrans extends Model
{
    protected $table = 'gl_trans';
    protected $fillable = [
    	'transdate', 'ref_id', 'src', 'debit', 'credit', 'amount', 'code','remarks', 'type'
    ];


    public function coa() {

    	return $this->belongsTo('App\Models\ChartOfAccount', 'code', 'code');
    }


}
