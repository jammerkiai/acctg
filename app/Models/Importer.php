<?php
/**
 * Created by PhpStorm.
 * User: jammer
 * Date: 24/1/16
 * Time: 9:50 PM
 */

namespace App\Models;

use Carbon\Carbon;
use DB;

class Importer
{
    protected $connection = 'fds_mysql';
    protected $start;
    protected $end;


    public function __construct($pdate) {
        $this->db = DB::connection($this->connection);
        $this->getInclusiveDateRange($pdate);
        return $this;
    }

    public function getInclusiveDateRange($fr, $to='') {
        if ($to == '') {
            $to = $fr;
        }
        $this->getStart($fr)->getEnd($to);
        return $this;
    }

    public function getStart($businessDate) {
        $sql = "SELECT datetime
                FROM `shift-transactions`
                where datetime < '$businessDate'
                and shift = 'start'
                order by datetime desc
                limit 0, 1";

        $this->start = $this->fetch($sql)[0]->datetime;
        return $this;
    }

    public function getEnd($businessDate) {
        $sql = "SELECT datetime
                FROM `shift-transactions`
                where date_format(datetime, '%Y-%m-%d') = '$businessDate'
                and shift='end'
                order by datetime desc
                limit 0, 1";

        $result = $this->fetch($sql);

        if (empty($result)) {
            $this->end = date('Y-m-d H:i:s');
        } else {
            $this->end = $this->fetch($sql)[0]->datetime;
        }

        return $this;
    }

    public function fetch($sql) {
        return  $this->db->select( DB::raw($sql) );
    }



}