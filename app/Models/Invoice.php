<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoices';

    protected $fillable = [
        'invoice_number', 'delivery_date', 'purchase_id', 'remarks'
    ];

    public function item() {
        return $this->hasMany('App\InvoiceItem', 'invoice_id');
    }

    public function check_voucher() {
        return $this->belongsTo('App\Models\CheckVoucher', 'id', 'invoice_id');
    }

    public function purchase() {
        return $this->belongsTo('App\Purchase', 'purchase_id');
    }
}
