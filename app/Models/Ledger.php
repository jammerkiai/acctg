<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;

class Ledger extends Model
{

    /**
     * @param $dfr
     * @param $dto
     * @return mixed
     */
    public static function getRootAccounts($dfr, $dto) {
        return DB::table('chart_of_accounts')
                ->leftJoin('gl_trans', 'chart_of_accounts.code', '=', 'gl_trans.code')
                ->select('chart_of_accounts.root', DB::raw("sum(if(gl_trans.transdate between '$dfr' and '$dto', gl_trans.amount,0)) as amt"))
                ->groupBy('chart_of_accounts.root')
                ->get();

    }

    /**
     * @param $root
     * @param $dfr
     * @param $dto
     * @return mixed
     */
    public static function getRecordsByRoot($root, $dfr, $dto) {

        $raw = DB::table('chart_of_accounts')
            ->leftJoin('gl_trans', 'chart_of_accounts.code', '=', 'gl_trans.code')
            ->select(
                'chart_of_accounts.classification',
                'chart_of_accounts.name',
                'chart_of_accounts.subclass',
                'chart_of_accounts.code',
                DB::raw("sum(if(gl_trans.transdate between '$dfr' and '$dto', gl_trans.amount, 0)) as amt"))
            ->where('chart_of_accounts.root', '=', $root)
            ->groupBy(DB::raw('chart_of_accounts.classification, chart_of_accounts.subclass'))
            ->get();

        return $raw;
    }

    /**
     * @param $key
     * @return mixed
     */
    public static function getAccountsByKey($key) {

        return DB::table('chart_of_accounts')
            ->where('subclass', $key)
            ->orWhere('classification', $key)
            ->orWhere('name', $key)
            ->get();

    }

    /**
     * @param $code
     * @param $dfr
     * @param $dto
     * @return mixed
     */
    public static function getTransactionsByCode($code, $dfr, $dto) {
        return  DB::table('gl_trans')
            ->where('code', $code)
            ->whereBetween('transdate', [$dfr, $dto])
            ->get();
    }

    /**
     * @param $subclass
     * @param $dfr
     * @param $dto
     * @return mixed
     */
    public static function getRecordsBySubClass($subclass, $dfr, $dto) {

        return DB::table('chart_of_accounts')
            ->leftJoin('gl_trans', 'chart_of_accounts.code', '=', 'gl_trans.code')
            ->select(
                'chart_of_accounts.classification',
                'chart_of_accounts.name',
                DB::raw("sum(if(gl_trans.transdate between '$dfr' and '$dto', gl_trans.amount, 0)) as amt"))
            ->where('chart_of_accounts.subclass', '=', $subclass)
            ->groupBy(DB::raw('chart_of_accounts.name'))
            ->get();

    }
    /**
     *
     */
    public static function getRecordsByClassification($class, $dfr, $dto) {
        return DB::table('chart_of_accounts')
            ->leftJoin('gl_trans', 'chart_of_accounts.code', '=', 'gl_trans.code')
            ->select(
                'chart_of_accounts.classification',
                'chart_of_accounts.name',
                DB::raw("sum(if(gl_trans.transdate between '$dfr' and '$dto', gl_trans.amount, 0)) as amt"))
            ->where('chart_of_accounts.classification', '=', $class)
            ->groupBy(DB::raw('chart_of_accounts.name'))
            ->get();
    }

    public static function getDefaultDateRange() {
        return [
          date('Y-m-d', strtotime('yesterday')),
          date('Y-m-d', strtotime('now'))
        ];
    }

    public static function getTotal($accts) {
        $total = 0;
        foreach ($accts as $a) {
            $total += $a->amt;
        }
        return $total;
    }

}

//        $sql = "select `chart_of_accounts`.`classification`,
//                      `chart_of_accounts`.`name`,
//                      sum(if(transdate between '2016-01-01' and '2016-01-31', gl_trans.amount, 0)) as amt
//                from `chart_of_accounts`
//                left join `gl_trans`
//                on `chart_of_accounts`.`code` = `gl_trans`.`code`
//                where `chart_of_accounts`.`root` = 'Liabilities'
//                group by `chart_of_accounts`.`classification`, `chart_of_accounts`.`name`";
