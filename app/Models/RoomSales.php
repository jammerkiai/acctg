<?php
/**
 * Created by PhpStorm.
 * User: jammer
 * Date: 25/1/16
 * Time: 10:52 PM
 */

namespace App\Models;

use App\Models\Importer;

class RoomSales extends Importer
{

    public function getRoomAndMiscSalesTotals($tender='Cash', $l=true) {
        if ($l) {
            $regflag = " and a.regflag > 0";
        } else {
            $regflag = " and a.regflag = 0";
        }

        $sql = "select sum(a.unit_cost * a.qty) as amount, b.code, b.name, 1 as aggr
                from room_sales a, chart_of_accounts b, sales_and_services c
                where a.sales_date between '{$this->start}' and '{$this->end}'
                and a.item_id=c.sas_id
                and c.parent=b.code
                and a.tendertype='$tender'
                $regflag
                group by c.parent
                union 
                ";

        $sql = " select sum(a.unit_cost * a.qty) as amount, b.code, b.name, 1 as aggr
                from room_sales a, chart_of_accounts b, sales_and_services c
                where a.sales_date between '{$this->start}' and '{$this->end}'
                and a.item_id=c.sas_id
                and c.code=b.code
                and a.tendertype='$tender'
                $regflag
                group by c.code
                ";


        return $this->fetch($sql);
    }


    public function getRoomAndMiscSalesLevel2($tender='Cash', $l=true) {
        if ($l) {
            $regflag = " and a.regflag > 0";
        } else {
            $regflag = " and a.regflag = 0";
        }

        $sql = "select (a.unit_cost * a.qty) as amount, b.code, b.name
                from room_sales a, chart_of_accounts b, sales_and_services c
                where a.sales_date between '{$this->start}' and '{$this->end}'
                and a.item_id=c.sas_id
                and c.parent=b.code
                and a.tendertype='$tender'
                $regflag
                union 
                ";

        $sql .= " select a.unit_cost * a.qty as amount, b.code, b.name
                from room_sales a, chart_of_accounts b, sales_and_services c
                where a.sales_date between '{$this->start}' and '{$this->end}'
                and a.item_id=c.sas_id
                and c.code=b.code
                and a.tendertype='$tender'
                $regflag
";


        return $this->fetch($sql);
    }

    public function getReservationSales() {

    }

    public function getRoomSales($itm='', $tender = 'Cash', $l = true) {

        if ($l) {
            $regflag = " and a.regflag > 0";
        } else {
            $regflag = " and a.regflag = 0";
        }

        $catsql = '';
        if ($itm != '') {
            $catsql = " and a.item_id = $itm ";
        }

        $sql = "select a.*, sum(a.unit_cost * a.qty) as amount,b.sas_cat_name, c.sas_description
                from room_sales a, sas_category b, sales_and_services c
                where a.sales_date between '{$this->start}' and '{$this->end}'
                and a.category_id=b.sas_cat_id
                and a.item_id=c.sas_id
                and a.tendertype = '$tender'
                $regflag
                $catsql
                order by a.category_id, a.item_id, a.sales_date";

        return $this->fetch($sql);
    }

    public function getTotal($items) {


    }

    public function getRoomSalesTotalsByItem($code = '') {

        $sql = "select a.category_id, b.sas_cat_name, c.sas_description,
                sum(a.unit_cost * a.qty) as amount, a.item_id, c.code
                from room_sales a, sas_category b, sales_and_services c
                where a.sales_date between '{$this->start}' and '{$this->end}'
                and a.category_id=b.sas_cat_id
                and a.item_id=c.sas_id";

        if ($code !== '') {
            $sql .= " and c.parent=$code ";
        }

        $sql .= " group by a.item_id
                  order by b.sas_cat_name, c.sas_description";

        return $this->fetch($sql);

    }

    public function getRoomSalesTotalsByCategory($cat = '') {
        $catsql = '';
        if ($cat != '') {
            $catsql = " and a.category_id=$cat ";
        }

        $sql = "select a.category_id, b.sas_cat_name, c.code, c.parent,
                sum(a.unit_cost * a.qty) as amount, a.item_id
                from room_sales a, sas_category b, sales_and_services c
                where a.sales_date between '{$this->start}' and '{$this->end}'
                and a.category_id=b.sas_cat_id
                and a.item_id=c.sas_id
                group by c.parent";

        return $this->fetch($sql);
    }


    public static function getAggregateTotals($data) {
        $total = 0;
        foreach ($data as $d) {
            if ($d->aggr == 1) {
                $total += $d->amount;
            }
        }
        return $total;
    }
}