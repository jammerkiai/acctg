<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfficialReceipt extends Model
{
    protected $table = 'official_receipts';

    public function officialReceiptItem() {
        return $this->hasMany('App\OfficialReceiptItem');
    }

    public function check_voucher() {
        return $this->belongsTo('App\Models\CheckVoucher', 'or_number', 'or_number');
    }
}
