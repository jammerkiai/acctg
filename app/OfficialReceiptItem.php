<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfficialReceiptItem extends Model
{
    protected $table = 'official_receipt_items';

    public function officialReceipt() {
        return $this->belongsTo('App\OfficialReceipt', 'or_id');
    }

    public function unit() {
        return $this->belongsTo('App\PurchaseItemUnit', 'unit_id');
    }
}
