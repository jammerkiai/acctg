<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $table = 'purchases';

    public function suppliers() {
        return $this->belongsTo('App\Supplier', 'supplier_id');
    }

    public function purchaseItems() {
        return $this->hasMany('App\PurchaseItem');
    }

    public function isComplete() {
        foreach($this->purchaseItems() as $item) {
            if ($item->status != 'canceled') {

            }
        }
    }
}
