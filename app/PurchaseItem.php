<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseItem extends Model
{
    protected $table = 'purchase_items';

    public function purchase() {
        return $this->belongsTo('App\Purchase', 'purchase_id');
    }

    public function unit() {
        return $this->belongsTo('App\PurchaseItemUnit', 'unit_id');
    }
}
