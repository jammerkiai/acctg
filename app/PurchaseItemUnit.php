<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseItemUnit extends Model
{
    protected $table = 'purchase_item_units';
}
