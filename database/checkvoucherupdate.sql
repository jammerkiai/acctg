ALTER TABLE `check_vouchers`
ADD `bank` INT NOT NULL AFTER `amount_in_words`,
ADD `status` ENUM('for releasing', 'released', 'cleared', 'canceled') NOT NULL DEFAULT 'for releasing' AFTER `bank`;