-- phpMyAdmin SQL Dump
-- version 4.4.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 28, 2016 at 09:55 AM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shogun`
--

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--
DROP TABLE IF EXISTS `invoices`;

CREATE TABLE IF NOT EXISTS `invoices` (
  `id` int(10) unsigned NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `invoice_number` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `or_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_receipt_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `billing_invoice_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `payee` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_date` date NOT NULL,
  `remarks` text COLLATE utf8_unicode_ci,
  `total` decimal(15,2) NOT NULL,
  `wht` decimal(12,2) NOT NULL,
  `net` decimal(12,2) NOT NULL,
  `posted` tinyint(4) NOT NULL DEFAULT '0',
  `type` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `purchase_id`, `invoice_number`, `or_number`, `delivery_receipt_number`, `billing_invoice_number`, `payee`, `delivery_date`, `remarks`, `total`, `wht`, `net`, `posted`, `type`, `created_at`, `updated_at`) VALUES
(1, 0, '', '431241234', '', '', '20', '2016-02-28', NULL, '12.00', '1.00', '0.00', 1, 'others', '2016-02-28 09:52:46', '2016-02-28 01:52:46');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_items`
--

DROP TABLE IF EXISTS `invoice_items`;
CREATE TABLE IF NOT EXISTS `invoice_items` (
  `id` int(10) unsigned NOT NULL,
  `purchase_item_id` bigint(11) NOT NULL,
  `invoice_id` bigint(20) NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `serial_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `particular` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `brand` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `wht` decimal(10,2) NOT NULL,
  `wht_percent` decimal(10,2) NOT NULL,
  `net` decimal(12,2) NOT NULL,
  `code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `posted` tinyint(4) NOT NULL DEFAULT '0',
  `is_vatable` tinyint(4) NOT NULL DEFAULT '0',
  `type` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `invoice_items`
--

INSERT INTO `invoice_items` (`id`, `purchase_item_id`, `invoice_id`, `price`, `serial_number`, `particular`, `model`, `brand`, `quantity`, `wht`, `wht_percent`, `net`, `code`, `posted`, `is_vatable`, `type`, `created_at`, `updated_at`) VALUES
(1, 0, 1, '12.00', '', 'Painting', '', '', 0, '0.21', '0.02', '10.50', '1002', 1, 1, 'others', '2016-02-28 09:52:46', '2016-02-28 01:52:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_items`
--
ALTER TABLE `invoice_items`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `invoice_items`
--
ALTER TABLE `invoice_items`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
