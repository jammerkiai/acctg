<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PurchaseOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('purchase_orders')) {
            Schema::create('purchase_orders', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('invoice_id');
                $table->integer('item_id');
                $table->decimal('price', 15, 2);
                $table->string('serial_number', 100);
                $table->string('mode', 100);
                $table->string('brand', 50);
                $table->integer('quantity');
                $table->tinyInteger('wht');
                $table->enum('status', ['received', 'pending', 'canceled']);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('purchase_orders');
    }
}
