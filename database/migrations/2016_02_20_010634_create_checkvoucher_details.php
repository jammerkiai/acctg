<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckvoucherDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('check_vouchers')) {
            Schema::create('check_vouchers', function (Blueprint $table) {
                $table->increments('id');
                $table->date('voucher_date');
                $table->string('voucher_number', 10);
                $table->string('check_number', 20);
                $table->string('payee');
                $table->string('invoice_number');
                $table->string('prepared_by');
                $table->string('checked_by');
                $table->string('approved_by');
                $table->text('particulars');
                $table->text('amount_in_words');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('check_vouchers');
    }
}
