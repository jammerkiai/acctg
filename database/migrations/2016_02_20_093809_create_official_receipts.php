<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfficialReceipts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('official_receipts')) {
            Schema::create('official_receipts', function (Blueprint $table) {
                $table->increments('id');
                $table->string('or_number');
                $table->string('delivery_receipt_number');
                $table->string('billing_invoice_number');
                $table->string('payee');
                $table->float('total_amount');
                $table->date('date_received');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('official_receipts');
    }
}
