<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfficialReceiptItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('official_receipt_items')) {
            Schema::create('official_receipt_items', function (Blueprint $table) {
                $table->increments('id');
                $table->string('or_id');
                $table->string('or_number');
                $table->string('particular');
                $table->float('amount');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('official_receipt_items');
    }
}
