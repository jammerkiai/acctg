<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetDepreciation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('asset_depreciation')) {
            Schema::create('asset_depreciation', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('code');
                $table->date('start_depreciation');
                $table->date('end_depreciation');
                $table->float('purchase_amount');
                $table->float('monthly_depreciation');
                $table->integer('useful_life');
                $table->string('unit_interval', 10);
                $table->text('remarks');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('asset_depreciation');
    }
}
