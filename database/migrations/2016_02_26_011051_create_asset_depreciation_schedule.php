<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetDepreciationSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('asset_depreciation_schedule')) {
            Schema::create('asset_depreciation_schedule', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('ad_id');
                $table->string('interval_count');
                $table->date('entry_date');
                $table->decimal('remaining_value', 12, 2);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('asset_depreciation_schedule');
    }
}
