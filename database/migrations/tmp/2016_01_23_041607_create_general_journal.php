
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralJournal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('general_journal', function (Blueprint $table) {
            $table->increments('id');
            $table->date('entrydate');
            $table->string('reason');
            $table->timestamps();
        });

        Schema::create('general_journal_details', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('general_journal_id');
            $table->string('code');
            $table->decimal('debit');
            $table->decimal('credit');
            $table->timestamps();
            $table->foreign('general_journal_id')
                  ->references('id')
                  ->on('general_journal')
                  ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('general_journal_details');
        Schema::drop('general_journal');
    }
}
