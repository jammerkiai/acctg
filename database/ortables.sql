-- phpMyAdmin SQL Dump
-- version 4.4.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 25, 2016 at 07:32 AM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shogun`
--

-- --------------------------------------------------------

--
-- Table structure for table `official_receipts`
--

CREATE TABLE IF NOT EXISTS `official_receipts` (
  `id` int(10) unsigned NOT NULL,
  `or_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_receipt_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billing_invoice_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payee` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total_amount` double(8,2) NOT NULL,
  `posted` tinyint(1) NOT NULL DEFAULT '0',
  `date_received` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `official_receipts`
--

INSERT INTO `official_receipts` (`id`, `or_number`, `delivery_receipt_number`, `billing_invoice_number`, `payee`, `total_amount`, `posted`, `date_received`, `created_at`, `updated_at`) VALUES
(1, '98765', '87654', '76543', 'Juan Masipag', 2500.00, 1, '2016-02-25', '2016-02-25 07:30:03', '2016-02-24 23:30:03'),
(2, '2345234', '', '', 'Juan', 8010.00, 1, '2016-02-25', '2016-02-25 07:26:10', '2016-02-24 23:26:10');

-- --------------------------------------------------------

--
-- Table structure for table `official_receipt_items`
--

CREATE TABLE IF NOT EXISTS `official_receipt_items` (
  `id` int(10) unsigned NOT NULL,
  `or_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `or_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `particular` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` double(8,2) NOT NULL,
  `wht` decimal(10,2) NOT NULL,
  `wht_percent` decimal(10,2) NOT NULL,
  `net` decimal(12,2) NOT NULL,
  `code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `posted` tinyint(1) NOT NULL DEFAULT '0',
  `is_vatable` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `official_receipt_items`
--

INSERT INTO `official_receipt_items` (`id`, `or_id`, `or_number`, `particular`, `amount`, `wht`, `wht_percent`, `net`, `code`, `posted`, `is_vatable`, `created_at`, `updated_at`) VALUES
(1, '1', '98765', 'Painting', 2000.00, '89.29', '0.05', '1696.43', '1004', 1, 1, '2016-02-25 07:30:03', '2016-02-24 23:30:03'),
(2, '1', '98765', 'Welding', 500.00, '22.32', '0.05', '424.11', '1004', 1, 1, '2016-02-25 07:30:03', '2016-02-24 23:30:03'),
(3, '2', '2345234', 'Painting', 10.00, '1.50', '0.15', '8.50', '1002', 1, 0, '2016-02-25 07:26:10', '2016-02-24 23:26:10'),
(4, '2', '2345234', 'Renting', 8000.00, '357.14', '0.05', '6785.71', '1003', 1, 1, '2016-02-25 07:26:10', '2016-02-24 23:26:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `official_receipts`
--
ALTER TABLE `official_receipts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `official_receipt_items`
--
ALTER TABLE `official_receipt_items`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `official_receipts`
--
ALTER TABLE `official_receipts`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `official_receipt_items`
--
ALTER TABLE `official_receipt_items`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
