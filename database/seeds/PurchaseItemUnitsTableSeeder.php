<?php

use Illuminate\Database\Seeder;

class PurchaseItemUnitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $units = array(
            'Box',
            'Can',
            'Pack',
            'Piece',
            'Pair',
            'Half dozen',
            'Dozen',
            'Liter',
            'Gallon',
            'Bottle',
            'Cup'
        );

        foreach($units as $unit) {
            DB::table('purchase_item_units')->insert(array(
                'name' => $unit
            ));
        }
    }
}
