<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Role;
use App\RoleUser;

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('permission_role')->delete();
        DB::table('role_user')->delete();
        DB::table('roles')->delete();
        DB::table('users')->delete();

        User::create([
        	'id' => 1,
        	'email' => 'jammer',
	        'password' => bcrypt('jammer'),
	        'remember_token' => str_random(10),
        ]);

        User::create([
        	'id' => 2,
        	'email' => 'rbm',
        	'password' => bcrypt('rbm'),
	        'remember_token' => str_random(10),
        ]);

        User::create([
        	'id' => 3,
        	'email' => 'mark',
        	'password' => bcrypt('mark'),
	        'remember_token' => str_random(10),
        ]);

        User::create([
            'id' => 4,
            'email' => 'king',
            'password' => bcrypt('king'),
            'remember_token' => str_random(10),
        ]);


        Role::create([
            'id'            => 1,
            'name'          => 'Root',
            'label'   => 'Use this account with extreme caution. When using this account it is possible to cause irreversible damage to the system.'
        ]);
        Role::create([
            'id'            => 2,
            'name'          => 'Administrator',
            'label'   => 'Full access to create, edit, and update companies, and orders.'
        ]);
        Role::create([
            'id'            => 3,
            'name'          => 'Manager',
            'label'   => 'Ability to create new companies and orders, or edit and update any existing ones.'
        ]);
        Role::create([
            'id'            => 4,
            'name'          => 'Supervisor',
            'label'   => 'Able to manage the company that the user belongs to, including adding sites, creating new users and assigning licences.'
        ]);
        Role::create([
            'id'            => 5,
            'name'          => 'User',
            'label'   => 'A standard user that can have a licence assigned to them. No administrative features.'
        ]);

        DB::table('role_user')->truncate();
        RoleUser::create([
        	'role_id' => 1,
        	'user_id' => 1
        ]);

        RoleUser::create([
        	'role_id' => 2,
        	'user_id' => 2
        ]);

        RoleUser::create([
        	'role_id' => 5,
        	'user_id' => 3
        ]);

        RoleUser::create([
            'role_id' => 3,
            'user_id' => 4
        ]);
    }

}