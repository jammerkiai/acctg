## Setup

1. Create a database (ie. acctg)
2. Run the migration script: `php artisan migrate`
3. Seed the database: `php artisan db:seed`
* NOTE: you can edit the UserTableSeeder.php file to add your own account info




