@extends('layouts.base')
@section('content')

	<!-- BEGIN PAGE -->
	<div class="page-content">
		<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		<!-- BEGIN PAGE CONTAINER-->
		<div class="container-fluid">
			<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Balance Sheet
						</h3>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row-fluid">
				<div class="span12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="tabbable tabbable-custom boxless">
                     <ul class="nav nav-tabs">
                        <li><a href="#tab_1" data-toggle="tab">Monthly</a></li>
                        <li class="active"><a href="#tab_2" data-toggle="tab">Yearly</a></li>
                     </ul>
                     <div class="tab-content">
                        <div class="tab-pane" id="tab_1">
                        	<div class="portlet box green">
								<div class="portlet-title">
									<h4><i class="icon-credit-card"></i></h4>
								</div>
								<div class="portlet-body">
									<div class="row-fluid">
										<div class="span4">
											<div class="form-horizontal">
												<div class="control-group">
				                                   <label class="control-label">Month</label>
				                                   <div class="controls">
				                                      <select class="small" tabindex="1">
				                                         <option value="January">January</option>
				                                         <option value="February">February</option>
				                                         <option value="March">March</option>
				                                         <option value="April">April</option>
				                                         <option value="May">May</option>
				                                         <option value="June">June</option>
				                                         <option value="July">July</option>
				                                         <option value="August">August</option>
				                                         <option value="September">September</option>
				                                         <option value="October">October</option>
				                                         <option value="November">November</option>
				                                         <option value="December">December</option>
				                                      </select>
				                                   </div>
				                                </div>
				                            </div>
				                        </div>
				                        <div class="span3">
											<div class="form-horizontal">
												<div class="control-group">
				                                   <label class="control-label">Year</label>
				                                   <div class="controls">
				                                      <select class="small" tabindex="1">
				                                         <option value="2016">2016</option>
				                                      </select>
				                                   </div>
				                                </div>
				                            </div>
				                        </div>
				                        <div class="span2">
											<div class="form-horizontal">
												<div class="control-group">
				                                   <div class="controls">
				                                      <input type="submit" class="btn blue" value="Go">
				                                   </div>
				                                </div>
				                            </div>
				                        </div>
		                        	</div>
		                        	<div class="jfp_txt_center">
			                        	<h4>H & R BUSINESS DEV INC</h4>
			                        	<h4>BALANCE SHEET</h4>
			                        	<h4>FOR THE MONTH OF JANUARY 2014</h4>
									</div><br>
									<div class="table_container">
										<table class="table_style" cellpadding="5" border="1">
											<tr>
												<td class="jfp_txt_center jfp_bold parent_title_head">ASSET</td>
												<td class="jfp_txt_center jfp_bold thead_style">BEGINNING</td>
												<td class="jfp_txt_center jfp_bold thead_style">MONTH TO DATE</td>
												<td class="jfp_txt_center jfp_bold thead_style">YEAR TO DATE</td>
											</tr>
											<tr>
												<td class="jfp_bold child_title_style">CURRENT ASSET</td>
												<td></td><td></td><td></td>
											</tr>
											<tr>
												<td>Cash In Bank</td>
												<td class="jfp_align_right"> 5,832,325.00</td>
												<td class="jfp_align_right"> 45,711.38 </td>
												<td class="jfp_align_right"> 5,878,036.38 </td>
											</tr>
											<tr>
												<td> Petty Cash Fund </td>
												<td class="jfp_align_right"> 20,000.00 </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> 20,000.00 </td>
											</tr>
											<tr>
												<td> Change Fund </td>
												<td class="jfp_align_right"> 20,000.00 </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> 20,000.00 </td>
											</tr>
											<tr>
												<td> Deposit Rental - Leasehold </td>
												<td class="jfp_align_right"> 1,080,000.00 </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> 1,080,000.00 </td>
											</tr>
											<tr>
												<td> Deposit Water - Maynilad </td>
												<td class="jfp_align_right"> 60,830.00 </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> 60,830.00 </td>
											</tr>
											<tr>
												<td> Investment - Meralco </td>
												<td class="jfp_align_right"> 505,980.00 </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> 505,980.00 </td>
											</tr>
											<tr>
												<td> Bill Deposit Meralco </td>
												<td class="jfp_align_right"> 534,644.00 </td>
												<td class="jfp_align_right"> 32,662.27 </td>
												<td class="jfp_align_right"> 567,306.27 </td>
											</tr>
											<tr>
												<td> Meter Deposit Meralco </td>
												<td class="jfp_align_right"> 29,000.00 </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> 29,000.00 </td>
											</tr>
											<tr>
												<td> Merchandise Inventory </td>
												<td class="jfp_align_right"> 82,762.00 </td>
												<td class="jfp_align_right"> (4,895.51)</td>
												<td class="jfp_align_right"> 77,866.49 </td>
											</tr>
											<tr>
												<td> Accounts Receivable - Salon </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> - </td>
											</tr>
											<tr>
												<td> Accounts Receivable - Credit Card </td>
												<td class="jfp_align_right"> 289,466.00 </td>
												<td class="jfp_align_right"> (189,342.34) </td>
												<td class="jfp_align_right"> 100,123.66 </td>
											</tr>
											<tr>
												<td> Accounts Receivable - Creditable Tax Withheld at Source </td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"> 11,380.76 </td>
												<td class="jfp_align_right"> 11,380.76 </td>
											</tr>
											<tr>
												<td> Accounts Receivable - SSS </td>
												<td class="jfp_align_right"> 2,730.00 </td>
												<td class="jfp_align_right"> 43,269.60 </td>
												<td class="jfp_align_right"> 45,999.60 </td>
											</tr>
											<tr>
												<td> Accounts Receivable - Creditable Input Tax </td>
												<td class="jfp_align_right"> 24,839.00 </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> 24,839.00 </td>
											</tr>
											<tr>
												<td> Accounts Receivable </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> 47,616.73 </td>
												<td class="jfp_align_right"> 47,616.73 </td>
											</tr>
											<tr>
												<td class="jfp_align_right total_style">TOTAL CURRENT ASSET</td>
												<td class="jfp_align_right total_style"> 8,482,576.00 </td>
												<td class="jfp_align_right total_style"> (13,597.11)</td>
												<td class="jfp_align_right total_style"> 8,468,978.89 </td>
											</tr>
											<tr>
												<td class="jfp_bold child_title_style"> NON CURRENT ASSET </td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td> Building </td>
												<td class="jfp_align_right"> 31,623,718.00 </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> 31,623,718.00 </td>
											</tr>
											<tr>
												<td> Building Equipment </td>
												<td class="jfp_align_right"> 785,580.00 </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> 785,580.00 </td>
											</tr>
											<tr>
												<td> Building Bangquet Utensils </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> - </td>
											</tr>
											<tr>
												<td> Building Furniture </td>
												<td class="jfp_align_right"> 107,589.00 </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> 107,589.00 </td>
											</tr>
											<tr>
												<td> Transport Vehicle </td>
												<td class="jfp_align_right"> 1,057,143.00 </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> 1,057,143.00 </td>
											</tr>
											<tr>
												<td> Building & Building Improvement </td>
												<td class="jfp_align_right"> 28,525,504.00 </td>
												<td class="jfp_align_right"> 43,757.14 </td>
												<td class="jfp_align_right"> 28,569,261.14 </td>
											</tr>
											<tr>
												<td> Office Equipment </td>
												<td class="jfp_align_right"> 238,951.00 </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> 238,951.00 </td>
											</tr>
											<tr>
												<td> Furniture & Fixtures </td>
												<td class="jfp_align_right"> 947,875.00 </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> 947,875.00 </td>
											</tr>
											<tr>
												<td> Hotel Equipment </td>
												<td class="jfp_align_right"> 6,326,425.00 </td>
												<td class="jfp_align_right"> 109,450.89 </td>
												<td class="jfp_align_right"> 6,435,875.89 </td>
											</tr>
											<tr>
												<td> Kitchen Equipment </td>
												<td class="jfp_align_right"> 1,139,156.00 </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> 1,139,156.00 </td>
											</tr>
											<tr>
												<td> Housekeeping Equipment </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> - </td>
											</tr>
											<tr>
												<td> Kitchen Wares </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> - </td>
											</tr>
											<tr>
												<td> Tools </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> - </td>
											</tr>
											<tr>
												<td> Software System </td>
												<td class="jfp_align_right"> 115,077.00 </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> 115,077.00 </td>
											</tr>
											<tr>
												<td class="jfp_align_right total_style"> SUB-TOTAL </td>
												<td class="jfp_align_right total_style"> 70,867,018.00 </td>
												<td class="jfp_align_right total_style"> 153,208.03 </td>
												<td class="jfp_align_right total_style"> 71,020,226.03 </td>
											</tr>
											<tr>
												<td class="jfp_align_right total_style"> Accumulated Depreciation </td>
												<td class="jfp_align_right total_style"> 20,659,040.00 </td>
												<td class="jfp_align_right total_style"> 299,276.20 </td>
												<td class="jfp_align_right total_style"> 20,958,316.20 </td>
											</tr>
											<tr>
												<td class="jfp_align_right total_style"> PROPERTY & EQUIPMENT  - NET </td>
												<td class="jfp_align_right total_style"> 50,207,978.00 </td>
												<td class="jfp_align_right total_style"> (146,068.17) </td>
												<td class="jfp_align_right total_style"> 50,061,909.83 </td>
											</tr>
											<tr>
												<td class="jfp_bold child_title_style">OTHER ASSET</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td> Pre-Operating Expenses - Shogun 2 </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td> Prepaid Insurance Expense </td>
												<td class="jfp_align_right"> 20,969.00 </td>
												<td class="jfp_align_right"> 26,796.85 </td>
												<td class="jfp_align_right"> 47,765.85 </td>
											</tr>
											<tr>
												<td> Deferred Income Tax Payable (MCIT) </td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> - </td>
											</tr>
											<tr>
												<td> Deferred Charges </td>
												<td class="jfp_align_right"> 982,420.00 </td>
												<td class="jfp_align_right"> 293,486.27 </td>
												<td class="jfp_align_right"> 1,275,906.27 </td>
											</tr>
											<tr>
												<td class="jfp_align_right total_style"> TOTAL OTHER ASSET </td>
												<td class="jfp_align_right total_style"> 1,003,389.00 </td>
												<td class="jfp_align_right total_style"> 320,283.12 </td>
												<td class="jfp_align_right total_style"> 1,323,672.12 </td>
											</tr>
											<tr>
												<td class="jfp_txt_center jfp_bold parent_title_head">ASSET</td>
												<td class="jfp_align_right jfp_bold parent_title_head"> 59,693,943.00 </td>
												<td class="jfp_align_right jfp_bold parent_title_head"> 160,617.84 </td>
												<td class="jfp_align_right jfp_bold parent_title_head"> 59,854,560.84 </td>
											</tr>
											<tr>
												<td colspan="4"></td>
											</tr>
											<tr>
												<td colspan="4"></td>
											</tr>
											<tr>
												<td class="jfp_txt_center jfp_bold parent_title_head"> LIABILITIES AND STOCKHOLDERS' EQUITY </td>
												<td class="jfp_txt_center jfp_bold thead_style">BEGINNING</td>
												<td class="jfp_txt_center jfp_bold thead_style">MONTH TO DATE</td>
												<td class="jfp_txt_center jfp_bold thead_style">YEAR TO DATE</td>
											</tr>
											<tr>
												<td class="jfp_bold child_title_style">CURRENT LIABILITIES</td>
												<td></td><td></td><td></td>
											</tr>
											<tr>
												<td> Accounts Payable - EWB </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> 8,084.19 </td>
												<td class="jfp_align_right"> 8,084.19 </td>
											</tr>
											<tr>
												<td> Accrued & Other Expense </td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"> 78,773.14 </td>
												<td class="jfp_align_right"> 78,773.14 </td>
											</tr>
											<tr>
												<td> Withholding Tax Payable </td>
												<td class="jfp_align_right"> 37,033.00 </td>
												<td class="jfp_align_right"> 1,675.51 </td>
												<td class="jfp_align_right"> 38,708.51 </td>
											</tr>
											<tr>
												<td> Expanded Tax Payable </td>
												<td class="jfp_align_right"> 42,430.00 </td>
												<td class="jfp_align_right"> (1,596.20) </td>
												<td class="jfp_align_right"> 40,833.80 </td>
											</tr>
											<tr>
												<td> Philhealth Payable </td>
												<td class="jfp_align_right"> 29,725.00 </td>
												<td class="jfp_align_right"> 875.00 </td>
												<td class="jfp_align_right"> 30,600.00 </td>
											</tr>
											<tr>
												<td> Pag-Ibig Premium Payable </td>
												<td class="jfp_align_right"> 20,950.00 </td>
												<td class="jfp_align_right"> 600.00 </td>
												<td class="jfp_align_right"> 21,550.00 </td>
											</tr>
											<tr>
												<td> Pag-Ibig Loan Payable </td>
												<td class="jfp_align_right"> 32,188.00 </td>
												<td class="jfp_align_right"> 331.50 </td>
												<td class="jfp_align_right"> 32,519.50 </td>
											</tr>
											<tr>
												<td> SSS Premium Payable </td>
												<td class="jfp_align_right"> 123,662.00 </td>
												<td class="jfp_align_right"> 15,928.00 </td>
												<td class="jfp_align_right"> 139,590.00 </td>
											</tr>
											<tr>
												<td> SSS Loan Payable </td>
												<td class="jfp_align_right"> 19,246.00 </td>
												<td class="jfp_align_right"> 1,680.70 </td>
												<td class="jfp_align_right"> 20,926.70 </td>
											</tr>
											<tr>
												<td> Income Tax Payable </td>
												<td class="jfp_align_right"> 73,638.00 </td>
												<td class="jfp_align_right"> 8,994.36 </td>
												<td class="jfp_align_right"> 82,632.36 </td>
											</tr>
											<tr>
												<td> Vat Payable </td>
												<td class="jfp_align_right"> 263,359.00 </td>
												<td class="jfp_align_right"> 23,710.49 </td>
												<td class="jfp_align_right"> 287,069.49 </td>
											</tr>
											<tr>
												<td class="jfp_align_right total_style"> TOTAL CURRENT LIABILITIES </td>
												<td class="jfp_align_right total_style"> 642,231.00 </td>
												<td class="jfp_align_right total_style"> 139,056.69 </td>
												<td class="jfp_align_right total_style"> 781,287.69 </td>
											</tr>
											<tr>													
												<td class="jfp_bold child_title_style">NON-CURRENT LIABILITIES</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td> Construction Payable </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"> - </td>
											</tr>
											<tr>
												<td> Rental Deposit </td>
												<td class="jfp_align_right"> 70,000.00 </td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"> 70,000.00 </td>
											</tr>
											<tr>
												<td> Loan Payable  & Other Payables </td>
												<td class="jfp_align_right"> 33,000,000.00 </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> 33,000,000.00 </td>
											</tr>
											<tr>
												<td class="jfp_align_right total_style"> TOTAL NON-CURRENT LIABILITIES </td>
												<td class="jfp_align_right total_style"> 33,070,000.00 </td>
												<td class="jfp_align_right total_style"> - </td>
												<td class="jfp_align_right total_style"> 33,070,000.00 </td>
											</tr>
											<tr>
												<td class="jfp_txt_center jfp_bold parent_title_head"> TOTAL LIABILITIES </td>
												<td class="jfp_align_right jfp_bold parent_title_head"> 33,712,231.00 </td>
												<td class="jfp_align_right jfp_bold parent_title_head"> 139,056.69 </td>
												<td class="jfp_align_right jfp_bold parent_title_head"> 33,851,287.69 </td>
											</tr>
											<tr>
												<td colspan="4"></td>
											</tr>
											<tr>
												<td colspan="4"></td>
											</tr>
											<tr>
												<td class="jfp_txt_center jfp_bold parent_title_head">  STOCKHOLDERS' EQUITY </td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
											<tr>
												<td> Capital Stock </td>
												<td class="jfp_align_right"> 26,000,000.00 </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> 26,000,000.00 </td>
											</tr>
											<tr>
												<td> Increase in  Capital Stock - Paid Up Deposit </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> - </td>
											</tr>
											<tr>
												<td> Retained Earnings </td>
												<td class="jfp_align_right"> (18,288.00)</td>
												<td class="jfp_align_right"> 21,561.15 </td>
												<td class="jfp_align_right"> 3,273.15 </td>
											</tr>
											<tr>
												<td class="jfp_align_right total_style"> TOTAL EQUITY </td>
												<td class="jfp_align_right total_style"> 25,981,712.00 </td>
												<td class="jfp_align_right total_style"> 21,561.15 </td>
												<td class="jfp_align_right total_style"> 26,003,273.15 </td>
											</tr>
											<tr>
												<td class="jfp_txt_center jfp_bold parent_title_head"> TOTAL LIABILITIES AND STOCKHOLDERS EQUITY </td>
												<td class="jfp_txt_center jfp_bold parent_title_head"> 59,693,943.00 </td>
												<td class="jfp_txt_center jfp_bold parent_title_head"> 160,617.84 </td>
												<td class="jfp_txt_center jfp_bold parent_title_head"> 59,854,560.84 </td>
											</tr>
											<tr>
												<td></td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"> (0.00)</td>
												<td class="jfp_align_right"> (0.00)</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
                        </div>
                        <div class="tab-pane active" id="tab_2">
                        	<div class="portlet box green">
								<div class="portlet-title">
									<h4><i class="icon-credit-card"></i></h4>
								</div>
								<div class="portlet-body">
									<div class="row-fluid">
										<div class="span4">
											<div class="form-horizontal">
												<div class="control-group">
				                                   <label class="control-label">From year</label>
				                                   <div class="controls">
				                                      <select class="small" tabindex="1">
				                                         <option value="2016">2016</option>
				                                      </select>
				                                   </div>
				                                </div>
				                            </div>
				                        </div>
				                        <div class="span3">
											<div class="form-horizontal">
												<div class="control-group">
				                                   <label class="control-label">To year</label>
				                                   <div class="controls">
				                                      <select class="small" tabindex="1">
				                                         <option value="2016">2016</option>
				                                      </select>
				                                   </div>
				                                </div>
				                            </div>
				                        </div>
				                        <div class="span2">
											<div class="form-horizontal">
												<div class="control-group">
				                                   <div class="controls">
				                                      <input type="submit" class="btn blue" value="Go">
				                                   </div>
				                                </div>
				                            </div>
				                        </div>
		                        	</div>
		                        	<div class="jfp_txt_center">
			                        	<h4>H & R BUSINESS DEV INC</h4>
			                        	<h4>BALANCE SHEET</h4>
			                        	<h4>DECEMBER 31, 2014 AND 2013</h4>
									</div><br>
									<div class="table_container">
										<table class="table_style" cellpadding="5" border="1">
											<tr>
												<td rowspan="2" class="jfp_txt_center jfp_bold parent_title_head">ASSET</td>
												<td rowspan="2" class="jfp_txt_center jfp_bold thead_style">2014</td>
												<td rowspan="2" class="jfp_txt_center jfp_bold thead_style">2013</td>
												<td colspan="2" class="jfp_txt_center jfp_bold thead_style">RATIO</td>
											</tr>
											<tr>
												<td class="jfp_txt_center jfp_bold thead_style">AMT INC</td>
												<td class="jfp_txt_center jfp_bold thead_style">% INC</td>
											</tr>
											<tr>
												<td class="jfp_bold child_title_style">CURRENT ASSET</td>
												<td></td><td></td><td></td><td></td>
											</tr>
											<tr>
												<td>Cash</td>
												<td class="jfp_align_right"> 4,350,651 </td>
												<td class="jfp_align_right"> 5,872,325 </td>
												<td class="jfp_align_right"> (1,521,674) </td>
												<td class="jfp_align_right">-26%</td>
											</tr>
											<tr>
												<td> CREDITABLE INPUT TAX </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right"> 24,839 </td>
												<td class="jfp_align_right"> (24,839)</td>
												<td class="jfp_align_right">-100%</td>
											</tr>
											<tr>
												<td> ACCOUNTS RECEIVABLE </td>
												<td class="jfp_align_right"> 181,869 </td>
												<td class="jfp_align_right"> 289,466 </td>
												<td class="jfp_align_right"> (107,597)</td>
												<td class="jfp_align_right">-37%</td>
											</tr>
											<tr>
												<td> INVENTORY </td>
												<td class="jfp_align_right"> 93,302 </td>
												<td class="jfp_align_right"> 82,762 </td>
												<td class="jfp_align_right"> 10,540 </td>
												<td class="jfp_align_right">13%</td>
											</tr>
											<tr>
												<td></td>
												<td class="jfp_align_right total_style"> 4,625,822 </td>
												<td class="jfp_align_right total_style"> 6,269,392 </td>
												<td class="jfp_align_right total_style"> (1,643,570)</td>
												<td class="jfp_align_right total_style">-26%</td>
											</tr>
											<tr>
												<td class="jfp_bold child_title_style">NON-CURRENT ASSET</td>
												<td></td><td></td><td></td><td></td>
											</tr>
											<tr>
												<td> PROPERTY & EQUIPMENT  - NET </td>
												<td class="jfp_align_right"> 47,055,524 </td>
												<td class="jfp_align_right"> 50,207,978 </td>
												<td class="jfp_align_right"> (3,152,454)</td>
												<td class="jfp_align_right">-6%</td>
											</tr>
											<tr>
												<td> OTHER ASSETS </td>
												<td class="jfp_align_right"> 3,739,055 </td>
												<td class="jfp_align_right"> 3,216,573 </td>
												<td class="jfp_align_right"> 522,482 </td>
												<td class="jfp_align_right">16%</td>
											</tr>
											<tr>
												<td></td>
												<td class="jfp_align_right total_style"> 50,794,579 </td>
												<td class="jfp_align_right total_style"> 53,424,551 </td>
												<td class="jfp_align_right total_style"> (2,629,972)</td>
												<td class="jfp_align_right total_style">-5%</td>
											</tr>
											<tr>
												<td class="jfp_txt_center jfp_bold parent_title_head">TOTAL ASSETS</td>
												<td class="jfp_align_right jfp_bold parent_title_head"> 55,420,401 </td>
												<td class="jfp_align_right jfp_bold parent_title_head"> 59,693,943 </td>
												<td class="jfp_align_right jfp_bold parent_title_head"> (4,273,542)</td>
												<td class="jfp_align_right jfp_bold parent_title_head">-7%</td>
											</tr>
											<tr><td colspan="5"></td></tr>
											<tr><td colspan="5"></td></tr>
											<tr>
												<td class="jfp_txt_center jfp_bold parent_title_head"> LIABILITIES AND STOCKHOLDERS' EQUITY </td>
												<td></td><td></td><td></td><td></td>
											</tr>
											<tr>
												<td class="jfp_bold child_title_style">CURRENT LIABILITIES</td>
												<td></td><td></td><td></td><td></td>
											</tr>
											<tr>
												<td> ACCOUNTS PAYABLE AND ACCRUED EXPENSES </td>
												<td class="jfp_align_right"> 637,827 </td>
												<td class="jfp_align_right"> 568,593 </td>
												<td class="jfp_align_right"> 69,234 </td>
												<td class="jfp_align_right">12%</td>
											</tr>
											<tr>
												<td> INCOME TAX PAYABLE </td>
												<td class="jfp_align_right"> 118,633 </td>
												<td class="jfp_align_right"> 73,638 </td>
												<td class="jfp_align_right"> 44,995 </td>
												<td class="jfp_align_right">61%</td>
											</tr>
											<tr>
												<td class="jfp_align_right total_style"> TOTAL CURRENT LIABILITIES </td>
												<td class="jfp_align_right total_style"> 756,460 </td>
												<td class="jfp_align_right total_style"> 642,231 </td>
												<td class="jfp_align_right total_style"> 114,229 </td>
												<td class="jfp_align_right total_style">18%</td>
											</tr>
											<tr>
												<td class="jfp_bold child_title_style">NON-CURRENT LIABILITIES</td>
												<td></td><td></td><td></td><td></td>
											</tr>
											<tr>
												<td> LOAN PAYABLE </td>
												<td class="jfp_align_right"> 28,000,000 </td>
												<td class="jfp_align_right"> 33,000,000 </td>
												<td class="jfp_align_right"> (5,000,000)</td>
												<td class="jfp_align_right">-15%</td>
											</tr>
											<tr>
												<td> RENTAL DEPOSITS </td>
												<td class="jfp_align_right"> 70,000 </td>
												<td class="jfp_align_right"> 70,000 </td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right">0%</td>
											</tr>
											<tr>
												<td class="jfp_align_right total_style"> TOTAL NON-CURRENT LIABILITIES </td>
												<td class="jfp_align_right total_style"> 28,070,000 </td>
												<td class="jfp_align_right total_style"> 33,070,000 </td>
												<td class="jfp_align_right total_style"> (5,000,000)</td>
												<td class="jfp_align_right total_style">-15%</td>
											</tr>
											<tr>
												<td class="jfp_txt_center jfp_bold parent_title_head">TOTAL LIABILITIES</td>
												<td class="jfp_align_right jfp_bold parent_title_head"> 28,826,460 </td>
												<td class="jfp_align_right jfp_bold parent_title_head"> 33,712,231 </td>
												<td class="jfp_align_right jfp_bold parent_title_head"> (4,885,771)</td>
												<td class="jfp_align_right jfp_bold parent_title_head">-14%</td>
											</tr>
											<tr>
												<td class="jfp_bold child_title_style"> STOCKHOLDERS' EQUITY </td>
												<td></td><td></td><td></td><td></td>
											</tr>
											<tr>
												<td> CAPITAL STOCK </td>
												<td class="jfp_align_right"> 26,000,000 </td>
												<td class="jfp_align_right"> 26,000,000 </td>
												<td class="jfp_align_right"> - </td>
												<td class="jfp_align_right">0%</td>
											</tr>
											<tr>
												<td> RETAINED EARNINGS </td>
												<td class="jfp_align_right"> 593,941 </td>
												<td class="jfp_align_right"> (18,288) </td>
												<td class="jfp_align_right"> 612,229 </td>
												<td class="jfp_align_right">-3348%</td>
											</tr>
											<tr>
												<td> TOTAL EQUITY </td>
												<td class="jfp_align_right"> 26,593,941 </td>
												<td class="jfp_align_right"> 25,981,712 </td>
												<td class="jfp_align_right"> 612,229 </td>
												<td class="jfp_align_right">2%</td>
											</tr>
											<tr>
												<td class="jfp_txt_center jfp_bold parent_title_head"> TOTAL LIABILITIES AND STOCKHOLDERS' EQUITY </td>
												<td class="jfp_align_right jfp_bold parent_title_head"> 55,420,401 </td>
												<td class="jfp_align_right jfp_bold parent_title_head"> 59,693,943 </td>
												<td class="jfp_align_right jfp_bold parent_title_head"> (4,273,542)</td>
												<td class="jfp_align_right jfp_bold parent_title_head">-7%</td>
											</tr>
										</table>
										<hr>
			                        	<div class="jfp_txt_center">
				                        	<h4>H & R BUSINESS DEV INC</h4>
				                        	<h4>BALANCE SHEET</h4>
				                        	<h4>DECEMBER 31, 2014</h4>
										</div><br>
										<div class="table_container">
											<table class="table_style" cellpadding="5" border="1">
												<tr>
													<td class="jfp_bold child_title_style">CURRENT ASSET</td>
													<td class="jfp_txt_center jfp_bold thead_style">2014</td>
													<td class="jfp_txt_center jfp_bold thead_style">2013</td>
												</tr>
												<tr>
													<td>Cash In Bank</td>
													<td class="jfp_align_right"> 4,310,651 </td>
													<td class="jfp_align_right"> 5,832,325 </td>
												</tr>
												<tr>
													<td> Petty Cash Fund </td>
													<td class="jfp_align_right"> 20,000 </td>
													<td class="jfp_align_right"> 20,000 </td>
												</tr>
												<tr>
													<td> Change Fund </td>
													<td class="jfp_align_right"> 20,000 </td>
													<td class="jfp_align_right"> 20,000 </td>
												</tr>
												<tr>
													<td> Deposit Rental - Leasehold </td>
													<td class="jfp_align_right"> 1,080,000 </td>
													<td class="jfp_align_right"> 1,080,000 </td>
												</tr>
												<tr>
													<td> Deposit Water - Maynilad </td>
													<td class="jfp_align_right"> 60,830 </td>
													<td class="jfp_align_right"> 60,830 </td>
												</tr>
												<tr>
													<td> Investment - Meralco </td>
													<td class="jfp_align_right"> 505,980 </td>
													<td class="jfp_align_right"> 505,980 </td>
												</tr>
												<tr>
													<td> Bill Deposit Meralco </td>
													<td class="jfp_align_right"> 893,929 </td>
													<td class="jfp_align_right"> 534,644 </td>
												</tr>
												<tr>
													<td> Meter Deposit Meralco </td>
													<td class="jfp_align_right"> 29,000 </td>
													<td class="jfp_align_right"> 29,000 </td>
												</tr>
												<tr>
													<td> Merchandise Inventory </td>
													<td class="jfp_align_right"> 93,303 </td>
													<td class="jfp_align_right"> 82,762 </td>
												</tr>
												<tr>
													<td> Accounts Receivable - Salon </td>
													<td class="jfp_align_right"> - </td>
													<td class="jfp_align_right"> - </td>
												</tr>
												<tr>
													<td> Accounts Receivable - Credit Card </td>
													<td class="jfp_align_right"> 166,239 </td>
													<td class="jfp_align_right"> 289,466 </td>
												</tr>
												<tr>
													<td> Accounts Receivable - Creditable Tax Withheld at Source </td>
													<td class="jfp_align_right"> (0)</td>
													<td class="jfp_align_right"> - </td>
												</tr>
												<tr>
													<td> Accounts Receivable - SSS </td>
													<td class="jfp_align_right"> 2,379 </td>
													<td class="jfp_align_right"> 2,730 </td>
												</tr>
												<tr>
													<td> Accounts Receivable - Creditable Input Tax </td>
													<td class="jfp_align_right"> (0)</td>
													<td class="jfp_align_right"> 24,839 </td>
												</tr>
												<tr>
													<td> Accounts Receivable - BIR Withholding Tax </td>
													<td class="jfp_align_right"> 13,251 </td>
													<td class="jfp_align_right"> - </td>
												</tr>
												<tr>
													<td class="jfp_align_right total_style">TOTAL</td>
													<td class="jfp_align_right total_style"> 7,195,561 </td>
													<td class="jfp_align_right total_style"> 8,482,576 </td>
												</tr>
												<tr><td colspan="3"></td></tr>
												<tr>
													<td> Property Plant & Equipment </td>
													<td class="jfp_align_right"> - </td>
													<td class="jfp_align_right"> - </td>
												</tr>
												<tr>
													<td> Building </td>
													<td class="jfp_align_right"> 31,650,081 </td>
													<td class="jfp_align_right"> 31,623,718 </td>
												</tr>
												<tr>
													<td> Building Equipment </td>
													<td class="jfp_align_right"> 785,580 </td>
													<td class="jfp_align_right"> 785,580 </td>
												</tr>
												<tr>
													<td> Transport Vehicle </td>
													<td class="jfp_align_right"> 1,057,143 </td>
													<td class="jfp_align_right"> 1,057,143 </td>
												</tr>
												<tr>
													<td> Building Banquet Utensils </td>
													<td class="jfp_align_right"> - </td>
													<td class="jfp_align_right"> - </td>
												</tr>
												<tr>
													<td> Building Furniture </td>
													<td class="jfp_align_right"> 107,589 </td>
													<td class="jfp_align_right"> 107,589 </td>
												</tr>
												<tr>
													<td> Building & Building Improvement </td>
													<td class="jfp_align_right"> 28,661,128 </td>
													<td class="jfp_align_right"> 28,525,504 </td>
												</tr>
												<tr>
													<td> Office Equipment </td>
													<td class="jfp_align_right"> 256,357 </td>
													<td class="jfp_align_right"> 238,951 </td>
												</tr>
												<tr>
													<td> Furniture & Fixtures </td>
													<td class="jfp_align_right"> 947,875 </td>
													<td class="jfp_align_right"> 947,875 </td>
												</tr>
												<tr>
													<td> Hotel Equipment </td>
													<td class="jfp_align_right"> 6,647,525 </td>
													<td class="jfp_align_right"> 6,326,425 </td>
												</tr>
												<tr>
													<td> Kitchen Equipment </td>
													<td class="jfp_align_right"> 1,139,156 </td>
													<td class="jfp_align_right"> 1,139,156 </td>
												</tr>
												<tr>
													<td> Housekeeping Equipment </td>
													<td class="jfp_align_right"> - </td>
													<td class="jfp_align_right"> - </td>
												</tr>
												<tr>
													<td> Kitchen Wares </td>
													<td class="jfp_align_right"> 3,518 </td>
													<td class="jfp_align_right"> - </td>
												</tr>
												<tr>
													<td> Tools </td>
													<td class="jfp_align_right"> - </td>
													<td class="jfp_align_right"> - </td>
												</tr>
												<tr>
													<td> Software System </td>
													<td class="jfp_align_right"> 115,077 </td>
													<td class="jfp_align_right"> 115,077 </td>
												</tr>
												<tr>
													<td> Sub-Total </td>
													<td class="jfp_align_right"> 71,371,029 </td>
													<td class="jfp_align_right"> 70,867,018 </td>
												</tr>
												<tr>
													<td>Less:</td>
													<td class="jfp_align_right"></td>
													<td class="jfp_align_right"></td>
												</tr>
												<tr>
													<td> Accumulated Depreciation </td>
													<td class="jfp_align_right"> 24,315,505 </td>
													<td class="jfp_align_right"> 20,659,040 </td>
												</tr>
												<tr>
													<td class="jfp_align_right total_style"> TOTAL </td>
													<td class="jfp_align_right total_style"> 47,055,525 </td>
													<td class="jfp_align_right total_style"> 50,207,978 </td>
												</tr>
												<tr><td colspan="3"></td></tr>
												<tr>
													<td>Other asset</td>
													<td class="jfp_align_right"> - </td>
													<td class="jfp_align_right"> - </td>
												</tr>
												<tr>
													<td> Prepaid Insurance Expense </td>
													<td class="jfp_align_right"> 27,370 </td>
													<td class="jfp_align_right"> 20,969 </td>
												</tr>
												<tr>
													<td> Deferred Charges </td>
													<td class="jfp_align_right"> 1,141,946 </td>
													<td class="jfp_align_right"> 982,420 </td>
												</tr>
												<tr>
													<td class="jfp_align_right total_style">TOTAL</td>
													<td class="jfp_align_right total_style"> 1,169,316 </td>
													<td class="jfp_align_right total_style"> 1,003,389 </td>
												</tr>
												<tr>
													<td class="jfp_txt_center jfp_bold parent_title_head">TOTAL ASSET</td>
													<td class="jfp_align_right jfp_bold parent_title_head"> 55,420,402 </td>
													<td class="jfp_align_right jfp_bold parent_title_head"> 59,693,943 </td>
												</tr>
												<tr>
													<td class="child_title_style"> Liabilities & Stockholders Equity </td>
													<td class="jfp_align_right"></td>
													<td class="jfp_align_right"></td>
												</tr>
												<tr>
													<td> Accrued Expense - 13th Month Pay </td>
													<td class="jfp_align_right"> - </td>
													<td class="jfp_align_right"> - </td>
												</tr>
												<tr>
													<td> Withholding Tax Payable </td>
													<td class="jfp_align_right"> - </td>
													<td class="jfp_align_right"> - </td>
												</tr>
												<tr>
													<td> Expanded Tax Payable </td>
													<td class="jfp_align_right"> 39,331 </td>
													<td class="jfp_align_right"> 42,430 </td>
												</tr>
												<tr>
													<td> Philhealth Payable </td>
													<td class="jfp_align_right"> 33,800 </td>
													<td class="jfp_align_right"> 29,725 </td>
												</tr>
												<tr>
													<td> Pag-Ibig Premium Payable </td>
													<td class="jfp_align_right"> 23,250 </td>
													<td class="jfp_align_right"> 20,950 </td>
												</tr>
												<tr>
													<td> Pag-Ibig Loan Payable </td>
													<td class="jfp_align_right"> 30,238 </td>
													<td class="jfp_align_right"> 32,188 </td>
												</tr>
												<tr>
													<td> SSS Premium Payable </td>
													<td class="jfp_align_right"> 153,980 </td>
													<td class="jfp_align_right"> 123,662 </td>
												</tr>
												<tr>
													<td> SSS Loan Payable </td>
													<td class="jfp_align_right"> 23,322 </td>
													<td class="jfp_align_right"> 19,246 </td>
												</tr>
												<tr>
													<td> Income Tax Payable </td>
													<td class="jfp_align_right"> 118,633 </td>
													<td class="jfp_align_right"> 73,638 </td>
												</tr>
												<tr>
													<td> Vat Payable </td>
													<td class="jfp_align_right"> 333,906 </td>
													<td class="jfp_align_right"> 263,359 </td>
												</tr>
												<tr>
													<td> Rental Deposit </td>
													<td class="jfp_align_right"> 70,000 </td>
													<td class="jfp_align_right"> 70,000 </td>
												</tr>
												<tr>
													<td> Construction Payable </td>
													<td class="jfp_align_right"> - </td>
													<td class="jfp_align_right"> - </td>
												</tr>
												<tr>
													<td> Accounts Payable - EWB </td>
													<td class="jfp_align_right"> - </td>
													<td class="jfp_align_right"> - </td>
												</tr>
												<tr><td colspan="3"></td></tr>
												<tr>
													<td> Loan Payable - BDO </td>
													<td class="jfp_align_right"> 28,000,000 </td>
													<td class="jfp_align_right"> 33,000,000 </td>
												</tr>
												<tr>
													<td></td>
													<td class="jfp_align_right"> 28,826,460 </td>
													<td class="jfp_align_right"> 33,712,231 </td>
												</tr>
												<tr><td colspan="3"></td></tr>
												<tr>
													<td> Capital Stock </td>
													<td class="jfp_align_right"> 26,000,000 </td>
													<td class="jfp_align_right"> 26,000,000 </td>
												</tr>
												<tr>
													<td> Increase in  Capital Stock - Paid Up Deposit </td>
													<td class="jfp_align_right"> - </td>
													<td class="jfp_align_right"> - </td>
												</tr>
												<tr>
													<td> Retained Earnings </td>
													<td class="jfp_align_right"> 593,941 </td>
													<td class="jfp_align_right"> (18,288)</td>
												</tr>
												<tr>
													<td></td>
													<td class="jfp_align_right"> 26,593,941 </td>
													<td class="jfp_align_right"> 25,981,712 </td>
												</tr>
												<tr>
													<td class="jfp_txt_center jfp_bold parent_title_head"> TOTAL LIABILITIES & STOCKHOLDERS </td>
													<td class="jfp_align_right jfp_bold parent_title_head"> 55,420,402 </td>
													<td class="jfp_align_right jfp_bold parent_title_head"> 59,693,943 </td>
												</tr>
												<tr>
													<td></td>
													<td class="jfp_align_right">(0)</td>
													<td class="jfp_align_right"> - </td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</div>
                        </div>
                     </div>
               	</div>
					
					<!-- END EXAMPLE TABLE PORTLET-->

			<!-- END PAGE CONTENT-->
		</div>
		<!-- END PAGE CONTAINER-->	
	</div>
	<!-- END PAGE -->	 	


@endsection
@section('customjs')
@endsection

