@extends('layouts.base')
@section('content')
<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Statement of Financial Position
						</h3>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
                        	<div class="portlet box green">
								<div class="portlet-title">
									<h4><i class="icon-credit-card"></i>Monthly</h4>
								</div>
								<div class="portlet-body">
		                        	<div class="row-fluid">
										{{ Form::open(['url' => 'balancesheetmonthly', 'method' => 'get']) }}
		                        		<div class="span6 filter_cont">
		                        			<span class="as_of"> As of </span>
											{{ Form::selectMonth('month', $month, ['class' => 'span3']) }}
											{{ Form::selectRange('year', [], $year, ['class' => 'span3']) }}
		                                    <input type="submit" class="btn blue as_of_go" value="Go">
		                        		</div>
										{{ Form::close() }}
		                        	</div>
		                        	<div class="jfp_txt_center">
			                        	<h4>H & R BUSINESS DEV INC</h4>
			                        	<h4>Statement of Financial Position</h4>
			                        	<h4>AS OF JANUARY 2016</h4>
									</div><br>
									<div class="table_container">
										<table class="table_style" cellpadding="5" border="1">
											<tr>
												<td class="jfp_txt_center jfp_bold parent_title_head">ASSET</td>
												<td class="jfp_txt_center jfp_bold thead_style">BEGINNING</td>
												<td class="jfp_txt_center jfp_bold thead_style">MONTH TO DATE</td>
												<td class="jfp_txt_center jfp_bold thead_style">YEAR TO DATE</td>
											</tr>
											<tr>
												<td class="jfp_bold child_title_style">CURRENT ASSET</td>
												<td></td><td></td><td></td>
											</tr>
											<?php $totCABB = $totCACB = $totCAYTD = 0; ?>
											@foreach($coa as $c)
												@if($c->classification == 'CURRENT ASSETS')
													<tr>
														<td>{{$c->code}} {{$c->name}}</td>
														@if(isset($bb) && isset($bb[$c->code]))
														<?php
															$begBal = isset($bb[$c->code]['bb']) ? $bb[$c->code]['bb'] : 0;
															$totCABB = $totCABB + $begBal;
															$totCACB = $totCACB + $bb[$c->code]['cb'];
															$totCAYTD = $totCAYTD + $bb[$c->code]['ytd'];
														?>

														<td class="jfp_align_right">
														 	{!!  number_format($begBal, 2) !!}
														</td>
														<td class="jfp_align_right">
															{!!  number_format($bb[$c->code]['cb'], 2) !!}
														</td>
														<td class="jfp_align_right">
															{!!  number_format($bb[$c->code]['ytd'], 2) !!}

														</td>
														@else
															<td class="jfp_align_right"> 0 </td>
															<td class="jfp_align_right"> 0 </td>
															<td class="jfp_align_right"> 0 </td>
														@endif
													</tr> 
												@endif
											@endforeach
											<tr>
												<td class="jfp_align_right total_style">TOTAL CURRENT ASSET</td>
												<td class="jfp_align_right total_style"> {!! number_format($totCABB, 2) !!} </td>
												<td class="jfp_align_right total_style"> {!! number_format($totCACB, 2) !!} </td>
												<td class="jfp_align_right total_style"> {!! number_format($totCAYTD, 2) !!} </td>
											</tr>
											<tr>
												<td class="jfp_bold child_title_style"> NON CURRENT ASSET </td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<?php $totNCABB = $totNCACB = $totNCAYTD = 0; ?>
											@foreach($coa as $c)
												@if($c->classification == 'NON-CURRENT ASSETS')
													<tr>
														<td>{{$c->code}} {{$c->name}}</td>
														@if(isset($bb[$c->code]))
															<?php
															$begBal = isset($bb[$c->code]['bb']) ? $bb[$c->code]['bb'] :0;
															$totNCABB = $totNCABB + $begBal;
															$totNCACB = $totNCACB + $bb[$c->code]['cb'];
															$totNCAYTD = $totNCAYTD + $bb[$c->code]['ytd'];
															?>

															<td class="jfp_align_right">
																{!!  number_format($begBal, 2) !!}
															</td>
															<td class="jfp_align_right">
																{!!  number_format($bb[$c->code]['cb'], 2) !!}
															</td>
															<td class="jfp_align_right">
																{!!  number_format($bb[$c->code]['ytd'], 2) !!}

															</td>
														@else
															<td class="jfp_align_right"> 0 </td>
															<td class="jfp_align_right"> 0 </td>
															<td class="jfp_align_right"> 0 </td>
														@endif
													</tr>
												@endif
											@endforeach

											<tr>
												<td class="jfp_align_right total_style">SUB-TOTAL</td>
												<td class="jfp_align_right total_style"> {!! number_format($totNCABB, 2) !!} </td>
												<td class="jfp_align_right total_style"> {!! number_format($totNCACB, 2) !!} </td>
												<td class="jfp_align_right total_style"> {!! number_format($totNCAYTD, 2) !!} </td>
											</tr>
											<tr>
												<td class="jfp_align_right total_style"> Accumulated Depreciation </td>
												<td class="jfp_align_right total_style"> 20,659,040.00 </td>
												<td class="jfp_align_right total_style"> 299,276.20 </td>
												<td class="jfp_align_right total_style"> 20,958,316.20 </td>
											</tr>
											<tr>
												<td class="jfp_align_right total_style"> PROPERTY & EQUIPMENT  - NET </td>
												<td class="jfp_align_right total_style"> 50,207,978.00 </td>
												<td class="jfp_align_right total_style"> (146,068.17) </td>
												<td class="jfp_align_right total_style"> 50,061,909.83 </td>
											</tr>
											<tr>
												<td class="jfp_txt_center jfp_bold parent_title_head">ASSET</td>
												<td class="jfp_align_right total_style"> {!! number_format($totCABB + $totNCABB, 2) !!} </td>
												<td class="jfp_align_right total_style"> {!! number_format($totCACB + $totNCACB, 2) !!} </td>
												<td class="jfp_align_right total_style"> {!! number_format($totCAYTD + $totNCAYTD, 2) !!} </td>
											</tr>
											<tr>
												<td colspan="4"></td>
											</tr>
											<tr>
												<td colspan="4"></td>
											</tr>
											<tr>
												<td class="jfp_txt_center jfp_bold parent_title_head"> LIABILITIES AND STOCKHOLDERS' EQUITY </td>
												<td class="jfp_txt_center jfp_bold thead_style">BEGINNING</td>
												<td class="jfp_txt_center jfp_bold thead_style">MONTH TO DATE</td>
												<td class="jfp_txt_center jfp_bold thead_style">YEAR TO DATE</td>
											</tr>
											<tr>
												<td class="jfp_bold child_title_style">CURRENT LIABILITIES</td>
												<td></td><td></td><td></td>
											</tr>
											<?php $totCLBB = $totCLCB = $totCLYTD = 0; ?>
											@foreach($coa as $c)
												@if($c->classification == 'CURRENT LIABILITIES')
													<tr>
														<td>{{$c->code}} {{$c->name}}</td>
														@if(isset($bb[$c->code]))
															<?php

															$begBal = isset($bb[$c->code]['bb']) ? $bb[$c->code]['bb'] :0;
															$totCLBB = $totCLBB + $begBal;
															$totCLCB = $totCLCB + $bb[$c->code]['cb'];
															$totCLYTD = $totCLYTD + $bb[$c->code]['ytd'];
															?>

															<td class="jfp_align_right">
																{!!  number_format($begBal, 2) !!}
															</td>
															<td class="jfp_align_right">
																{!!  number_format($bb[$c->code]['cb'], 2) !!}
															</td>
															<td class="jfp_align_right">
																{!!  number_format($bb[$c->code]['ytd'], 2) !!}

															</td>
														@else
															<td class="jfp_align_right"> 0 </td>
															<td class="jfp_align_right"> 0 </td>
															<td class="jfp_align_right"> 0 </td>
														@endif
													</tr>
												@endif
											@endforeach

											<tr>
												<td class="jfp_align_right total_style">TOTAL CURRENT LIABILITIES</td>
												<td class="jfp_align_right total_style"> {!! number_format($totCLBB, 2) !!} </td>
												<td class="jfp_align_right total_style"> {!! number_format($totCLCB, 2) !!} </td>
												<td class="jfp_align_right total_style"> {!! number_format($totCLYTD, 2) !!} </td>
											</tr>
											<tr>													
												<td class="jfp_bold child_title_style">NON-CURRENT LIABILITIES</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<?php $totNCLBB = $totNCLCB = $totNCLYTD = 0; ?>
											@foreach($coa as $c)
												@if($c->classification == 'NON-CURRENT LIABILITIES')
													<tr>
														<td>{{$c->code}} {{$c->name}}</td>
														@if(isset($bb[$c->code]))
															<?php
															$begBal = isset($bb[$c->code]['bb']) ? $bb[$c->code]['bb'] :0;
															$totNCLBB = $totNCLBB + $begBal;
															$totNCLCB = $totNCLCB + $bb[$c->code]['cb'];
															$totNCLYTD = $totNCLYTD + $bb[$c->code]['ytd'];
															?>

															<td class="jfp_align_right">
																{!!  number_format($begBal, 2) !!}
															</td>
															<td class="jfp_align_right">
																{!!  number_format($bb[$c->code]['cb'], 2) !!}
															</td>
															<td class="jfp_align_right">
																{!!  number_format($bb[$c->code]['ytd'], 2) !!}

															</td>
														@else
															<td class="jfp_align_right"> 0 </td>
															<td class="jfp_align_right"> 0 </td>
															<td class="jfp_align_right"> 0 </td>
														@endif
													</tr>
												@endif
											@endforeach
											<tr>
												<td class="jfp_align_right total_style">TOTAL NON-CURRENT LIABILITIES </td>
												<td class="jfp_align_right total_style"> {!! number_format($totNCLBB, 2) !!} </td>
												<td class="jfp_align_right total_style"> {!! number_format($totNCLCB, 2) !!} </td>
												<td class="jfp_align_right total_style"> {!! number_format($totNCLYTD, 2) !!} </td>
											</tr>
											<tr>
												<td class="jfp_txt_center jfp_bold parent_title_head"> TOTAL LIABILITIES </td>
												<td class="jfp_align_right total_style"> {!! number_format($totCLBB + $totNCLBB, 2) !!} </td>
												<td class="jfp_align_right total_style"> {!! number_format($totCLCB + $totNCLCB, 2) !!} </td>
												<td class="jfp_align_right total_style"> {!! number_format($totCLYTD + $totNCLYTD, 2) !!} </td>
											</tr>
											<tr>
												<td colspan="4"></td>
											</tr>
											<tr>
												<td colspan="4"></td>
											</tr>
											<tr>
												<td class="jfp_txt_center jfp_bold parent_title_head">  STOCKHOLDERS' EQUITY </td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
											<?php $totEBB = $totECB = $totEYTD = 0; ?>

											@foreach($coa as $c)
												@if($c->classification == 'STOCKHOLDERS EQUITY')
													<tr>
														<td>{{$c->code}} {{$c->name}}</td>
														@if(isset($bb[$c->code]))
															<?php
															$begBal = isset($bb[$c->code]['bb']) ? $bb[$c->code]['bb'] :0;
															$totEBB = $totEBB + $begBal;
															$totECB = $totECB + $bb[$c->code]['cb'];
															$totEYTD = $totEYTD + $bb[$c->code]['ytd'];
															?>

															<td class="jfp_align_right">
																{!!  number_format($begBal, 2) !!}
															</td>
															<td class="jfp_align_right">
																{!!  number_format($bb[$c->code]['cb'], 2) !!}
															</td>
															<td class="jfp_align_right">
																{!!  number_format($bb[$c->code]['ytd'], 2) !!}

															</td>
														@else
															<td class="jfp_align_right"> 0 </td>
															<td class="jfp_align_right"> 0 </td>
															<td class="jfp_align_right"> 0 </td>
														@endif
													</tr>
												@endif
											@endforeach
											<tr>
												<td class="jfp_align_right total_style">TOTAL EQUITY </td>
												<td class="jfp_align_right total_style"> {!! number_format($totEBB, 2) !!} </td>
												<td class="jfp_align_right total_style"> {!! number_format($totECB, 2) !!} </td>
												<td class="jfp_align_right total_style"> {!! number_format($totEYTD, 2) !!} </td>
											</tr>
											<tr>
												<td class="jfp_txt_center jfp_bold parent_title_head"> TOTAL LIABILITIES AND STOCKHOLDERS EQUITY </td>
												<td class="jfp_align_right total_style"> {!! number_format($totCLBB + $totNCLBB + $totEBB, 2) !!} </td>
												<td class="jfp_align_right total_style"> {!! number_format($totCLCB + $totNCLCB + $totECB, 2) !!} </td>
												<td class="jfp_align_right total_style"> {!! number_format($totCLYTD + $totNCLYTD + $totEYTD, 2) !!} </td>
											</tr>
											<tr>
												<td></td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"> (0.00)</td>
												<td class="jfp_align_right"> (0.00)</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
				<br>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	 

@endsection
@section('customjs')
@endsection

