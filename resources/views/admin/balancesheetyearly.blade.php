@extends('layouts.base')
@section('content')

	<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Statement of Financial Position
						</h3>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
                        	<div class="portlet box green">
								<div class="portlet-title">
									<h4><i class="icon-credit-card"></i>Yearly</h4>
								</div>
								<div class="portlet-body">
		                        	<div class="row-fluid">
		                        	{{ Form::open(['method' => 'get', 'url' => 'balancesheetyearly']) }}
		                        		<div class="span6 filter_cont">
		                        			<span class="as_of"> As of </span>
		                        			{{ Form::selectRange('year', $minYear , $maxYear, $year) }}
		                                    <span class="and"> and {{ $year-1 }} </span>
		                                    <input type="submit" class="btn blue as_of_go" value="Go">
		                        		</div>
		                        	{{ Form::close() }}
		                        	</div>
		                        	<div class="jfp_txt_center">
			                        	<h4>H & R BUSINESS DEV INC</h4>
			                        	<h4>Statement of Financial Position</h4>
			                        	<h4>As of DECEMBER 31, {{ $year }} AND {!! $year - 1 !!}</h4>
									</div><br>
									<div class="table_container">
										<table class="table_style" cellpadding="5" border="1">
											<tr>
												<td rowspan="2" class="jfp_txt_center jfp_bold parent_title_head">ASSET</td>
												<td rowspan="2" class="jfp_txt_center jfp_bold thead_style">{{ $year }}</td>
												<td rowspan="2" class="jfp_txt_center jfp_bold thead_style">{{ $year -1  }}</td>
												<td colspan="2" class="jfp_txt_center jfp_bold thead_style">RATIO</td>
											</tr>
											<tr>
												<td class="jfp_txt_center jfp_bold thead_style">AMT INC</td>
												<td class="jfp_txt_center jfp_bold thead_style">% INC</td>
											</tr>
											<tr>
												<td class="jfp_bold child_title_style">CURRENT ASSET</td>
												<td></td><td></td><td></td><td></td>
											</tr>
											@foreach($coa as $c)
											@if($c->classification == 'CURRENT ASSETS')
											<tr>
												<td>{{$c->code}} {{$c->name}}</td>
												<td class="jfp_align_right"> {{ $c->thisYear }} </td>
												<td class="jfp_align_right"> {{ $c->lastYear }} </td>
												<td class="jfp_align_right"> {{ $c->diff }} </td>
												<td class="jfp_align_right"> {{ $c->pctDiff }}%</td>
											</tr>
											@endif
											@endforeach
											<tr>
												<td class="jfp_align_right total_style">TOTAL CURRENT ASSETS </td>
												<td class="jfp_align_right total_style"> 
													{{ $agg['CURRENT ASSETS']['thisYear'] }} 
												</td>
												<td class="jfp_align_right total_style"> 
													{{ $agg['CURRENT ASSETS']['lastYear'] }} 
												</td>
												<td class="jfp_align_right total_style"> 
													{{ $agg['CURRENT ASSETS']['thisYear'] - $agg['CURRENT ASSETS']['lastYear'] }} 
												</td>
												<td class="jfp_align_right total_style">
													@if($agg['CURRENT ASSETS']['thisYear'] > 0) 
													{{ (100 * ($agg['CURRENT ASSETS']['thisYear'] - $agg['CURRENT ASSETS']['lastYear'])) / $agg['CURRENT ASSETS']['thisYear']  }} 
													@else
														0
													@endif
													%
												</td>
											</tr>
											<tr>
												<td class="jfp_bold child_title_style">NON-CURRENT ASSET</td>
												<td></td><td></td><td></td><td></td>
											</tr>
											@foreach($coa as $c)
											@if($c->classification == 'NON-CURRENT ASSETS')
											<tr>
												<td>{{$c->code}} {{$c->name}}</td>
												<td class="jfp_align_right"> {{ $c->thisYear }} </td>
												<td class="jfp_align_right"> {{ $c->lastYear }} </td>
												<td class="jfp_align_right"> {{ $c->diff }} </td>
												<td class="jfp_align_right"> {{ $c->pctDiff }}%</td>
											</tr>
											@endif
											@endforeach
											<tr>
												<td class="jfp_align_right total_style">TOTAL NON-CURRENT ASSETS</td>
												<td class="jfp_align_right total_style"> 
													{{ $agg['NON-CURRENT ASSETS']['thisYear'] }} 
												</td>
												<td class="jfp_align_right total_style"> 
													{{ $agg['NON-CURRENT ASSETS']['lastYear'] }} 
												</td>
												<td class="jfp_align_right total_style"> 
													{{ $agg['NON-CURRENT ASSETS']['thisYear'] - $agg['NON-CURRENT ASSETS']['lastYear'] }} 
												</td>
												<td class="jfp_align_right total_style">
													@if($agg['NON-CURRENT ASSETS']['thisYear'] > 0) 
													{{ (100 * ($agg['NON-CURRENT ASSETS']['thisYear'] - $agg['NON-CURRENT ASSETS']['lastYear'])) / $agg['NON-CURRENT ASSETS']['thisYear']  }} 
													@else
														0
													@endif
													%
												</td>
											</tr>
											<?php
												$thisYear = $agg['CURRENT ASSETS']['thisYear'] + $agg['NON-CURRENT ASSETS']['thisYear'];
												$lastYear = $agg['CURRENT ASSETS']['lastYear'] +  $agg['NON-CURRENT ASSETS']['lastYear'];

												$diff = $thisYear - $lastYear;

												$pctDiff = 0;
												if ($thisYear > 0) {
													$pctDiff = (100 * ($thisYear - $lastYear)) / $thisYear;
												}
												
 											?>
											<tr>
												<td class="jfp_txt_center jfp_bold parent_title_head">TOTAL ASSETS</td>
												<td class="jfp_align_right total_style"> 
													{{ $thisYear }} 
												</td>
												<td class="jfp_align_right total_style"> 
													{{ $lastYear }} 
												</td>
												<td class="jfp_align_right total_style"> 
													{{ $diff }} 
												</td>
												<td class="jfp_align_right total_style">
													{{ $pctDiff }}
												</td>
											</tr>
											<tr><td colspan="5"></td></tr>
											<tr><td colspan="5"></td></tr>
											<tr>
												<td class="jfp_txt_center jfp_bold parent_title_head"> LIABILITIES AND STOCKHOLDERS' EQUITY </td>
												<td></td><td></td><td></td><td></td>
											</tr>
											<tr>
												<td class="jfp_bold child_title_style">CURRENT LIABILITIES</td>
												<td></td><td></td><td></td><td></td>
											</tr>
											@foreach($coa as $c)
											@if($c->classification == 'CURRENT LIABILITIES')
											<tr>
												<td>{{$c->code}} {{$c->name}}</td>
												<td class="jfp_align_right"> {{ $c->thisYear }} </td>
												<td class="jfp_align_right"> {{ $c->lastYear }} </td>
												<td class="jfp_align_right"> {{ $c->diff }} </td>
												<td class="jfp_align_right"> {{ $c->pctDiff }}%</td>
											</tr>
											@endif
											@endforeach
											<tr>
												<td class="jfp_align_right total_style"> TOTAL CURRENT LIABILITIES </td>
												<td class="jfp_align_right total_style"> 
													{{ $agg['CURRENT LIABILITIES']['thisYear'] }} 
												</td>
												<td class="jfp_align_right total_style"> 
													{{ $agg['CURRENT LIABILITIES']['lastYear'] }} 
												</td>
												<td class="jfp_align_right total_style"> 
													{{ $agg['CURRENT LIABILITIES']['thisYear'] - $agg['CURRENT LIABILITIES']['lastYear'] }} 
												</td>
												<td class="jfp_align_right total_style">
													@if($agg['CURRENT LIABILITIES']['thisYear'] > 0) 
													{{ (100 * ($agg['CURRENT LIABILITIES']['thisYear'] - $agg['CURRENT LIABILITIES']['lastYear'])) / $agg['CURRENT LIABILITIES']['thisYear']  }} 
													@else
														0
													@endif
													%
												</td>
											</tr>
											<tr>
												<td class="jfp_bold child_title_style">NON-CURRENT LIABILITIES</td>
												<td></td><td></td><td></td><td></td>
											</tr>
											@foreach($coa as $c)
											@if($c->classification == 'NON-CURRENT LIABILITIES')
											<tr>
												<td>{{$c->code}} {{$c->name}}</td>
												<td class="jfp_align_right"> {{ $c->thisYear }} </td>
												<td class="jfp_align_right"> {{ $c->lastYear }} </td>
												<td class="jfp_align_right"> {{ $c->diff }} </td>
												<td class="jfp_align_right"> {{ $c->pctDiff }}%</td>
											</tr>
											@endif
											@endforeach
											<tr>
												<td class="jfp_align_right total_style"> TOTAL NON-CURRENT LIABILITIES </td>
												<td class="jfp_align_right total_style"> 
													{{ $agg['NON-CURRENT LIABILITIES']['thisYear'] }} 
												</td>
												<td class="jfp_align_right total_style"> 
													{{ $agg['NON-CURRENT LIABILITIES']['lastYear'] }} 
												</td>
												<td class="jfp_align_right total_style"> 
													{{ $agg['NON-CURRENT LIABILITIES']['thisYear'] - $agg['NON-CURRENT LIABILITIES']['lastYear'] }} 
												</td>
												<td class="jfp_align_right total_style">
													@if($agg['NON-CURRENT LIABILITIES']['thisYear'] > 0) 
													{{ (100 * ($agg['NON-CURRENT LIABILITIES']['thisYear'] - $agg['NON-CURRENT LIABILITIES']['lastYear'])) / $agg['NON-CURRENT LIABILITIES']['thisYear']  }} 
													@else
														0
													@endif
													%
												</td>
											</tr>
											<?php
												$thisYearLia = $agg['CURRENT LIABILITIES']['thisYear'] + $agg['NON-CURRENT LIABILITIES']['thisYear'];
												$lastYearLia = $agg['CURRENT LIABILITIES']['lastYear'] +  $agg['NON-CURRENT LIABILITIES']['lastYear'];

												$diffLia = $thisYearLia - $lastYearLia;

												$pctDiffLia = 0;
												if ($thisYearLia > 0) {
													$pctDiffLia = (100 * ($thisYearLia - $lastYearLia)) / $thisYearLia;
												}
												
 											?>
											<tr>
												<td class="jfp_txt_center jfp_bold parent_title_head">TOTAL LIABILITIES</td>
												<td class="jfp_align_right total_style"> 
													{{ $thisYearLia }} 
												</td>
												<td class="jfp_align_right total_style"> 
													{{ $lastYearLia }} 
												</td>
												<td class="jfp_align_right total_style"> 
													{{ $diffLia }} 
												</td>
												<td class="jfp_align_right total_style">
													{{ $pctDiffLia }}
												</td>
											</tr>
											<tr>
												<td class="jfp_bold child_title_style"> STOCKHOLDERS' EQUITY </td>
												<td></td><td></td><td></td><td></td>
											</tr>
											@foreach($coa as $c)
											@if($c->classification == 'STOCKHOLDERS EQUITY')
											<tr>
												<td>{{$c->code}} {{$c->name}}</td>
												<td class="jfp_align_right"> {{ $c->thisYear }} </td>
												<td class="jfp_align_right"> {{ $c->lastYear }} </td>
												<td class="jfp_align_right"> {{ $c->diff }} </td>
												<td class="jfp_align_right"> {{ $c->pctDiff }}%</td>
											</tr>
											@endif
											@endforeach
											<tr>
												<td class="jfp_align_right total_style"> TOTAL EQUITY </td>
												<td class="jfp_align_right total_style"> 
													{{ $agg['STOCKHOLDERS EQUITY']['thisYear'] }} 
												</td>
												<td class="jfp_align_right total_style"> 
													{{ $agg['STOCKHOLDERS EQUITY']['lastYear'] }} 
												</td>
												<td class="jfp_align_right total_style"> 
													{{ $agg['STOCKHOLDERS EQUITY']['thisYear'] - $agg['STOCKHOLDERS EQUITY']['lastYear'] }} 
												</td>
												<td class="jfp_align_right total_style">
													@if($agg['STOCKHOLDERS EQUITY']['thisYear'] > 0) 
													{{ (100 * ($agg['STOCKHOLDERS EQUITY']['thisYear'] - $agg['STOCKHOLDERS EQUITY']['lastYear'])) / $agg['STOCKHOLDERS EQUITY']['thisYear']  }} 
													@else
														0
													@endif
													%
												</td>
											</tr>
											<?php
												$thisYearLiaEq = $thisYearLia + $agg['STOCKHOLDERS EQUITY']['thisYear'];
												$lastYearLiaEq = $thisYearLia + $agg['STOCKHOLDERS EQUITY']['lastYear'];

												$diffLiaEq = $thisYearLiaEq - $lastYearLiaEq;

												$pctDiffLiaEq = 0;
												if ($thisYearLiaEq > 0) {
													$pctDiffLiaEq = (100 * $diffLiaEq) / $thisYearLiaEq;
												}
												
 											?>
											<tr>
												<td class="jfp_txt_center jfp_bold parent_title_head"> TOTAL LIABILITIES AND STOCKHOLDERS' EQUITY </td>
												<td class="jfp_align_right jfp_bold parent_title_head">
													{{ $thisYearLiaEq }}</td>
												<td class="jfp_align_right jfp_bold parent_title_head">
													{{ $lastYearLiaEq }}
												</td>
												<td class="jfp_align_right jfp_bold parent_title_head"> 
													{{ $diffLiaEq }}
												</td>
												<td class="jfp_align_right jfp_bold parent_title_head">
													{{ $pctDiffLiaEq }}
												</td>
											</tr>
										</table>

										</div>
									</div>
								</div>
							</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
				<br>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	

@endsection
@section('customjs')
@endsection

