@extends('layouts.base')
@section('content')
<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Statement of Comprehensive Income
						</h3>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
                        	<div class="portlet box green">
								<div class="portlet-title">
									<h4><i class="icon-credit-card"></i>Monthly</h4>
								</div>
								<div class="portlet-body">
									<div class="row-fluid">
										{{ Form::open(['url' => 'incomestatementmonthly', 'method' => 'get']) }}
										<div class="span6 filter_cont">
											<span class="as_of"> As of </span>
											{{ Form::selectMonth('month', $month, ['class' => 'span3']) }}
											{{ Form::selectRange('year', $minYear , $maxYear, $year) }}
											<input type="submit" class="btn blue as_of_go" value="Go">
										</div>
										{{ Form::close() }}
		                        	</div>
		                        	<div class="jfp_txt_center">
			                        	<h4>H & R BUSINESS DEV INC</h4>
			                        	<h4>STATEMENT OF COMPREHENSIVE INCOME</h4>
			                        	<h4>FOR THE MONTH ENDED JANUARY 2016</h4>
									</div><br>
									<div class="table_container">
										<table class="table_style" cellpadding="5" border="1">
											<tr>
												<td class="jfp_bold child_title_style">SERVICE INCOME</td>
												<td class="jfp_txt_center jfp_bold thead_style">MONTH TO DATE</td>
												<td class="jfp_txt_center jfp_bold thead_style">YEAR TO DATE</td>
											</tr>
											<tr>
												<td> Room sales </td>
												<td class="jfp_align_right">{{ $bb['4001']['cb'] }}</td>
												<td class="jfp_align_right">{{ $bb['4001']['ytd'] }}</td>
											</tr>
											<tr>
												<td> Rental Income </td>
												<td class="jfp_align_right">{{ $bb['4010']['cb'] }}</td>
												<td class="jfp_align_right">{{ $bb['4010']['ytd'] }}</td>
											</tr>
											<tr>
												<td> Kitchen Sales </td>
												<td class="jfp_align_right">{{ $bb['4002']['cb'] }}</td>
												<td class="jfp_align_right">{{ $bb['4002']['ytd'] }}</td>
											</tr>
											<tr>
												<td class="jfp_align_right total_style"> SERVICE INCOME </td>
												<td class="jfp_align_right total_style"> 4,034,997.32 </td>
												<td class="jfp_align_right total_style"></td>
											</tr>
											<tr>
												<td class="jfp_bold child_title_style"> LESS COST OF SERVICE </td>
												<td colspan="2"></td>
											</tr>
											<tr>
												<td>Inventory beginning</td>
												<td class="jfp_align_right"> 82,762.00 </td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td>Purchases</td>
												<td class="jfp_align_right"> 160,319.91 </td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td>Total Available</td>
												<td class="jfp_align_right"> 243,081.91 </td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td>Inventory End</td>
												<td class="jfp_align_right"> 93,148.95 </td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td class="jfp_align_right total_style"> COST OF KITCHEN SALES </td>
												<td class="jfp_align_right total_style"> 149,932.96 </td>
												<td class="jfp_align_right total_style"></td>
											</tr>
											<tr>
												<td>Salaries & Wages</td>
												<td class="jfp_align_right">{{ $bb['5004']['cb'] }}</td>
												<td class="jfp_align_right">{{ $bb['5004']['ytd'] }}</td>
											</tr>
											<tr>
												<td>13th month Pay</td>
												<td class="jfp_align_right">{{ $bb['5011']['cb'] }}</td>
												<td class="jfp_align_right">{{ $bb['5011']['ytd'] }}</td>
											</tr>
											<tr>
												<td>Depreciation</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td>Laundry</td>
												<td class="jfp_align_right">{{ $bb['5010']['cb'] }}</td>
												<td class="jfp_align_right">{{ $bb['5010']['ytd'] }}</td>
											</tr>
											<tr>
												<td>Supplies/Facilities</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td>Rental</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td class="jfp_txt_center jfp_bold parent_title_head"> GROSS PROFIT </td>
												<td class="jfp_align_right jfp_bold parent_title_head"> 3,885,064.36 </td>
												<td class="jfp_align_right jfp_bold parent_title_head"></td>
											</tr>
											<tr><td colspan="3"></td></tr>
											<tr>
												<td class="jfp_bold child_title_style">OPERATING EXPENSES</td>
												<td colspan="2"></td>
											</tr>
											<tr>
												<td>UTILITIES</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td>SALARIES AND WAGES</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td>INTEREST AND BANK CHARGES</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td>TAXES AND LICENSES</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td>SSS,PH & HDMF CONTRIBUTIONS</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td>SECURITY SERVICES</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td>REPAIRS AND MAINTENANCE</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td>RENTAL</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td>COMMUNICATIONS</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td>SUPPLIES</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td>DEPRECIATION</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td>TRANSPORTATION AND TRAVEL</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td>EMPLOYEE BENEFITS</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td>BILLBOARDS, SIGNBOARDS AND PLACARDS</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td>13TH MONTH PAY</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td>FINANCING CHARGES</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td>INSURANCE</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td>PROFESSIONAL FEE</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td>REPRESENTATION AND ENTERTAINMENT</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td>TRAININGS AND MEETINGS</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td>MISCELLANEOUS</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td class="jfp_txt_center jfp_bold parent_title_head">TOTAL OPERATING EXPENSES</td>
												<td class="jfp_align_right jfp_bold parent_title_head"></td>
												<td class="jfp_align_right jfp_bold parent_title_head"></td>
											</tr>
											<tr>
												<td class="jfp_bold child_title_style">INCOME FROM OPERATION</td>
												<td colspan="2"></td>
											</tr>
											<tr>
												<td>OTHER INCOME</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td>MISCELLANEOUS INCOME</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td>INTEREST INCOME</td>
												<td class="jfp_align_right"></td>
												<td class="jfp_align_right"></td>
											</tr>
											<tr>
												<td class="jfp_align_right total_style">Sub-Total</td>
												<td class="jfp_align_right total_style"></td>
												<td class="jfp_align_right total_style"></td>
											</tr>
											<tr>
												<td class="jfp_bold jfp_txt_center">INCOME BEFORE INCOME TAX</td>
												<td class="jfp_align_right jfp_bold"></td>
												<td class="jfp_align_right jfp_bold"></td>
											</tr>
											<tr>
												<td class="jfp_bold jfp_txt_center">PROVISION FOR INCOME TAX</td>
												<td class="jfp_align_right jfp_bold"></td>
												<td class="jfp_align_right jfp_bold"></td>
											</tr>
											<tr>
												<td class="jfp_txt_center jfp_bold parent_title_head">NET INCOME</td>
												<td class="jfp_align_right jfp_bold parent_title_head"></td>
												<td class="jfp_align_right jfp_bold parent_title_head"></td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
				<br>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	 	
@endsection
@section('customjs')
@endsection

