@extends('layouts.base')
@section('content')
	<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Statement of Comprehensive Income
						</h3>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
                    	<div class="portlet box green">
							<div class="portlet-title">
								<h4><i class="icon-credit-card"></i>Yearly</h4>
							</div>
							<div class="portlet-body">
								<div class="row-fluid">
									{{ Form::open(['method' => 'get', 'url' => 'incomestatementyearly']) }}
									<div class="span6 filter_cont">
										<span class="as_of"> As of </span>
										{{ Form::selectRange('year', $minYear , $maxYear, $year) }}
										<span class="and"> and {{ $year-1 }} </span>
										<input type="submit" class="btn blue as_of_go" value="Go">
									</div>
									{{ Form::close() }}
	                        	</div>
	                        	<div class="jfp_txt_center">
		                        	<h4>H & R BUSINESS DEV INC</h4>
		                        	<h4>STATEMENT OF COMPREHENSIVE INCOME</h4>
		                        	<h4>FOR THE YEAR ENDED DECEMBER 31, 2016 AND 2015</h4>
								</div><br>
								<div class="table_container">
									<table class="table_style" cellpadding="5" border="1">
										<tr>
											<td rowspan="2" class="jfp_txt_center jfp_bold parent_title_head">REVENUES</td>
											<td rowspan="2" colspan="2" class="jfp_txt_center jfp_bold thead_style">2016</td>
											<td rowspan="2" colspan="2" class="jfp_txt_center jfp_bold thead_style">2015</td>
											<td colspan="2" class="jfp_txt_center jfp_bold thead_style">RATIO</td>
										</tr>
										<tr>
											<td class="jfp_txt_center jfp_bold thead_style">AMT INC</td>
											<td class="jfp_txt_center jfp_bold thead_style">% INC(DEC)</td>
										</tr>
										<tr>
											<td class="jfp_bold"> SERVICE INCOME </td>
											<td class="jfp_align_right total_style"> 49,997,208 </td>
											<td class="jfp_align_right total_style">100.0%</td>
											<td class="jfp_align_right total_style"> 42,411,481 </td>
											<td class="jfp_align_right">100.0%</td>
											<td class="jfp_align_right"> 7,585,727 </td>
											<td class="jfp_align_right">17.9%</td>
										</tr>
										<tr>
											<td class="jfp_bold"> LESS: COST OF SERVICE </td>
											<td class="jfp_align_right total_style"> 33,287,500 </td>
											<td class="jfp_align_right total_style">66.6%</td>
											<td class="jfp_align_right total_style"> 26,647,582 </td>
											<td class="jfp_align_right">62.8%</td>
											<td class="jfp_align_right"> 6,639,918 </td>
											<td class="jfp_align_right">24.9%</td>
										</tr>
										<tr>
											<td class="pad_left_27"> Inventory beginning </td>
											<td class="jfp_align_right"> 82,762 </td>
											<td class="jfp_align_right"></td>
											<td class="jfp_align_right"> 85,178 </td>
											<td class="jfp_align_right"></td>
											<td class="jfp_align_right"></td>
											<td class="jfp_align_right"></td>
										</tr>
										<tr>
											<td class="pad_left_27"> Purchases </td>
											<td class="jfp_align_right"> 2,118,342 </td>
											<td class="jfp_align_right"></td>
											<td class="jfp_align_right"> 1,788,111 </td>
											<td class="jfp_align_right"></td>
											<td class="jfp_align_right"></td>
											<td class="jfp_align_right"></td>
										</tr>
										<tr>
											<td class="pad_left_27"> Total Available </td>
											<td class="jfp_align_right"> 2,201,104 </td>
											<td class="jfp_align_right"></td>
											<td class="jfp_align_right"> 1,873,289 </td>
											<td class="jfp_align_right"></td>
											<td class="jfp_align_right"></td>
											<td class="jfp_align_right"></td>
										</tr>
										<tr>
											<td class="pad_left_27"> Inventory End  </td>
											<td class="jfp_align_right"> 93,302 </td>
											<td class="jfp_align_right"></td>
											<td class="jfp_align_right"> 82,762 </td>
											<td class="jfp_align_right"></td>
											<td class="jfp_align_right"></td>
											<td class="jfp_align_right"></td>
										</tr>
										<tr>
											<td> Cost Of Kitchen Sales </td>
											<td class="jfp_align_right"> 2,107,802 </td>
											<td class="jfp_align_right"></td>
											<td class="jfp_align_right"> 1,790,527 </td>
											<td class="jfp_align_right"></td>
											<td class="jfp_align_right"> 317,275 </td>
											<td class="jfp_align_right">17.7%</td>
										</tr>
										<tr>
											<td> Salaries & Wages </td>
											<td class="jfp_align_right"> 14,714,123 </td>
											<td class="jfp_align_right"></td>
											<td class="jfp_align_right"> 11,478,732 </td>
											<td class="jfp_align_right"></td>
											<td class="jfp_align_right"> 3,235,391 </td>
											<td class="jfp_align_right">28.2%</td>
										</tr>
										<tr>
											<td> 13th month Pay </td>
											<td class="jfp_align_right"> 1,152,129 </td>
											<td class="jfp_align_right"></td>
											<td class="jfp_align_right"> 871,275 </td>
											<td class="jfp_align_right"></td>
											<td class="jfp_align_right"> 280,854 </td>
											<td class="jfp_align_right">32.2%</td>
										</tr>
										<tr>
											<td> Depreciation </td>
											<td class="jfp_align_right"> 3,107,995 </td>
											<td class="jfp_align_right"></td>
											<td class="jfp_align_right"> 3,191,270 </td>
											<td class="jfp_align_right"></td>
											<td class="jfp_align_right"> (83,275)</td>
											<td class="jfp_align_right">-2.6%</td>
										</tr>
										<tr>
											<td> Laundry </td>
											<td class="jfp_align_right"> 1,041,118 </td>
											<td class="jfp_align_right"></td>
											<td class="jfp_align_right"> 1,124,424 </td>
											<td class="jfp_align_right"></td>
											<td class="jfp_align_right"> (83,306)</td>
											<td class="jfp_align_right">-7.4%</td>
										</tr>
										<tr>
											<td> Supplies/Facilities </td>
											<td class="jfp_align_right"> 7,623,567 </td>
											<td class="jfp_align_right"></td>
											<td class="jfp_align_right"> 4,840,001 </td>
											<td class="jfp_align_right"></td>
											<td class="jfp_align_right"> 2,783,566 </td>
											<td class="jfp_align_right">57.5%</td>
										</tr>
										<tr>
											<td> Rental </td>
											<td class="jfp_align_right"> 3,540,765 </td>
											<td class="jfp_align_right"></td>
											<td class="jfp_align_right"> 3,351,353 </td>
											<td class="jfp_align_right"></td>
											<td class="jfp_align_right"> 189,412 </td>
											<td class="jfp_align_right">5.7%</td>
										</tr>
										<tr>
											<td class="jfp_txt_center jfp_bold parent_title_head"> GROSS PROFIT </td>
											<td class="jfp_align_right jfp_bold parent_title_head"> 16,709,709 </td>
											<td class="jfp_align_right jfp_bold parent_title_head">33.4%</td>
											<td class="jfp_align_right jfp_bold parent_title_head"> 15,763,899 </td>
											<td class="jfp_align_right parent_title_head">37.2%</td>
											<td class="jfp_align_right parent_title_head"> 945,810 </td>
											<td class="jfp_align_right parent_title_head">6.0%</td>
										</tr>
										<tr><td colspan="7"></td></tr>
										<tr>
											<td class="jfp_bold child_title_style"> OPERATING EXPENSES </td>
											<td colspan="6" class="jfp_align_right"></td>
										</tr>
										<tr>
											<td> UTILITIES </td>
											<td class="jfp_align_right"> 4,625,895 </td>
											<td class="jfp_align_right">9.3%</td>
											<td class="jfp_align_right"> 4,354,547 </td>
											<td class="jfp_align_right">8.7%</td>
											<td class="jfp_align_right"> 271,348 </td>
											<td class="jfp_align_right">6.2%</td>
										</tr>
										<tr>
											<td> SALARIES AND WAGES </td>
											<td class="jfp_align_right"> 2,596,610 </td>
											<td class="jfp_align_right">5.2%</td>
											<td class="jfp_align_right"> 2,869,683 </td>
											<td class="jfp_align_right">5.7%</td>
											<td class="jfp_align_right"> (273,073)</td>
											<td class="jfp_align_right">-9.5%</td>
										</tr>
										<tr>
											<td> INTEREST AND BANK CHARGES </td>
											<td class="jfp_align_right"> 883,299 </td>
											<td class="jfp_align_right">1.8%</td>
											<td class="jfp_align_right"> 1,170,980 </td>
											<td class="jfp_align_right">2.3%</td>
											<td class="jfp_align_right"> (287,681)</td>
											<td class="jfp_align_right">-24.6%</td>
										</tr>
										<tr>
											<td> TAXES AND LICENSES </td>
											<td class="jfp_align_right"> 1,370,017 </td>
											<td class="jfp_align_right">2.7%</td>
											<td class="jfp_align_right"> 1,299,147 </td>
											<td class="jfp_align_right">2.6%</td>
											<td class="jfp_align_right"> 70,870 </td>
											<td class="jfp_align_right">5.5%</td>
										</tr>
										<tr>
											<td> SSS,PH & HDMF CONTRIBUTIONS </td>
											<td class="jfp_align_right"> 1,505,734 </td>
											<td class="jfp_align_right">3.0%</td>
											<td class="jfp_align_right"> 1,192,721 </td>
											<td class="jfp_align_right">2.4%</td>
											<td class="jfp_align_right"> 313,013 </td>
											<td class="jfp_align_right">26.2%</td>
										</tr>
										<tr>
											<td> SECURITY SERVICES </td>
											<td class="jfp_align_right"> 829,153 </td>
											<td class="jfp_align_right">1.7%</td>
											<td class="jfp_align_right"> 804,502 </td>
											<td class="jfp_align_right">1.6%</td>
											<td class="jfp_align_right"> 24,651 </td>
											<td class="jfp_align_right">3.1%</td>
										</tr>
										<tr>
											<td> REPAIRS AND MAINTENANCE </td>
											<td class="jfp_align_right"> 361,326 </td>
											<td class="jfp_align_right">0.7%</td>
											<td class="jfp_align_right"> 466,446 </td>
											<td class="jfp_align_right">0.9%</td>
											<td class="jfp_align_right"> (105,120)</td>
											<td class="jfp_align_right">-22.5%</td>
										</tr>
										<tr>
											<td> RENTAL </td>
											<td class="jfp_align_right"> 624,841 </td>
											<td class="jfp_align_right">1.2%</td>
											<td class="jfp_align_right"> 372,373 </td>
											<td class="jfp_align_right">0.7%</td>
											<td class="jfp_align_right"> 252,468 </td>
											<td class="jfp_align_right">67.8%</td>
										</tr>
										<tr>
											<td> COMMUNICATIONS </td>
											<td class="jfp_align_right"> 494,545 </td>
											<td class="jfp_align_right">1.0%</td>
											<td class="jfp_align_right"> 327,104 </td>
											<td class="jfp_align_right">0.7%</td>
											<td class="jfp_align_right"> 167,441 </td>
											<td class="jfp_align_right">51.2%</td>
										</tr>
										<tr>
											<td> SUPPLIES </td>
											<td class="jfp_align_right"> 120,834 </td>
											<td class="jfp_align_right">0.2%</td>
											<td class="jfp_align_right"> 53,939 </td>
											<td class="jfp_align_right">0.1%</td>
											<td class="jfp_align_right"> 66,895 </td>
											<td class="jfp_align_right">124.0%</td>
										</tr>
										<tr>
											<td> DEPRECIATION </td>
											<td class="jfp_align_right"> 548,470 </td>
											<td class="jfp_align_right">1.1%</td>
											<td class="jfp_align_right"> 354,586 </td>
											<td class="jfp_align_right">0.7%</td>
											<td class="jfp_align_right"> 193,884 </td>
											<td class="jfp_align_right">54.7%</td>
										</tr>
										<tr>
											<td> TRANSPORTATION AND TRAVEL </td>
											<td class="jfp_align_right"> 187,646 </td>
											<td class="jfp_align_right">0.4%</td>
											<td class="jfp_align_right"> 178,390 </td>
											<td class="jfp_align_right">0.4%</td>
											<td class="jfp_align_right"> 9,256 </td>
											<td class="jfp_align_right">5.2%</td>
										</tr>
										<tr>
											<td> EMPLOYEE BENEFITS </td>
											<td class="jfp_align_right"> 790,753 </td>
											<td class="jfp_align_right">1.6%</td>
											<td class="jfp_align_right"> 456,181 </td>
											<td class="jfp_align_right">0.9%</td>
											<td class="jfp_align_right"> 334,572 </td>
											<td class="jfp_align_right">73.3%</td>
										</tr>
										<tr>
											<td> BILLBOARDS, SIGNBOARDS AND PLACARDS </td>
											<td class="jfp_align_right"> 14,329 </td>
											<td class="jfp_align_right">0.0%</td>
											<td class="jfp_align_right"> 53,739 </td>
											<td class="jfp_align_right">0.1%</td>
											<td class="jfp_align_right"> (39,410)</td>
											<td class="jfp_align_right">-73.3%</td>
										</tr>
										<tr>
											<td> 13TH MONTH PAY </td>
											<td class="jfp_align_right"> 203,317 </td>
											<td class="jfp_align_right">0.4%</td>
											<td class="jfp_align_right"> 217,819 </td>
											<td class="jfp_align_right">0.4%</td>
											<td class="jfp_align_right"> (14,502)</td>
											<td class="jfp_align_right">-6.7%</td>
										</tr>
										<tr>
											<td> FINANCING CHARGES </td>
											<td class="jfp_align_right"> 806,338 </td>
											<td class="jfp_align_right">1.6%</td>
											<td class="jfp_align_right"> 728,384 </td>
											<td class="jfp_align_right">1.5%</td>
											<td class="jfp_align_right"> 77,954 </td>
											<td class="jfp_align_right">10.7%</td>
										</tr>
										<tr>
											<td> INSURANCE </td>
											<td class="jfp_align_right"> 113,377 </td>
											<td class="jfp_align_right">0.2%</td>
											<td class="jfp_align_right"> 127,263 </td>
											<td class="jfp_align_right">0.3%</td>
											<td class="jfp_align_right"> (13,886)</td>
											<td class="jfp_align_right">-10.9%</td>
										</tr>
										<tr>
											<td> PROFESSIONAL FEE </td>
											<td class="jfp_align_right"> 39,829 </td>
											<td class="jfp_align_right">0.1%</td>
											<td class="jfp_align_right"> 30,900 </td>
											<td class="jfp_align_right">0.1%</td>
											<td class="jfp_align_right"> 8,929 </td>
											<td class="jfp_align_right">28.9%</td>
										</tr>
										<tr>
											<td> REPRESENTATION AND ENTERTAINMENT </td>
											<td class="jfp_align_right"> 5,538 </td>
											<td class="jfp_align_right">0.0%</td>
											<td class="jfp_align_right"> 5,679 </td>
											<td class="jfp_align_right">0.0%</td>
											<td class="jfp_align_right"> (141)</td>
											<td class="jfp_align_right">-2.5%</td>
										</tr>
										<tr>
											<td> TRAININGS AND MEETINGS </td>
											<td class="jfp_align_right"> 4,000 </td>
											<td class="jfp_align_right">0.0%</td>
											<td class="jfp_align_right">-</td>
											<td class="jfp_align_right">0.0%</td>
											<td class="jfp_align_right"> 4,000 </td>
											<td class="jfp_align_right"></td>
										</tr>
										<tr>
											<td> MISCELLANEOUS </td>
											<td class="jfp_align_right"> 10,649 </td>
											<td class="jfp_align_right">0.0%</td>
											<td class="jfp_align_right"> 25,921 </td>
											<td class="jfp_align_right">0.1%</td>
											<td class="jfp_align_right"> (15,272)</td>
											<td class="jfp_align_right">-58.9%</td>
										</tr>
										<tr>
											<td class="jfp_txt_center jfp_bold parent_title_head"> TOTAL OPERATING EXPENSES </td>
											<td class="jfp_align_right jfp_bold parent_title_head"> 16,136,500 </td>
											<td class="jfp_align_right jfp_bold parent_title_head">32.3%</td>
											<td class="jfp_align_right jfp_bold parent_title_head"> 15,090,304 </td>
											<td class="jfp_align_right parent_title_head">35.6%</td>
											<td class="jfp_align_right parent_title_head">1,046,196 </td>
											<td class="jfp_align_right parent_title_head">6.9%</td>
										</tr>
										<tr>
											<td class="jfp_txt_center jfp_bold parent_title_head">  INCOME FROM OPERATION </td>
											<td class="jfp_align_right jfp_bold parent_title_head"> 573,209 </td>
											<td class="jfp_align_right jfp_bold parent_title_head">1.1%</td>
											<td class="jfp_align_right jfp_bold parent_title_head"> 673,595 </td>
											<td class="jfp_align_right parent_title_head">1.6%</td>
											<td class="jfp_align_right parent_title_head"> (100,386)</td>
											<td class="jfp_align_right parent_title_head">-14.9%</td>
										</tr>
										<tr>
											<td class="jfp_bold child_title_style">OTHER INCOME</td>
											<td colspan="6"></td>
										</tr>
										<tr>
											<td> RENTAL INCOME </td>
											<td class="jfp_align_right"> 252,000 </td>
											<td class="jfp_align_right">0.5%</td>
											<td class="jfp_align_right"> 252,000 </td>
											<td class="jfp_align_right">0.6%</td>
											<td class="jfp_align_right">-</td>
											<td class="jfp_align_right">0.0%</td>
										</tr>
										<tr>
											<td> MISCELLANEOUS INCOME </td>
											<td class="jfp_align_right"> 396,244 </td>
											<td class="jfp_align_right">1.2%</td>
											<td class="jfp_align_right"> 237,785 </td>
											<td class="jfp_align_right">0.9%</td>
											<td class="jfp_align_right"> 158,459 </td>
											<td class="jfp_align_right">66.6%</td>
										</tr>
										<tr>
											<td> INTEREST INCOME </td>
											<td class="jfp_align_right"> 8,080 </td>
											<td class="jfp_align_right">0.0%</td>
											<td class="jfp_align_right"> 7,731 </td>
											<td class="jfp_align_right">0.0%</td>
											<td class="jfp_align_right"> 349 </td>
											<td class="jfp_align_right">4.5%</td>
										</tr>
										<tr>
											<td class="jfp_align_right jfp_bold total_style"> Sub-Total </td>
											<td class="jfp_align_right jfp_bold"> 656,323 </td>
											<td class="jfp_align_right jfp_bold">1.3%</td>
											<td class="jfp_align_right jfp_bold"> 497,516 </td>
											<td class="jfp_align_right">1.2%</td>
											<td class="jfp_align_right"> 158,807 </td>
											<td class="jfp_align_right">31.9%</td>
										</tr>
										<tr>
											<td class="jfp_txt_center jfp_bold">  INCOME BEFORE INCOME TAX </td>
											<td class="jfp_align_right jfp_bold"> 1,229,532 </td>
											<td class="jfp_align_right jfp_bold">2.5%</td>
											<td class="jfp_align_right jfp_bold"> 1,171,111 </td>
											<td class="jfp_align_right">2.8%</td>
											<td class="jfp_align_right"> 58,421 </td>
											<td class="jfp_align_right">5.0%</td>
										</tr>
										<tr>
											<td class="jfp_txt_center jfp_bold"> PROVISION FOR INCOME TAX </td>
											<td class="jfp_align_right jfp_bold"> 366,436 </td>
											<td class="jfp_align_right jfp_bold">0.7%</td>
											<td class="jfp_align_right jfp_bold"> 349,014 </td>
											<td class="jfp_align_right">0.8%</td>
											<td class="jfp_align_right"> 17,422 </td>
											<td class="jfp_align_right">5.0%</td>
										</tr>
										<tr>
											<td class="jfp_txt_center jfp_bold parent_title_head"> NET INCOME </td>
											<td class="jfp_align_right jfp_bold parent_title_head"> 863,096 </td>
											<td class="jfp_align_right jfp_bold parent_title_head">1.7%</td>
											<td class="jfp_align_right jfp_bold parent_title_head"> 822,097 </td>
											<td class="jfp_align_right parent_title_head">1.9%</td>
											<td class="jfp_align_right parent_title_head"> 40,999 </td>
											<td class="jfp_align_right parent_title_head">5.0%</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
				<br>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	  
@endsection
@section('customjs')
@endsection

