@extends('layouts.base')

@section('content')
    <div class="page-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        2307 Form
                    </h3>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="form-horizontal">
                <div class="row-fluid form-horizontal">
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label">Date from:</label>
                            <div class="controls ">
                                <input type="date" value="{!! date('Y-m-d', strtotime('yesterday')) !!}"/>
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label">Date to:</label>
                            <div class="controls" >
                                <input type="date" value="{!! date('Y-m-d', strtotime('now')) !!}"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Select Supplier</label>
                    <div class="controls">
                        <select class="span6 " data-placeholder="Choose a Supplier" tabindex="1">
                            <option value="">Select...</option>
                            @foreach(\App\Models\Supplier::all() as $supplier)
                                <option value="{{ $supplier->id }}">
                                    {{ $supplier->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="jfp_center"><input type="submit" class="btn green" value="Submit"></div>
            </div>
            <br>
            <!-- END PAGE CONTENT-->
            <table class="table table-striped table-bordered table-hover" id="sample_1">
                <thead>
                <tr>
                    <th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
                    <th>Supplier's Name</th>
                    <th>TIN</th>
                    <th>Registered Address</th>
                    <th>Zip Code</th>
                    <th>Foreign Address</th>
                    <th>Zip Code</th>
                </tr>
                </thead>
                <tbody>
                <tr class="odd gradeX">
                    <td><input type="checkbox" class="checkboxes" value="1" /></td>
                    <td>Lorem ipsum</td>
                    <td>Lorem ipsum</td>
                    <td>Lorem ipsum</td>
                    <td>Lorem ipsum</td>
                    <td>Lorem ipsum</td>
                    <td>Lorem ipsum</td>
                </tr>
                <tr class="odd gradeX">
                    <td><input type="checkbox" class="checkboxes" value="1" /></td>
                    <td>Lorem ipsum</td>
                    <td>Lorem ipsum</td>
                    <td>Lorem ipsum</td>
                    <td>Lorem ipsum</td>
                    <td>Lorem ipsum</td>
                    <td>Lorem ipsum</td>
                </tr>
                <tr class="odd gradeX">
                    <td><input type="checkbox" class="checkboxes" value="1" /></td>
                    <td>Lorem ipsum</td>
                    <td>Lorem ipsum</td>
                    <td>Lorem ipsum</td>
                    <td>Lorem ipsum</td>
                    <td>Lorem ipsum</td>
                    <td>Lorem ipsum</td>
                </tr>
                </tbody>
            </table>
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
@endsection