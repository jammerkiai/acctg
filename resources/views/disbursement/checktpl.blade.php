@extends('layouts.bare')
@section('content')
    <style>
        body {
            margin: 0 0;
        }

        .cheque {
            @if($cv->bank == 'BDO')
            background-image: url({{ asset('images/bdo.jpg') }});
            @else
            background-image: url({{ asset('images/ew.jpg') }});
            @endif
            background-size: cover;
            background-position: 0 -16px;
            display: block;
            position: absolute;
            top: 0px;
            left: 0px;
            width: 8in;
            height: 3in;
            border: 1px solid red;
        }

        .cheque div {
            position: absolute;
        }

        .chequeNumber {
            top: 14px;
            right: 150px;
        }

        .chequeDate {
            top: 22px;
            right: 70px;
        }

        .amount {
            @if($cv->bank == 'BDO')
            top: 50px;
            left: 570px;
            width: 170px;
            @else
            top: 54px;
            left: 600px;
            width: 150px;
            @endif
            text-align: center;
        }

        .payee {
            top: 54px;
            left: 100px;
            width: 440px;
            text-align: center;
        }

        .amountInWords {
            top: 84px;
            left: 80px;
            width: 660px;
            height: 38px;
            text-align: center;
        }

    </style>
    <div class="cheque">
        <div class="chequeDate">{!! date('F d, Y', strtotime($cv->voucher_date)) !!}</div>
        <div class="payee">{{ $cv->payee or '' }}</div>
        <div class="amount">{{ $cv->invoice->total  }}</div>
        <div class="amountInWords">
            {{ $cv->amount_in_words or 'Eight hundred seventy seven million eight hundred seventy eight thousand five hundred thirty five and forty five cents' }}</div>
    </div>
@endsection