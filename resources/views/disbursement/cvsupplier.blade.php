@extends('layouts.bare')
@section('content')
    <style>
        body {
            margin: 0 0;
        }

        .cvoucher {
            @if($st == 1)
            background-image: url({{ asset('images/cv1.jpg') }});
            @else
            background-image: url({{ asset('images/cv2.jpg') }});
            @endif
            background-size: cover;
            background-position: 0 -16px;
            display: block;
            position: absolute;
            top: 0px;
            left: 0px;
            width: 8in;
            height: 10in;
        }

        .cvoucher div {
            position: absolute;
        }

        .chequeNumber {
            @if($st==1)
            top: 226px;
            left: 590px;
            @else
            top: 216px;
            left: 566px;
            @endif
            width: 164px;
        }

        .chequeDate {
            @if($st==1)
            top: 200px;
            left: 550px;
            @else
            top: 186px;
            left: 536px;
            @endif
            width: 190px;
        }

        .particulars {
            @if($st==1)
            top: 366px;
            left: 50px;
            @else
            top: 340px;
            left: 50px;
            @endif
            width: 554px;
            height: 230px;
            text-align: left;
        }

        .amount {
            @if($st==1)
            top: 366px;
            left: 620px;
            @else
            top: 340px;
            left: 620px;
            @endif
            width: 100px;
            height: 24px;
            text-align: right;
        }

        .wht {
            @if($st==1)
            top: 396px;
            left: 620px;
            @else
            top: 370px;
            left: 620px;
            @endif
            width: 100px;
            height: 24px;
            text-align: right;
        }

        .net {
            @if($st==1)
            top: 416px;
            left: 620px;
            @else
            top: 400px;
            left: 620px;
            @endif
            width: 100px;
            height: 24px;
            text-align: right;
        }

        .payee {
            @if($st==1)
            top: 275px;
            left: 110px;
            @else
            top: 250px;
            left: 100px;
            @endif
            width: 620px;
            height: 48px;
            text-align: center;
        }

        .preparedBy {
            @if($st==1)
            top: 620px;
            left: 50px;
            @else
            top: 600px;
            left: 50px;
            @endif
            width: 110px;

        }

        .checkedBy {
            @if($st==1)
            top: 696px;
            left: 50px;
            @else
            top: 676px;
            left: 50px;
            @endif
            width: 110px;

        }

        .approvedBy {
            @if($st==1)
            top: 775px;
            left: 50px;
            @else
            top: 755px;
            left: 50px;
            @endif
            width: 110px;
        }

        .total {
            @if($st==1)
            top: 868px;
            left: 172px;
            @else
            top: 860px;
            left: 154px;
            @endif
            width: 130px;
        }

    </style>
    <div class="cvoucher">
        <div class="payee">{{ $cv->payee }}</div>
        <div class="particulars">{{ $cv->particulars }}</div>
        <div class="total">{{ $cv->invoice->total }}</div>
        <div class="chequeNumber">{{ $cv->check_number }}</div>
        <div class="chequeDate">{{ $cv->voucher_date }}</div>
        <div class="preparedBy">{{ $cv->prepared_by }}</div>
        <div class="checkedBy">{{ $cv->checked_by }}</div>
        <div class="approvedBy">{{ $cv->approved_by }}</div>
        <div class="amount">{{ $cv->invoice->total }}</div>
        <div class="wht">Less 1%: {{ $cv->invoice->wht }}</div>
        <div class="net">Net: {{ $cv->invoice->net }}</div>
    </div>
@endsection