@extends('layouts.base')

@section('content')
    <!-- BEGIN PAGE -->
    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <div id="portlet-config" class="modal hide">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button"></button>
                <h3>portlet Settings</h3>
            </div>
            <div class="modal-body">
                <p>Here will be a configuration form</p>
            </div>
        </div>
        <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    </br>
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <a href="{{ url('/disbursement') }}"><button type="button" class="btn green">Back</button></a>
                    <h3 class="page-title">
                        Expense details
                    </h3>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->

            <div class="row-fluid">
                <div class="span12">
                    <div class="portlet box purple">
                        <div class="portlet-title">
                            <h4><i class="icon-shopping-cart"></i>Invoice Number: {{ $invoice->invoice_number }}</h4>
                            <h4 class="pull-right">Date Received: {{ $invoice->delivery_date }}</h4>
                        </div>

                        <div class="portlet-body">
                            <form method="post" action="{{ url('disbursement/inventory/add') }}" name="addInventoryForm">
                                {!! csrf_field() !!}
                            <table border="1" cellspacing="5" cellpadding="5" class="table table-striped table-bordered table-hover" id="sample_1">
                                <tr>
                                    <th>Item Name</th>
                                    <th>Chart of Accounts</th>
                                    <th>Total Cost</th>
                                    <th>WHT</th>
                                    <th>Net Total</th>
                                </tr>
                                @foreach($ii as $item)
                                <tr>
                                    <td>{{ $item->purchaseItem->name }}</td>
                                    <td class="select-auto-width">
                                        <select class="select-auto" data-placeholder="Choose a Category" name="entry[]" required @if($item->posted == 1) disabled @endif>
                                            <option value="">Select a category...</option>
                                            @foreach($coa as $acct)
                                             <option value="{{ $acct->code }}" @if($item->code == $acct->code) selected @endif> {{ trim($acct->name) }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="total">{{ $item->price * $item->quantity }}</td>
                                    <td class="wht">{{ $item->wht }}</td>
                                    <td class="net">{{ $item->net }}</td>
                                    <td style="display: none">
                                        @if($item->is_vatable == 'yes')
                                            <input type="checkbox" class="vatable" disabled checked/>
                                        @else
                                            <input type="checkbox" class="vatable" disabled/>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                <tr>
                                    <td colspan="2">
                                        <div class="pull-right"><strong>Totals:</strong></div>
                                    </td>
                                    <td id="grandTotal"></td>
                                    <td id="grandWhtTotal"></td>
                                    <td id="grandNet"></td>
                                </tr>
                                <tr>
                                    <!-- <td colspan="2" class="jfp_center"><button type="button" class="btn blue">Add to Inventory</button></td>
                                    <td colspan="2">Total Purchase: 336</br>WHT: 3</br>Input TAX: 36</br>Purchases Net of Input TAX: 300<br>Purchases Net of WHT: 333</td> -->
                                    <td colspan="5" class="jfp_center">
                                        @if($invoice->posted==0)
                                        <button type="submit" class="btn blue">Add to Inventory <i class="icon-ok"></i>
                                        </button>
                                        @endif
                                    </td>
                                </tr>
                            </table>
                            <input type="hidden" name="items" value="{{$ii}}">
                            </form>
                        </div>
                    </div>
                    @if($invoice->posted!=0)
                        @include('disbursement.printing')
                    @endif
                </div>
            </div>
            <br>
            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
@endsection
@section('customjs')
    <script>
        $(document).ready(function() {
            calculateGrandTotals();
            autoPickBank();
            function calculateGrandTotals() {
                var totals = 0, whts = 0, nets = 0;
                $.each($('.total'), function(i, j){
                    var item = parseFloat($(j).text());
                    if (isNaN(item)) {
                        item = 0;
                    }
                    totals = totals +  item;
                    $('#grandTotal').html( totals.toFixed(2) );
                });

                $.each($('.wht'), function(i, j){
                    var item = parseFloat($(j).text());
                    if (isNaN(item)) {
                        item = 0;
                    }
                    whts = whts +  item;
                    $('#grandWhtTotal').html( whts.toFixed(2) );
                });

                $.each($('.net'), function(i, j){
                    var item = parseFloat($(j).text());
                    if (isNaN(item)) {
                        item = 0;
                    }
                    nets = nets +  item;
                    $('#grandNet').html( nets.toFixed(2) );
                });
            }

            function autoPickBank() {
                $.each($('.vatable'), function(i, j){
                    if($(j).is(':checked')) {
                        $('#bank').html('Banco De Oro');
                    }
                });
            }
        });
    </script>

@endsection