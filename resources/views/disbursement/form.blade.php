@extends('layouts.base')

@section('content')
<!-- BEGIN PAGE -->
<div class="page-content">
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Disbursement Form
                </h3>
                <!-- END PAGE TITLE & BREADCRUMB-->
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>

        <!-- Default -->
        <div class="control-group default hide">
            <label class="control-label">Particular</label>
            <div class="controls">
                <input type="text" name="particulars[]" placeholder="Particular" class="span3 " value=""/>
                <input type="text" name="amounts[]" placeholder="Amount" class="span2 p_amount" value="" oninput="updateAmount();"/>
                <button type="button" class="item-remover"><i class="icon-remove"></i></button>
            </div>
        </div>

        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <form method="post" action="{{ url('disbursement/service/add') }}" name="disform">
            {!! csrf_field() !!}
            <div class="form-horizontal">
                <div class="control-group">
                    <label class="control-label">OR No.</label>
                    <div class="controls">
                        <input type="text" class="span4 " id="orn" name="orNumber"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Delivery Receipt No.</label>
                    <div class="controls">
                        <input type="text" class="span4 " id="drn" name="deliveryReceiptNumber"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Billing Invoice No.</label>
                    <div class="controls">
                        <input type="text" class="span4 " id="bin" name="billingInvoiceNumber"/>
                    </div>
                </div>
                <div class="control-group clone_row">
                    <label class="control-label">Particular</label>
                    <div class="controls">
                        <input type="text" name="particulars[]" placeholder="Particular" class="span3 " />
                        <input type="text" name="amounts[]" placeholder="Amount" class="span2 p_amount" oninput="updateAmount();"/>
                        <button type="button" class="item-remover"><i class="icon-remove"></i></button>
                    </div>
                </div>
                <div id="clone_container">
                </div>
                <div class="control-group">
                    <label class="control-label"></label>
                    <div class="controls">
                        <button class="btn blue" id="add_item">Add Particular</button>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Name of Payee</label>
                    <div class="controls">
                    <select class="select-auto" data-placeholder="Supplier" tabindex="1" name="payee" required>
                        <option value="">Select...</option>
                        @foreach($suppliers as $supplier)
                            <option value="{{ trim($supplier->name) }}">{{ $supplier->name }}</option>
                        @endforeach
                    </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Total Amount</label>
                    <div class="controls">
                        <input type="text" class="span4 " name="total_amount" id="total_amount"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Date received of billing invoice</label>
                    <div class="controls">
                        <input type="date" class="span4 " value="{!! date('Y-m-d') !!}" name="dateReceived"/>
                    </div>
                </div>
                <div class="jfp_left">
                    <a href="{{ url('/disbursement') }}" class="btn btn-primary green">
                        <i class="icon-arrow-left"></i> Back
                    </a>
                    <input type="submit" class="btn green" value="Submit">
                </div>
            </div>
        </form>
        <br>

        <!-- END PAGE CONTENT-->

    </div>
    <!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
<style>
    .item-remover {
        display:none;
    }
    #clone_container .item-remover {
        display:inline-block;
    }
</style>
@endsection
@section('customjs')
    <script type="text/javascript">

        function updateAmount() {
            var total_amount = 0;
            $.each($('.p_amount'), function (i, j) {
                var item = parseFloat($(j).val());
                if (isNaN(item)) {
                    item = 0;
                }
                total_amount = total_amount + item;
                $('#total_amount').val(total_amount.toFixed(2));
            });
        }

        $(document).ready(function() {

            $('#add_item').on('click', function(e) {
                e.preventDefault();
                var elem = $('.default').eq(0);
                var clone = elem.clone();
                clone.removeClass('hide default');
                clone.addClass('clone_row');
                $('#clone_container').append(clone);


                $('.item-remover').on('click', function(e) {
                    e.preventDefault();
                    console.log($(this));
                    $(this).closest('.clone_row').remove();
                    updateAmount();
                });
            });

        });
    </script>
@endsection