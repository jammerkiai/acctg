@extends('layouts.base')

@section('content')
<!-- BEGIN PAGE -->
		<div class="page-content">

			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Disbursement
						</h3>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="tabbable tabbable-custom boxless">
	                     <ul class="nav nav-tabs">
	                        <li class="active"><a href="#tab_1" data-toggle="tab">For Purchase</a></li>
	                        <li><a href="#tab_2" data-toggle="tab">For Others</a></li>
	                     </ul>
	                     <div class="tab-content">
	                        <div class="tab-pane active" id="tab_1">
	                        	<div class="portlet box green">
									<div class="portlet-title">
										<h4><i class="icon-list-ul"></i></h4>
									</div>
									<div class="portlet-body">
										<table class="table table-striped table-bordered table-hover" id="sample_1">
											<thead>
												<tr>
													<th>Invoice No.</th>
													<th>Purchase Order No.</th>
													<th class="hidden-480">Date Received</th>
													<th >Total</th>
													<th >WHT</th>
													<th >NET</th>
													<th >Action</th>
												</tr>
											</thead>
											<tbody>
                                                @foreach($invoices as $invoice)
												<tr class="odd gradeX">
													<td>{{ $invoice->invoice_number }}</td>
													<td>{{ $invoice->purchase_id }}</td>
													<td class="hidden-480">{{ $invoice->delivery_date }}</td>
													<td >{{ number_format($invoice->total,2) }}</td>
													<td >{{ number_format($invoice->wht,2) }}</td>
													<td >{{ number_format($invoice->net,2) }}</td>
													<td class="center hidden-480"><a href="{{ url('/disbursement/'. $invoice->id . '/details') }}">View/Edit</a> | Delete</td>
												</tr>
                                                @endforeach
											</tbody>
										</table>
									</div>
								</div>
	                        </div>
	                        <div class="tab-pane" id="tab_2">
	                        	<div class="portlet box green">
									<div class="portlet-title">
										<h4><i class="icon-list-ul"></i></h4>
									</div>
									<div class="portlet-body">
										<div class="clearfix">
											<div class="btn-group">
												<a href="{{ url('disbursement/form') }}"><button id="sample_editable_1_new" class="btn green">
												Add New <i class="icon-plus"></i>
												</button></a>
											</div>
										</div>
										<table class="table table-striped table-bordered table-hover" id="sample_1">
											<thead>
												<tr>
													<th>OR Number</th>
													<th>Delivery Receipt Number</th>
													<th>Billing Invoice Number</th>
													<th>Name of Payee</th>
													<th>Total Amount</th>
													<th class="hidden-480">Date Received of Billing Invoice</th>
													<th >Action</th>
												</tr>
											</thead>
											<tbody>
                                                @foreach($ors as $or)
												<tr class="odd gradeX">
													<td>{{ $or->or_number }}</td>
													<td>{{ $or->delivery_receipt_number }}</td>
													<td>{{ $or->billing_invoice_number }}</td>
													<td>{{ $or->payee }}</td>
													<td>{{ number_format($or->total, 2) }}</td>
													<td class="hidden-480">{{ $or->delivery_date }}</td>
													<td class="center hidden-480"><a href="{{ url('/disbursement/' . $or->id . '/details/others') }}">View/Edit</a> | Delete</td>
												</tr>
                                                @endforeach
											</tbody>
										</table>
									</div>
								</div>
	                        </div>
	                     </div>
	                    </div> 	
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
				<br>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	
@endsection
@section('customjs')
    <script>
        jQuery(document).ready(function() {
            // initiate layout and plugins
            App.setPage("table_managed");
            App.init();
        });
    </script>
@endsection