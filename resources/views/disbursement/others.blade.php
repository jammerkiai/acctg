@extends('layouts.base')

@section('content')
<!-- BEGIN PAGE -->
<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div id="portlet-config" class="modal hide">
        <div class="modal-header">
            <button data-dismiss="modal" class="close" type="button"></button>
            <h3>portlet Settings</h3>
        </div>
        <div class="modal-body">
            <p>Here will be a configuration form</p>
        </div>
    </div>
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Expense details <small>Others</small>
                </h3>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <h4><i class="icon-shopping-cart"></i>OR Number: {{ $ordata->or_number }}</h4>
                    </div>
                    <div class="portlet-body">
                        <form method="post" action="{{ url('disbursement/payable/add') }}" name="dpaForm">
                            {!! csrf_field() !!}

                        <div class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label">WHT %:</label>
                                <div class="controls">
                                    <select class="span3 whtSelector" data-placeholder="Choose a Category" tabindex="1" name="whtPercent" id="whtPercent">
                                        @foreach([0.00,0.01,0.02,0.05,0.10,0.15] as $whtP)
                                            <option value="{{$whtP}}"
                                                @if($particulars[0]->wht_percent == $whtP) selected @endif
                                            >
                                            {{$whtP * 100}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">VAT %:</label>
                                <div class="controls">
                                    <input type="text" class="span3 vatSelector" name="vatPercent" id="vatPercent" value="{{ $particulars[0]->vat_percent or 0}}"/>
                                </div>
                            </div>
                        </div>   
                        <table border="1" cellspacing="5" cellpadding="5" class="table table-striped table-bordered table-hover" id="sample_1">
                            <tr>
                                <th>Particulars</th>
                                <th>Chart of Accounts of Expenses</th>
                                <th>Total Expenses</th>
                                <th>WHT</th>
                                <th>VAT</th>
                                <th>Net Total</th>
                            </tr>
                            @foreach($particulars as $key => $particular)
                            <tr>
                                <td>{{$particular->particular}}</td>
                                <td class="select-auto-width">
                                    <select class="select-auto" data-placeholder="Choose a Category" tabindex="1" name="entry[]" required @if($particular->posted == 1) disabled @endif>
                                        <option value="">Select...</option>
                                        @foreach($coa as $acct)
                                            <option value="{{ $acct->code }}"
                                            @if($particular->code == $acct->code)
                                                    selected
                                            @endif>
                                                {{ $acct->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </td>
                                <td class="amount" id="amount_{{$particular->id}}">{{ number_format($particular->price, 2, '.', '')}}</td>
                                <td><input type="text" class="wht span12" id="wht_{{$particular->id}}" data-id="{{$particular->id}}" name="whtValue[]" value="{{$particular->wht}}" @if($particular->posted == 1) disabled @endif/></td>
                                <td>
                                    <input type="text" name="vatValue[]" id="vat_{{$particular->id}}" class="vat span12" value="{{$particular->vat}}" />
                                </td>
                                <td><input type="text" class="net span12" id="net_{{$particular->id}}" name="netValue[]" value="{{ number_format($particular->net, 2, '.', '') }}" @if($particular->posted == 1) disabled @endif/></td>
                               
                            </tr>
                            @endforeach
                            <tr>
                                <td colspan="2"><strong><span class="pull-right">Grand Totals:</span></strong></td>
                                <td><span id="grandTotal"></span></td>
                                <td><span id="grandWHT"></span></td>
                                <td><span id="grandVAT"></span></td>
                                <td><span id="grandNet"></span></td>
                                
                            </tr>
                            <tr>
                                <!-- <td colspan="2" class="jfp_center"><button type="button" class="btn blue">Add to Inventory</button></td>
                                <td colspan="2">Total Purchase: 336</br>WHT: 3</br>Input TAX: 36</br>Purchases Net of Input TAX: 300<br>Purchases Net of WHT: 333</td> -->
                                <td colspan="7" class="jfp_center">
                                    @if($ordata->posted==0)
                                    <button type="submit" class="btn blue">Add to Payables <i class="icon-ok"></i>
                                    </button>
                                    @else
                                    <a href="{{ url('assetmanagement/create', [$ordata->id]) }}" target="_blank" class="btn blue"> Assets Management</a>
                                    @endif
                                </td>
                            </tr>
                        </table>
                        <input type="hidden" name="particulars" value="{{$particulars}}"/>
                        </form>
                    </div>
                </div>
                <hr>
                <div class="form-horizontal">
                    @if($ordata->posted!=0)
                        @include('disbursement.printingor')
                    @endif
                </div>
            </div>
        </div>
        <br>
        <!-- END PAGE CONTENT-->
    </div>
    <!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
@endsection
@section('customjs')
    <script>
    $(document).ready(function() {

        $('.whtSelector, .vatSelector').on('change', function(e) {
            e.preventDefault();
            $('.wht').each(function(i, v){
                calculateLinePrice($(this).data('id'));
            });

        });

        $('.vat, .net, .wht').on('change', function(e) {
            calculateGrandTotals();
        });

        function calculateLinePrice(id) {
            var amountElem = $('#amount_' + id);
            var amount = parseFloat(amountElem.text()),

            whtValue = parseFloat($('#whtPercent').val()),
            
            wht = (whtValue != 0.00)? amount * whtValue: 0,
            
            net = amount - wht;
            vat = 0;

            if ($('#vatPercent').val() > 0) {
                var originalPrice = (amount/$('#vatPercent').val());
                vat = originalPrice * ($('#vatPercent').val() - 1);

                if(whtValue != 0.00) {
                    wht = originalPrice * whtValue;
                    net = originalPrice - wht;
                }
            }

            $('#wht_' + id).val(wht.toFixed(2));
            $('#net_' + id).val(net.toFixed(2));
            $('#vat_' + id).val(vat.toFixed(2));

            calculateGrandTotals();
        }

        calculateGrandTotals();
        function calculateGrandTotals() {
            var totals = 0, whts = 0, nets = 0, vats = 0;
            $.each($('.amount'), function(i, j){
                var item = parseFloat($(j).text());
                if (isNaN(item)) {
                    item = 0;
                }
                totals = totals +  item;
                $('#grandTotal').html( totals.toFixed(2) );
            });

            $.each($('.wht'), function(i, j){
                var item = parseFloat($(j).val());
                if (isNaN(item)) {
                    item = 0;
                }
                whts = whts +  item;
                $('#grandWHT').html( whts.toFixed(2) );
            });

            $.each($('.net'), function(i, j){
                var item = parseFloat($(j).val());
                if (isNaN(item)) {
                    item = 0;
                }
                nets = nets +  item;
                $('#grandNet').html( nets.toFixed(2) );
            });

            $.each($('.vat'), function(i, j){
                var item = parseFloat($(j).val());
                if (isNaN(item)) {
                    item = 0;
                }
                vats = vats +  item;
                $('#grandVAT').html( vats.toFixed(2) );
            });
        }
    });
    </script>
@endsection