<h3>Check Voucher and Printing</h3>
<form method="post" action="{{ url('disbursement/savecheck') }}" name="savecheckform">
    {!! csrf_field() !!}
    <input type="hidden" name="invoice_id" value="{{ $invoice->id or ''}}" />
    <div class="form-horizontal">
        <div class="control-group">
            <label class="control-label">Date Prepared:</label>
            <div class="controls">
                <input type="date" name="voucher_date"
                       class="span3"
                       value="{!! $invoice->check_voucher->voucher_date or date('Y-m-d') !!}"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Bank Name:</label>
            <div class="controls">
                <input type="hidden" name="bank" id="bank"
                       @if($invoice->wht > 0) value="BDO" @else value="EWB" @endif">
                <p class="jfp_align_text" id="bankLabel">@if($invoice->wht > 0) BDO @else EWB @endif</p>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Name of Payee:</label>
            <div class="controls">
                <input type="text" class="span6"
                       name="payee"
                       value="{{ $invoice->check_voucher->payee or $invoice->purchase->suppliers->name }}"
                       required/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Amount In Words:</label>
            <div class="controls">
                                <textarea class="span6 ckeditor"
                                          name="amount_in_words"
                                          rows="3" required>{{ $invoice->check_voucher->amount_in_words or '' }}</textarea>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Particulars:</label>
            <div class="controls">
                                <textarea class="span6 ckeditor" name="particulars"
                                          rows="6" required>{{ $invoice->check_voucher->particulars or $invoice->purchase->remarks }}</textarea>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">Voucher No.:</label>
            <div class="controls">
                <input type="text" class="span6"
                       value="{{ $invoice->check_voucher->voucher_number or $ctr['Voucher'] }}"
                       name="voucher_number" required /><!--System generated value-->
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Check No.:</label>
            <div class="controls">
                <input type="text" class="span6"
                       value="{{ $invoice->check_voucher->check_number or $check_number }}"
                       name="check_number" required />
                <input type="hidden" id="bdoNumber" value="{{ $ctr['BDO'] }}" />
                <input type="hidden" id="ewbNumber" value="{{ $ctr['EWB'] }}" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Prepared by:</label>
            <div class="controls">
                <input type="text" class="span6"
                       value="{{ $invoice->check_voucher->prepared_by or Auth::user()->name }}"
                       name="prepared_by" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Checked by:</label>
            <div class="controls">
                <input type="text" class="span6"
                       value="{{ $invoice->check_voucher->checked_by or 'RBM'}}"
                       name="checked_by" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Approved by:</label>
            <div class="controls">
                <input type="text" class="span6"
                       value="{{ $invoice->check_voucher->approved_by or 'RSM'}}"
                       name="approved_by" />
            </div>
        </div>
    </div>
    <div class="jfp_center">
        <button type="submit" role="button" class="btn green">Save Details <i class="icon-save"></i></button>
        @if(isset($invoice->check_voucher->id))
            <a href="{{ url('disbursement/cvsupplier', [$invoice->check_voucher->id]) }}" target="_blank" role="button" class="btn blue" data-toggle="modal">Print Voucher (Supplier) <i class="icon-envelope"></i></a>
            <a href="{{ url('disbursement/cvshogun', [$invoice->check_voucher->id]) }}" target="_blank" role="button" class="btn blue" data-toggle="modal">Print Voucher (Shogun) <i class="icon-calendar"></i></a>
            <a href="{{ url('disbursement/printCheck', [$invoice->check_voucher->id]) }}" target="_blank" role="button" class="btn blue" data-toggle="modal">Create Cheque <i class="icon-print"></i></a>
        @endif
    </div>

    <input type="hidden" name="invoice_number" value="{{ $invoice->invoice_number }}" />
    <input type="hidden" name="or_number" value="{{ $invoice->or_number }}" />
    <input type="hidden" name="id" value="{{ $invoice->check_voucher->id or ''}}" />
</form>