@extends('layouts.base')
@section('navigator')
@section('content')
<!-- BEGIN PAGE -->
<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div id="portlet-config" class="modal hide">
        <div class="modal-header">
            <button data-dismiss="modal" class="close" type="button"></button>
            <h3>portlet Settings</h3>
        </div>
        <div class="modal-body">
            <p>Here will be a configuration form</p>
        </div>
    </div>
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->           
                <h3 class="page-title">
                    Dashboard
                </h3>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="tiles">
            <a href="/purchasing">
                <div class="tile bg-yellow selected">
                    <div class="corner"></div>
                    <div class="tile-body">
                        <i class="icon-shopping-cart"></i>
                    </div>
                    <div class="tile-object">
                        <div class="name">
                            Purchases
                        </div>
                    </div>
                </div>
            </a>
            <a href="/disbursement">
                <div class="tile bg-green selected">
                    <div class="corner"></div>
                    <div class="tile-body">
                        <i class="icon-credit-card"></i>
                    </div>
                    <div class="tile-object">
                        <div class="name">
                            Disbursement
                        </div>
                        <div class="number">
                        </div>
                    </div>
                </div>
            </a>
            <a href="/releasing">
                <div class="tile bg-purple selected">
                    <div class="corner"></div>
                    <div class="tile-body">
                        <i class="icon-ok-sign"></i>
                    </div>
                    <div class="tile-object">
                        <div class="name">
                            Cheque Releasing
                        </div>
                        <div class="number">
                        </div>
                    </div>
                </div>
            </a>
            <a href="/sales">
                <div class="tile bg-green selected">
                    <div class="corner"></div>
                    <div class="tile-body">
                        <i class="icon-money"></i>
                    </div>
                    <div class="tile-object">
                        <div class="name">
                            Sales
                        </div>
                        <div class="number">
                        </div>
                    </div>
                </div>
            </a>
            <a href="/journal">
                <div class="tile bg-blue selected">
                    <div class="corner"></div>
                    <div class="tile-body">
                        <i class="icon-list-alt"></i>
                    </div>
                    <div class="tile-object">
                        <div class="name">
                            General Journal
                        </div>
                        <div class="number">
                        </div>
                    </div>
                </div>
            </a>
            <a href="/ledger">
                <div class="tile bg-red selected">
                    <div class="corner"></div>
                    <div class="tile-body">
                        <i class="icon-list-alt"></i>
                    </div>
                    <div class="tile-object">
                        <div class="name">
                            General Ledger
                        </div>
                        <div class="number">
                        </div>
                    </div>
                </div>
            </a>
            @if(  Auth::user()->hasRole('Root') or Auth::user()->hasRole('Administrator') or Auth::user()->hasRole('Manager') )
        
            <a href="/reports">
                <div class="tile bg-purple selected">
                    <div class="corner"></div>
                    <div class="tile-body">
                        <i class="icon-user"></i>
                    </div>
                    <div class="tile-object">
                        <div class="name">
                            Admin Reports
                        </div>
                        <div class="number">
                        </div>
                    </div>
                </div>
            </a>
            <a href="/settings">
                <div class="tile bg-blue selected">
                    <div class="corner"></div>
                    <div class="tile-body">
                        <i class="icon-cogs"></i>
                    </div>
                    <div class="tile-object">
                        <div class="name">
                            Settings
                        </div>
                        <div class="number">
                        </div>
                    </div>
                </div>
            </a>
            @endif
        </div>
        <br>
        <!-- END PAGE CONTENT-->
    </div>
    <!-- END PAGE CONTAINER-->  
</div>
@endsection
