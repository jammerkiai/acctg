@extends('layouts.base')
@section('content')
<!-- BEGIN PAGE -->
<div class="page-content">
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
				<h3 class="page-title">
					General Journal
				</h3>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
                    	<div class="portlet box blue">
							<div class="portlet-title">
								<h4><i class="icon-list-ul"></i>Journal Entry Form</h4>
							</div>
							<table>
								<tbody>
								<tr class="odd gradeX orig_row hide">
									<td><input class="span4 count" type="text" value="1" disabled="disabled"></td>
									<td class="select-auto-width">
										<select  name="glcode[]" class="GL glcode">
											@foreach($coa as $acct)
												<option value="{{ $acct->code }}">{{ $acct->code }} - {{ $acct->name }}</option>
											@endforeach
										</select>
									</td>
									<td>
										<input name="debit[]" class="span10 debit" type="text" value="0" />
									</td>
									<td>
										<input name="credit[]" class="span10 credit" type="text" value="0"/>
									</td>
									<td><span class="remove_row">Remove</span></td>
								</tr>
								</tbody>
							</table>
							<form name="journalentry" id="journalentry" method="POST" action="{{ url('/journal') }}">
							{!! csrf_field() !!}
							<div class="portlet-body">
								<div class="row-fluid">
									<div class="span12">
									<div id="message"></div>
										<table class="table table-striped table-bordered table-hover" id="tblentry">
											<thead>
												<tr>
													<th>Line No.</th>
													<th>GL Account No.</th>
													<th>Debit</th>
													<th>Credit</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<tr class="odd gradeX clone_row">
													<td><input class="span4 count" type="text" value="1" disabled="disabled"></td>
													<td class="select-auto-width">
														<select  name="glcode[]" class="GL glcode select-auto">
															@foreach($coa as $acct)
																<option value="{{ $acct->code }}">{{ $acct->code }} - {{ $acct->name }}</option>
															@endforeach
														</select>
													</td>
													<td>
														<input name="debit[]" class="span10 debit" type="text" value="0" />
													</td>
													<td>  
														<input name="credit[]" class="span10 credit" type="text" value="0"/>
													</td>
													<td><span class="remove_row">Remove</span></td>
												</tr>
											</tbody>
										</table>
										
									</div>
								</div>
								<button class="btn green" id="add_row">Add</button>
								<div class="form-horizontal">
						           	<div class="control-group">
						              <label class="control-label">Reason</label>
						              <div class="controls">
						                 <textarea rows="3" cols="8" class="span8 required" name="reason" required></textarea>
						              </div>
						           	</div>
						           	<div class="control-group">
						              <label class="control-label">Posting date</label>
						              <div class="controls">
						                 <input type="date" name="entrydate" class="required" value="{{ date('Y-m-d') }}" required>
						              </div>
						           	</div>
									<div class="control-group">
										<label class="control-label">Type</label>
										<div class="controls">
											<input type="checkbox" checked name="type" value="l">
										</div>
									</div>
								</div>
								<input type="submit" class="btn blue" value="Submit" id="submitbtn">
							</div>
							</form>
						</div>
						
				
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
		<br>
		<!-- END PAGE CONTENT-->
	</div>
	<!-- END PAGE CONTAINER-->	
</div>
<!-- END PAGE -->	
@endsection
@section('customjs')
<script>
	$(document).ready(function() {			

		function resetNumbering() {
			var count = $('#tblentry tbody tr').length;
			for(var x = 1; x <= count; x++) {
				$('#tblentry tbody tr input.count').eq(x-1).val(x);
			}
		}

		$('#add_row').on('click', function(e) {
			e.preventDefault();
			var elem = $('.orig_row').eq(0).clone();
            elem.removeClass('hide');
            elem.find('select').selectize({
                create: false,
                sortField: 'text'
            });
			$('#tblentry tbody').append(elem);

			resetNumbering();
		});

		$("#tblentry").on('click', '.remove_row', function () {
		    $(this).closest('tr').remove();
		    resetNumbering();
		});

		$('#submitbtn').on('click', function(e){
			e.preventDefault();

			var dr = 0;
			var cr = 0;
			var count = $('#tblentry tbody tr').length;

			$('#message').empty();

			for(x = 1; x <= count; x++) {
				dr = dr + parseInt($('#tblentry tbody tr input.debit').eq(x-1).val());
				cr = cr + parseInt($('#tblentry tbody tr input.credit').eq(x-1).val());
				console.log(dr + ' ' + cr);
			}



			if (dr != cr ) {
				$('#message').append("<div class='alert alert-danger fade in'><a href='#' class=close data-dismiss=alert aria-label=close>&times;</a>Not balanced! </div>");
			} else if (dr == 0 && cr == 0) {
				$('#message').append("<div class='alert alert-danger fade in'><a href='#' class=close data-dismiss=alert aria-label=close>&times;</a>Please enter valid amounts! </div>");
			
			} else {
				$('#journalentry').submit();	
			}

		});
		

	});
</script>
@endsection