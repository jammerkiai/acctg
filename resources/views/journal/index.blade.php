@extends('layouts.base')
@section('content')
<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							General Journal
						</h3>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
	                        	<div class="portlet box blue">
									<div class="portlet-title">
										<h4><i class="icon-list-ul"></i>Journal Entry List</h4>
									</div>
									<div class="portlet-body">
										<div class="clearfix">
											<div class="btn-group">
												<a href="{{ url('/journal/create') }}"><button id="sample_editable_1_new" class="btn green">
												Add New Entry <i class="icon-plus"></i>
												</button></a>
											</div>
										</div>
										<table class="table table-striped table-bordered table-hover" id="sample_1">
											<thead>
												<tr>
													<th>Document No.</th>
													<th>Posting Date</th>
													<th class="hidden-480">Amount</th>
													<th class="hidden-480">Reason of the Journal</th>
													<th >Action</th>
												</tr>
											</thead>
											<tbody>
												@foreach($gj as $itm)
												<tr class="odd gradeX">
													<td>{{ $itm->id }}</td>
													<td class="hidden-480">{{ $itm->entrydate }}</td>
													<td class="hidden-480">{{ $itm->amount }}</td>
													<td >{{ $itm->reason }}</td>
													<td class="center hidden-480">
														<a href="{{ url('/journal') . '/' . $itm->id }}">View</a>
														@if( Auth::user()->hasRole('Root') or Auth::user()->hasRole('Manager') )
														 | 
														<a href="{{ url('/journal') . '/' . $itm->id }}/edit">Edit</a> 
														 | 
														<a href="{{ url('/journal') }}">Delete</a>
														@endif
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
						
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
				<br>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	
@endsection