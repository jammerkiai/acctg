@extends('layouts.base')
@section('content')
<!-- BEGIN PAGE -->
<div class="page-content">
	<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
	<div id="portlet-config" class="modal hide">
		<div class="modal-header">
			<button data-dismiss="modal" class="close" type="button"></button>
			<h3>portlet Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here will be a configuration form</p>
		</div>
	</div>
	<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
	<!-- BEGIN PAGE CONTAINER-->
	<div class="container-fluid">
		<!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
				<h3 class="page-title">
					General Journal
				</h3>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row-fluid">
			<div class="span12">
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
                    	<div class="portlet box blue">
							<div class="portlet-title">
								<h4><i class="icon-list-ul"></i>Journal Entry No: {{ $gj->id }}</h4>
							</div>

							<div class="portlet-body">
								<div class="row-fluid">
									<div class="span12">
										<table class="table table-striped table-bordered table-hover" id="tblentry">
											<thead>
												<tr>
													<th>Line No.</th>
													<th>GL Account No.</th>
													<th>Debit</th>
													<th>Credit</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												@foreach($gj->gltrans as $gl)
												<tr class="odd gradeX clone_row">
													<td>
														{{ $gl->id }}
													</td>
													<td>
														{{ $gl->code }} - {{ $gl->coa->name }}
													</td>
													<td>  
														{{ $gl->debit }}
														
													</td>
													<td>  
														{{ $gl->credit}}	
													</td>
													<td><span class="remove_row"></span></td>
												</tr>
												@endforeach
											</tbody>
										</table>
										
									</div>
								</div>

								<div class="form-horizontal">
						           	<div class="control-group">
						              <label class="control-label">Reason: </label>
						              <div class="controls well well-sm">
						                  {{ $gj->reason }}
						              </div>
						           	</div>
						           	<div class="control-group">
									  <label class="control-label">Posting date </label>
									  <div class="controls  well well-sm">
										 {{ $gj->entrydate }}
									  </div>
						           	</div>
									<div class="control-group">
										<label class="control-label">Type </label>
										<div class="controls">
											<input type="checkbox" name="type" value="l" @if($gl->type=='l') checked @endif>
										</div>
									</div>
								</div>
								<a href="{{ url('/journal') }}" class="btn btn-primary green">
									<i class="icon-arrow-left"></i> Back
								</a>
							</div>
						</div>
						
				
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
		<br>
		<!-- END PAGE CONTENT-->
	</div>
	<!-- END PAGE CONTAINER-->	
</div>
<!-- END PAGE -->	
@endsection
