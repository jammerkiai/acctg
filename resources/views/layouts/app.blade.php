<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <title>Shogun | Log-in</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <link href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/metro.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/style_responsive.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/style_default.css') }}" rel="stylesheet" id="style_color" />
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/jfp/css/jfp_custom.css') }}" />
  <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
  <!-- BEGIN LOGO -->
  <div class="logo">
    <img src="{{ asset('assets/jfp/images/shogun_logo.png') }}" alt="" /> 
  </div>
  <!-- END LOGO -->
  <!-- BEGIN LOGIN -->
  <h3 class="jfp_txt_center jfp_color_white">ACCOUNTING SYSTEM</h3>
  <div class="content">
    <!-- BEGIN LOGIN FORM -->
    @yield('content')
    
    <!-- END LOGIN FORM -->
    
  </div>
  <!-- END LOGIN -->
  <!-- BEGIN COPYRIGHT -->
  <div class="copyright">
    2015 &copy; Shogun Suite Hotel.
  </div>

  <!-- END COPYRIGHT -->
  <!-- BEGIN JAVASCRIPTS -->
  <script src="{{ asset('assets/js/jquery-1.8.3.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('assets/jquery-validation/dist/jquery.validate.min.js') }}"></script>
  <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>