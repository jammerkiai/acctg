<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <title>Shogun | Dashboard</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/metro.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/bootstrap/css/bootstrap-responsive.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/style_responsive.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/data-tables/DT_bootstrap.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/style_default.css') }}" rel="stylesheet" id="style_color" />
    <link href="{{ asset('assets/select2/css/selectize.min.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/jfp/css/jfp_custom.css') }}" />
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />
    <style>
        .item {
            margin-bottom: 0 !important;
            overflow: initial !important;
        }
        .selectize-input, .select-auto-width{
            width: 400px;
        }
    </style>
</head>

<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
    <!-- BEGIN HEADER -->

    <div class="header navbar navbar-inverse navbar-fixed-top">
        <!-- BEGIN TOP NAVIGATION BAR -->
        <div class="navbar-inner">
            <div class="container-fluid">
                <!-- BEGIN LOGO -->
                <a class="brand" href="/home">
                <img src="{{ asset('assets/jfp/images/shogun_logo.png') }}" alt="logo" width="127px"/>
                </a>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
                <img src="{{ asset('assets/img/menu-toggler.png')}}" alt="" />
                </a>          
                <!-- END RESPONSIVE MENU TOGGLER -->                
                <!-- BEGIN TOP NAVIGATION MENU -->                  
                <ul class="nav pull-right">
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <li class="dropdown user">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="username">{{ Auth::user()->name }}</span>
                        <i class="icon-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="/profile"><i class="icon-user"></i> My Profile</a></li>
                            <li class="divider"></li>
                            <li><a href="/logout"><i class="icon-key"></i> Log Out</a></li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
                <!-- END TOP NAVIGATION MENU -->    
            </div>
        </div>
        <!-- END TOP NAVIGATION BAR -->
    </div>
        <!-- END HEADER -->
    <!-- BEGIN CONTAINER -->    
    <div class="page-container row-fluid leaf-green">
        @include('sidebar')
        @yield('content')
    </div>


    <!-- BEGIN FOOTER -->
    <div class="footer">
        2015 &copy; Shogun Suite Hotel
        <div class="span pull-right">
            <span class="go-top"><i class="icon-angle-up"></i></span>
        </div>
    </div>
    <!-- END FOOTER -->
    <!-- BEGIN JAVASCRIPTS -->
    <!-- Load javascripts at bottom, this will reduce page load time -->

    <script src="{{ asset('assets/js/jquery-1.8.3.min.js') }}"></script>            
    <script src="{{ asset('assets/breakpoints/breakpoints.js') }}"></script>            
    <script src="{{ asset('assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js') }}"></script>   
    <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.blockui.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.cookie.js') }}"></script>
    <script src="{{ asset('assets/js/app.js') }}"></script>
    <script src="{{ asset('assets/data-tables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/data-tables/DT_bootstrap.js') }}"></script>
    <script src="{{ asset('assets/select2/js/selectize.min.js') }}"></script>
    {{--<script src="{{ asset('assets/fullcalendar/fullcalendar/fullcalendar.min.js') }}"></script> --}}
    <script type="text/javascript" src="{{ asset('assets/uniform/jquery.uniform.min.js') }}"></script>
    {{--<script type="text/javascript" src="{{ asset('assets/chosen-bootstrap/chosen/chosen.jquery.min.js') }}"></script>--}}
    <!-- ie8 fixes -->
    <!--[if lt IE 9]>
    <!--<script src="assets/js/excanvas.js"></script>-->
    <!--<script src="assets/js/respond.js"></script>-->
    <![endif]-->
    <!-- END JAVASCRIPTS -->
    <script>
        $(document).ready(function(){
            App.init();
            var selectAuto = $('.select-auto');
            selectAuto.selectize({
                create: false,
                sortField: 'text'
            });
            $(".alert-success").fadeTo(2000, 500).slideUp(500, function(){
                $(".alert-success").alert('close');
            });
        });
    </script>
    @yield('customjs')
</body>
<!-- END BODY -->
</html>