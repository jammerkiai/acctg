@extends('layouts.base')
@section('content')
<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							General Ledger
						</h3>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
	                        	<div class="portlet box green">
									<div class="portlet-title">
										<h4><i class="icon-list-ul"></i></h4>
									</div>
									<div class="portlet-body">
										<form name="ledger" method="GET" action="{{ url('/ledger') }}">
										<div class="row-fluid form-horizontal">
											<div class="span5">
												<div class="control-group">
									              <label class="control-label">Date from:</label>
									              <div class="controls">
								                         <input type="date" name="dfr" id="dfr"
																class="span10" value="{{ $dfr or date('Y-m-d', strtotime('yesterday')) }}" />
									              </div>
									           	</div>
											</div>
											<div class="span5">
												<div class="control-group">
									              <label class="control-label">Date to:</label>
									              <div class="controls">
								                         <input type="date" name="dto" id="dto"
																class="span10" value="{{ $dto or date('Y-m-d', strtotime('now')) }}" />
									              </div>
									           	</div>
											</div>
											<div class="span2">
												<input type="submit" class="btn blue" value="Submit" />
											</div>
										</div>
										</form>
										<table class="table table-striped table-bordered table-hover" id="sample_1">
											<thead>
												<tr>
													<th></th>
													<th>Ending Balance</th>
												</tr>
											</thead>
											<tbody>
												<tr class="odd gradeX">
													<td><b>Assets</b></td>
													<td><a href="{{ url('/ledger/level2', ['root' => 'Assets' ,'dfr' => $dfr, 'dto' => $dto ]) }}">{{ $gl['ASSETS'] or '0.00' }}</a></td>
												</tr>
												<tr class="odd gradeX">
													<td><b>Liabilities</b></td>
													<td><a href="{{ url('/ledger/level2', ['root' => 'Liabilities' ,'dfr' => $dfr, 'dto' => $dto ]) }}">{{ $gl['LIABILITIES'] or '0.00' }}</a></td>
												</tr>
												<tr class="odd gradeX">
													<td><b>Equity</b></td>
													<td><a href="{{ url('/ledger/level2', ['root' => 'Equity' ,'dfr' => $dfr, 'dto' => $dto ]) }}">{{ $gl['EQUITY'] or '0.00' }}</td>
												</tr>
											</tbody>
										</table>
										<hr>
										<table class="table table-striped table-bordered table-hover" id="sample_1">
											<thead>
												<tr>
													<th></th>
													<th>Total Balance</th>
												</tr>
											</thead>
											<tbody>
												<tr class="odd gradeX">
													<td><b>Expenses</b></td>
													<td><a href="{{ url('/ledger/level2', ['root' => 'Expenses' ,'dfr' => $dfr, 'dto' => $dto ]) }}">{{ $gl['EXPENSES'] or '0.00' }}</a></td>
												</tr>
												<tr class="odd gradeX">
													<td><b>Income</b></td>
													<td><a href="{{ url('/ledger/level2', ['root' => 'Income' ,'dfr' => $dfr, 'dto' => $dto ]) }}">{{ $gl['INCOME'] or '0.00' }}</a></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
						
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
				<br>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	
@endsection