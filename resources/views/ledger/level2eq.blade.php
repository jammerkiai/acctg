@extends('layouts.base')
@section('content')
        <!-- BEGIN PAGE -->
<div class="page-content">
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    General Ledger - {{ $root }}
                </h3>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <h4><i class="icon-list-ul"></i></h4>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Ending Balance</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($gl as $class => $amt)
                                <tr class="odd gradeX">
                                    <td><b>{{ $class }}</b></td>
                                    <td><a href="{{url('/ledger/level3', ['subclass'=>$amt['subclass'], 'dfr'=> $dfr, 'dto'=>$dto])}}">{{ $amt['amt'] }}</a></td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <br>
        <!-- END PAGE CONTENT-->
    </div>
    <!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
@endsection
