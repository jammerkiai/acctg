@extends('layouts.base')
@section('content')
<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Purchase
						</h3>
						<!-- END PAGE TITLE & BREADCRUMB-->
						@if (session('error'))
							<div class="alert alert-danger">
								{{ session('error') }}
							</div>
						@endif
					</div>
				</div>
				<div class="row-fluid">
					<div class="form-horizontal pull-right">
						<div class="control-group">
							<form method="get" action="{{ url('purchasing') }}">
								<select name="status">
									@foreach($status as $key => $st)
										<option value="{{ $key }}"
												@if($key == $filter) selected @endif>
											{{ $st }}
										</option>
									@endforeach
								</select>
								<input type="submit" value="Filter" class="btn btn-sm blue"/>
							</form>
						</div>
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
	                        	<div class="portlet box yellow">
									<div class="portlet-title">
										<h4><i class="icon-shopping-cart"></i>Purchase Order</h4>
									</div>
									<div class="portlet-body">
										<div class="clearfix">
											<div class="btn-group">
												<a href="{{ url('/purchase/form') }}"><button id="sample_editable_1_new" class="btn green">
												Add New <i class="icon-plus"></i>
												</button></a>
											</div>
										</div>
										<table class="table table-striped table-bordered table-hover" id="sample_1">
											<thead>
												<tr>
													<th>Purchase Order No.</th>
													<th>Supplier</th>
													<th class="hidden-480">Date Order</th>
													<th class="hidden-480">Date Received</th>
													<th class="hidden-480">Remarks</th>
													<th class="hidden-480">Order Status</th>
													<th >Action</th>
												</tr>
											</thead>
											<tbody>
                                                @foreach($purchases as $purchase)
												<tr class="odd gradeX">
													<td>{{ $purchase->id }}</td>
													<td>{{ $purchase->suppliers->name }}</td>
													<td class="hidden-480">{{ $purchase->ordered_date }}</td>
													<td class="hidden-480">{{ $purchase->received_date }}</td>
													<td class="hidden-480">{{ $purchase->remarks }}</td>
													<td>
                                                        @if($purchase->status == 'received')
                                                        <span class="label label-success">Closed</span>
                                                        @else
                                                        <span class="label label-warning">Waiting for Delivery</span>
                                                        @endif
                                                    </td>
													<td class="center hidden-480">
                                                        <a href="{{ url('/purchase/'. $purchase->id .'/details') }}">View/Edit</a> |
                                                        <a href="{{ url('/purchase/'. $purchase->id .'/delete') }}">Delete</a>
                                                    </td>
												</tr>
                                                @endforeach
											</tbody>
										</table>
									</div>
								</div>
						
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
				<br>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	 	
	</div>
@endsection
@section('customjs')
    <script>
        jQuery(document).ready(function() {
            // initiate layout and plugins
            App.setPage("table_managed");
            App.init();
        });
    </script>
@endsection