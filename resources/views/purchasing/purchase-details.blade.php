@extends('layouts.base')
@section('content')
    <!-- BEGIN PAGE -->
    <div class="page-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            </br>
            <a href="{{ url('/purchasing') }}" class="btn btn-primary blue">
                <i class="icon-arrow-left"></i> Back
            </a>
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        Purchase details
                    </h3>
                    <h4>Purchase Order No. {{ $purchase->id }}</h4>
                    <h4>
                        Status:
                        @if($purchase->status == 'received')
                            <span class="closed_purchase">Closed</span>
                        @else
                            <span class="ongoing_purchase">Waiting for Delivery</span>
                        @endif
                    </h4>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            @if($purchase->status == 'pending')
            <div class="row-fluid">
                <div class="span12">
                    <form method="POST" action="{{ url('/purchase/payable/add') }}">
                        <div class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label">Invoice Number:</label>
                                <div class="controls">
                                    <input type="text" class="span6 required" name="invoiceNumber" required/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Received Date:</label>
                                <div class="controls">
                                    <input type="date" name="receivedDate" value="{{ date('Y-m-d') }}"/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">WHT Percentage:</label>
                                <div class="controls">
                                    <input type="text" name="wht_pct" id="wht_pct" value="{{ $set['WHT'] or 0.01 }}"/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">VAT:</label>
                                <div class="controls">
                                    <input type="text" name="vat_pct" id="vat_pct" value="{{ $set['VAT'] or 1.12 }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <h4><i class="icon-shopping-cart"></i>Items Received</h4>
                            </div>

                                <div class="portlet-body">
                                    <table border="1" cellspacing="5" cellpadding="5" class="table table-striped table-bordered table-hover" id="sample_1">
                                        <tr>
                                            <th>#</th>
                                            <th>Item Name</th>
                                            <th>Description</th>
                                            <th>Unit Price</th>
                                            <th>Qty</th>
                                            <th>Total</th>
                                            <th>WHT</th>
                                            <th>Net</th>
                                            <th>WHT</th>
                                            <th>VAT</th>
                                            <th>Cancel</th>
                                        </tr>
                                        @foreach($purchaseReceive as $key => $order)
                                            @if($order->ordered_qty > $order->received_qty)
                                            <tr class="itemInfo">
                                                <td><input type="hidden" name="itemIds[]" value="{{ $order->id }}" />{{$key+1}}</td>
                                                <td class="span2">
													<strong>{{ $order->name }} </strong>
													<br>
													<em style="color:brown">{{ $order->ordered_qty }} {!! str_plural($order->unit->name) !!}</em>
													<br>
													<span class="small" style="font-size:smaller;color:orange">(Pending: {{ $order->ordered_qty - $order->received_qty }})</span>
                                                </td>
                                                <td class="span2">
													<div style="font-size:smaller">Serial: <input type="text" name="itemSerials[]" class="span7" placeholder="Serial" value=""/></div>
													<div style="font-size:smaller">Model: <input type="text" name="itemModels[]" class="span7" placeholder="Model"  value=""/></div>
													<div style="font-size:smaller">Brand: <input type="text" name="itemBrands[]" class="span7" placeholder="Brand"  value=""/></div>
												</td>
                                                <td><input type="text" name="itemUnitPrices[]"
                                                           class="span12 itemPrice updatePurchase"
                                                           value="0" data-id="{{$order->id}}" id="unitprice_{{$order->id}}"/>
                                                </td>
                                                <td class="span2"><input type="number" name="itemRecQty[]"
                                                           class="span12 itemQty updatePurchase"
                                                           value="0" min="0" max="{{ $order->ordered_qty - $order->received_qty }}"
                                                           data-id="{{$order->id}}" id="qty_{{$order->id}}" />
                                                </td>
                                                <td>
                                                    <input type="hidden" name="itemTotalPrices[]" class="total span12 " id="total_{{$order->id}}" />
                                                    <span id="display_total_{{$order->id}}">0</span>
                                                </td>
                                                <td>
                                                    <input type="hidden" name="itemWHTs[]" class="wht span12 " id="wht_{{$order->id}}" />
                                                    <span id="display_wht_{{$order->id}}">0</span>
                                                </td>
                                                <td>
                                                    <input type="hidden" name="itemNetTotals[]" class="net span12 " id="net_{{$order->id}}" />
                                                    <span id="display_net_{{$order->id}}">0</span>
                                                </td>
                                                <td class="span1"><input type="checkbox" class="togglePurchase" name="itemAddWHT[]" id="togglewht_{{$order->id}}" data-id="{{$order->id}}"/></td>
                                                <td class="span1"><input type="checkbox" class="togglePurchase" name="itemAddVAT[]" value="{{ $key }}" id="togglevat_{{$order->id}}" data-id="{{$order->id}}"/></td>
                                                <td class="span1"><input type="checkbox" class="togglePurchase cancel" name="itemCancel[]" id="cancel_{{$order->id}}" data-id="{{$order->id}}" /></td>
                                            </tr>
                                            @endif
                                        @endforeach
                                            <tr>
                                                <td colspan="7"><strong><span class="pull-right">Grand Totals:</span></strong></td>
                                                <td><span id="grandTotal"></span></td>
                                                <td><span id="grandWHT"></span></td>
                                                <td><span id="grandNet"></span></td>
                                                <td colspan="3"></td>
                                            </tr>
                                        <tr>
                                            <td colspan="16" class="jfp_center">
                                                <button type="submit" class="btn blue">Add to Payables <i class="icon-ok"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                        </div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="purchaseNo" value="{{ $purchase->id }}">
                        <input type="hidden" name="hiddentotal" id="hiddentotal" >
                        <input type="hidden" name="hiddennet" id="hiddennet" >
                        <input type="hidden" name="hiddenwht" id="hiddenwht" >

                    </form>
                </div>
            </div>
            @endif

            <h3 class="page-title">
                Received Items
            </h3>
            <!-- For Complete -->
            @foreach($invoices as $invoice)
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <h4><i class="icon-shopping-cart"></i>Invoice Number: {{ $invoice->invoice_number }}</h4>
                            <h4 class="pull-right">Date Received: {{ $invoice->delivery_date }}</h4>
                        </div>
                            <div class="portlet-body">
                                <table border="1" cellspacing="5" cellpadding="5" class="table table-striped table-bordered table-hover tblReceived">
                                    <tr>
                                        <th>Item Name</th>
                                        <th>Serial Number</th>
                                        <th>Model</th>
                                        <th>Brand</th>
                                        <th>Unit Price</th>
                                        <th>Received Qty</th>
                                        <th>Total Cost</th>
                                        <th>WHT</th>
                                        <th>Net Total</th>
                                    </tr>
                                    @foreach($invoice->item as $item)
                                        <tr>
                                            <td>{{ $item->purchaseItem->name }}</td>
                                            <td>{{ $item->serial_number }}</td>
                                            <td>{{ $item->model }}</td>
                                            <td>{{ $item->brand }}</td>
                                            <td>{{ number_format($item->price , 2)}}</td>
                                            <td>{{ number_format($item->quantity , 2)}}</td>
                                            <td>{{ number_format($item->price * $item->quantity , 2)}}</td>
                                            <td>{{ number_format($item->wht , 2)}}</td>
                                            <td>{{ number_format($item->net , 2)}}</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="6">
                                            <div class="pull-right"><strong>Totals:</strong></div>
                                        </td>
                                        <td class="received_total">{{ $invoice->total }}</td>
                                        <td class="received_wht">{{ $invoice->wht }}</td>
                                        <td class="received_net">{{ $invoice->net }}</td>
                                    </tr>
                                </table>
                            </div>
                    </div>
                </div>
            </div>
            @endforeach
            <br>
            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
@endsection
@section('customjs')
    <script>
        $(document).ready(function() {
            var purchaseTotal = $('#purchase_total');
            var whtTotal = $('#wht_total');
            var purchaseTotalWht = $('#purchase_total_wht');


            $('.togglePurchase').on('click', function(e) {
                calculateLinePrice($(this).data('id'));
            });

            $('.updatePurchase').on('change', function(e) {
                e.preventDefault();
                calculateLinePrice($(this).data('id'));
            });

            function calculateLinePrice(id) {
                var unit = $('#unitprice_' + id).val(),
                    wht_pct = $('#wht_pct').val(),
                    vat_pct = $('#vat_pct').val(),
                    qty = $('#qty_' + id).val(),
                    total = unit * qty,
                    wht = $('#togglewht_' + id).is(':checked') ? total * wht_pct : 0,
                    net = total - wht;

                if ($('#togglevat_' + id).is(':checked')) {
                    wht = (total/vat_pct) * wht_pct;
                    net = total - wht;
                }

                $('#total_' + id).val(total.toFixed(2));
                $('#wht_' + id).val(wht.toFixed(2));
                $('#net_' + id).val(net.toFixed(2));

                $('#display_total_' + id).html(total.toFixed(2));
                $('#display_wht_' + id).html(wht.toFixed(2));
                $('#display_net_' + id).html(net.toFixed(2));

                calculateGrandTotals();

            }

            function calculateGrandTotals() {
                var totals = 0, whts = 0, nets = 0;
                $.each($('.total'), function(i, j){
                    var item = parseFloat($(j).val());
                    if (isNaN(item)) {
                        item = 0;
                    }
                    totals = totals +  item;
                    $('#grandTotal').html( totals.toFixed(2) );
                    $('#hiddentotal').val(totals.toFixed(2));
                });

                $.each($('.wht'), function(i, j){
                    var item = parseFloat($(j).val());
                    if (isNaN(item)) {
                        item = 0;
                    }
                    whts = whts +  item;
                    $('#grandWHT').html( whts.toFixed(2) );
                    $('#hiddenwht').val(whts.toFixed(2));
                });

                $.each($('.net'), function(i, j){
                    var item = parseFloat($(j).val());
                    if (isNaN(item)) {
                        item = 0;
                    }
                    nets = nets +  item;
                    $('#grandNet').html( nets.toFixed(2) );
                    $('#hiddennet').val(nets.toFixed(2));
                });
            }

            function calculateInvoicedTotals() {

            }
        });
    </script>
@endsection
