@extends('layouts.base')
@section('content')
<!-- BEGIN PAGE -->
<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div id="portlet-config" class="modal hide">
        <div class="modal-header">
            <button data-dismiss="modal" class="close" type="button"></button>
            <h3>portlet Settings</h3>
        </div>
        <div class="modal-body">
            <p>Here will be a configuration form</p>
        </div>
    </div>
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">

                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Purchase Order
                </h3>
                <!-- END PAGE TITLE & BREADCRUMB-->
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <form method="POST" action="{{ url('/purchase/order/add') }}">
        <div class="form-horizontal">
            <div class="control-group">
                <label class="control-label">Date order</label>
                <div class="controls">
                    <input type="date" name="order_date" value="{{ date('Y-m-d') }}"/>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label">Select Supplier</label>
                <div class="controls">
                    <select name="supplier" class="select-auto" data-placeholder="Supplier" tabindex="1">
                        <option value="">Select...</option>
                        @foreach($suppliers as $supplier)
                            <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="control-group clone_row">
                <label class="control-label">Item</label>
                <div class="controls">
                    <input type="text" placeholder="Item Name" name="item_names[]" class="span3 " />
                    <select name="units[]" class="span3 required" required data-placeholder="Choose a Category" tabindex="1">
                        <option value="">Select Unit...</option>
                        @foreach($units as $unit)
                            <option value="{{ $unit->id }}">{{ $unit->name }}</option>
                        @endforeach
                    </select>
                    <input type="text" placeholder="Quantity" name="quantity[]"  class="span3 " />
                    <button type="button" class="item-remover"><i class="icon-remove"></i></button>
                </div>
            </div>
            <div id="clone_container">
            </div>
            <div class="control-group">
                <label class="control-label"></label>
                <div class="controls">
                    <button class="btn blue" id="add_item">Add Item</button>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Remarks</label>
                <div class="controls">
                    <textarea class="large" rows="3" name="remarks"></textarea>
                </div>
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="jfp_center"><input type="submit" class="btn green" value="Submit"></div>
        </div>
        </form>
        <br>
        <!-- END PAGE CONTENT-->
    </div>
    <!-- END PAGE CONTAINER-->
</div>
<style>
    .item-remover {
        display:none;
    }
    #clone_container .item-remover {
        display:inline-block;
    }
</style>
<!-- END PAGE -->
@endsection
@section('customjs')
    <script type="text/javascript">
        $(document).ready(function() {

            $('#add_item').on('click', function(e) {
                e.preventDefault();
                var clone = $('.clone_row').eq(0).clone();
                $('#clone_container').append(clone);


                $('.item-remover').on('click', function(e) {
                    e.preventDefault();
                    console.log($(this));
                    $(this).closest('.clone_row').remove();
                });
            });


        });
    </script>
@endsection
