@extends('layouts.base')
@section('content')

    <div class="page-content">
        <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        Cheque Details
                    </h3>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div>
                <div class="portlet box green">
                    <div class="portlet-title">
                        <h4><i class="icon-credit-card"></i></h4>
                    </div>
                    <div class="portlet-body">
                        <table cellpadding="5" border="1" class="table table-striped">
                            <tr>
                                <td><strong>Bank: </strong></td>
                                <td>{{ $cv->bank }}</td>
                            </tr>
                            <tr>
                                <td><strong>Date Prepared: </strong></td>
                                <td>{{ $cv->voucher_date }}</td>
                            </tr>
                            <tr>
                                <td><strong>Name of Payee: </strong></td>
                                <td>{{ $cv->payee }}</td>
                            </tr>
                            <tr>
                                <td><strong>Particulars: </strong></td>
                                <td>{{ $cv->particulars }}</td>
                            </tr>
                            <tr>
                                <td><strong>Check No.: </strong></td>
                                <td>{{ $cv->check_number }}</td>
                            </tr>
                            <tr>
                                <td><strong>Voucher No.: </strong></td>
                                <td>{{ $cv->voucher_number }}</td>
                            </tr>
                            <tr>
                                <td><strong>Prepared By: </strong></td>
                                <td>{{ $cv->prepared_by }}</td>
                            </tr>
                            <tr>
                                <td><strong>Checked By: </strong></td>
                                <td>{{ $cv->checked_by }}</td>
                            </tr>
                            <tr>
                                <td><strong>Approved By: </strong></td>
                                <td>{{ $cv->approved_by }}</td>
                            </tr>
                            <tr>
                                <td><strong>Amount:</strong></td>
                                <td>&#8369; {{ $cv->invoice->total }}</td>
                            </tr>
                            <tr>
                                <td><strong>Status:</strong></td>
                                <td>{{ $cv->status }}</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <a href="{{ url('releasing') }}" class="btn green">Back</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTAINER-->
        </div>
        <!-- END PAGE -->
    </div>

@endsection