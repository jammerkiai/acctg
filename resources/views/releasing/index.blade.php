@extends('layouts.base')
@section('content')
<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Cheque Releasing
						</h3>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<div class="row-fluid">
					<div class="form-horizontal pull-right">
						<div class="control-group">
							<form method="get" action="{{ url('releasing') }}">
								<select name="status">
									@foreach($status as $st)
										<option value="{{ $st }}"
												@if($st == $filter) selected @endif>
											{{ $st }}
										</option>
									@endforeach
								</select>
								<input type="submit" value="Filter" class="btn btn-sm blue"/>
							</form>
						</div>
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="tabbable tabbable-custom boxless">
	                     <ul class="nav nav-tabs">
	                        <li class="active"><a href="#tab_1" data-toggle="tab">Disbursement 1</a></li>
	                        <li><a href="#tab_2" data-toggle="tab">Disbursement 2</a></li>
	                     </ul>
	                     <div class="tab-content">
	                        <div class="tab-pane active" id="tab_1">
	                        	<div class="portlet box purple">
									<div class="portlet-title">
										<h4><i class="icon-credit-card"></i>Disbursement 1</h4>
									</div>
									<div class="portlet-body">
										<table class="table table-striped table-bordered table-hover" id="sample_1">
											<thead>
												<tr>
													<th>Bank</th>
													<th class="hidden-480">Date Prepared</th>
													<th>Name of Payee</th>
													<th class="hidden-480">Check No.</th>
													<th class="hidden-480">Voucher No.</th>
													<th class="hidden-480">Prepared by:</th>
													<th class="hidden-480">Checked by:</th>
													<th class="hidden-480">Approved by:</th>
													<th class="hidden-480">Check Status:</th>
													<th >Action</th>
												</tr>
											</thead>
											<tbody>
											@foreach($cv as $c)
												<tr class="odd gradeX">
													<td>{{ $c->bank }}</td>
													<td class="hidden-480">{{ $c->voucher_date }}</td>
													<td>{{ $c->payee }}</td>
													<td class="hidden-480">{{ $c->check_number }}</td>
													<td class="hidden-480">{{ $c->voucher_number }}</td>
													<td class="hidden-480">{{ $c->prepared_by }}</td>
													<td class="hidden-480">{{ $c->checked_by }}</td>
													<td class="hidden-480">{{ $c->approved_by }}</td>
													<td >
														<select class="statusSelect" data-id="{{ $c->id }}"
																data-value="{{ $c->status }}">
															@foreach($status as $st)
																<option value="{{ $st }}"
																		@if($st == $c->status) selected @endif>
																	{{ $st }}
																</option>
															@endforeach
														</select>
													</td>
													<td class="center hidden-480">
														<a href="{{ url('releasing', [$c->id]) }}">View</a> | Delete
													</td>
												</tr>
											@endforeach
											</tbody>
										</table>
									</div>
								</div>
	                        </div>
	                        <div class="tab-pane" id="tab_2">
	                        	<div class="portlet box blue">
									<div class="portlet-title">
										<h4><i class="icon-credit-card"></i>Disbursement 2</h4>
									</div>
									<div class="portlet-body">
										<table class="table table-striped table-bordered table-hover" id="sample_1">
											<thead>
												<tr>
													<th>Bank</th>
													<th class="hidden-480">Date Prepared</th>
													<th>Name of Payee</th>
													<th class="hidden-480">Check No.</th>
													<th class="hidden-480">Voucher No.</th>
													<th class="hidden-480">Prepared by:</th>
													<th class="hidden-480">Checked by:</th>
													<th class="hidden-480">Approved by:</th>
													<th class="hidden-480">Check Status:</th>
													<th >Action</th>
												</tr>
											</thead>
											<tbody>
											@foreach($cv2 as $c)
												<tr class="odd gradeX">
													<td>{{ $c->bank }}</td>
													<td class="hidden-480">{{ $c->voucher_date }}</td>
													<td>{{ $c->payee }}</td>
													<td class="hidden-480">{{ $c->check_number }}</td>
													<td class="hidden-480">{{ $c->voucher_number }}</td>
													<td class="hidden-480">{{ $c->prepared_by }}</td>
													<td class="hidden-480">{{ $c->checked_by }}</td>
													<td class="hidden-480">{{ $c->approved_by }}</td>
													<td >
														<select class="statusSelect" data-id="{{ $c->id }}"
																data-value="{{ $c->status }}">
															@foreach($status as $st)
															<option value="{{ $st }}"
																	@if($st == $c->status) selected @endif>
																{{ $st }}
															</option>
															@endforeach
														</select>
													</td>
													<td class="center hidden-480">
														<a href="{{ url('releasing', [$c->id]) }}">View</a> | Delete
													</td>
												</tr>
											@endforeach

											</tbody>
										</table>
									</div>
								</div>
	                        </div>
	                     </div>
	               	</div>
						
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
				<br>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	
@endsection
@section('customjs')
	<script>
		$(document).ready(function(){
			$('.statusSelect').on('change', function(e){
				e.preventDefault();
				var status = $(this).val(),
					cid = $(this).data('id'),
					url = "{{ url('releasing/status') }}";

				$.ajax({
					type: "POST",
					url: url,
					data: { status: status, id: cid},
					success: function(resp){
						console.log(resp);
					},
					dataType: 'json'
				});
			});
		});
	</script>
@endsection