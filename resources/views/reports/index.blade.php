@extends('layouts.base')
@section('content')
<!-- BEGIN PAGE -->
<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div id="portlet-config" class="modal hide">
        <div class="modal-header">
            <button data-dismiss="modal" class="close" type="button"></button>
            <h3>portlet Settings</h3>
        </div>
        <div class="modal-body">
            <p>Here will be a configuration form</p>
        </div>
    </div>
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->           
                <h3 class="page-title">
                    Admin Reports
                </h3>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="tiles">
            <a href="/balancesheetyearly">
                <div class="tile bg-yellow selected">
                    <div class="corner"></div>
                    <div class="tile-body">
                        <i class="icon-adjust"></i>
                    </div>
                    <div class="tile-object">
                        <div class="name">
                            Balance Sheet Yearly
                        </div>
                    </div>
                </div>
            </a>
			 <a href="/balancesheetmonthly">
                <div class="tile bg-red selected">
                    <div class="corner"></div>
                    <div class="tile-body">
                        <i class="icon-adjust"></i>
                    </div>
                    <div class="tile-object">
                        <div class="name">
                            Balance Sheet Monthly
                        </div>
                    </div>
                </div>
            </a>
            <a href="/incomestatementyearly">
                <div class="tile bg-green selected">
                    <div class="corner"></div>
                    <div class="tile-body">
                        <i class="icon-pencil"></i>
                    </div>
                    <div class="tile-object">
                        <div class="name">
                            Income Statement Yearly
                        </div>
                        <div class="number">
                        </div>
                    </div>
                </div>
            </a>
			 <a href="/incomestatementmonthly">
                <div class="tile bg-blue selected">
                    <div class="corner"></div>
                    <div class="tile-body">
                        <i class="icon-pencil"></i>
                    </div>
                    <div class="tile-object">
                        <div class="name">
                            Income Statement Monthly
                        </div>
                        <div class="number">
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <br>
        <!-- END PAGE CONTENT-->
    </div>
    <!-- END PAGE CONTAINER-->  
</div>
@endsection