@if (count($sales))
    <table class="table">
        <caption><h3>{{$caption}}</h3></caption>
        <thead>
        <tr>
            <th>#</th>
            <th>Date</th>
            <th>Occupancy</th>
            <th>Category</th>
            <th>Item</th>
            <th>Unit Cost</th>
            <th>Status</th>
            <th>Tender</th>
        </tr>
        </thead>
        <tbody>
        @foreach($sales as $sale)
            <tr>
                <td>{{ $sale->roomsales_id }}</td>
                <td>{{ $sale->sales_date }}</td>
                <td>{{ $sale->occupancy_id }}</td>
                <td>{{ $sale->sas_cat_name }}</td>
                <td>{{ $sale->sas_description }}</td>
                <td>{{ $sale->amount }}</td>
                <td>{{ $sale->status }}</td>
                <td>{{ $sale->tendertype }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif