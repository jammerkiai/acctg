@extends('layouts.base')
@section('content')
<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Sales
						</h3>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="row-fluid form-horizontal">
						<div class="span4">
							<div class="control-group">
							  <label class="control-label">Date from:</label>
							  <div class="controls">
									 <input type="date" class="span10" name="dfr"
											 value="{{ $dfr }}"/>
							  </div>
							</div>
						</div>
						<div class="span4">
							<div class="control-group">
							  <label class="control-label">Date to:</label>
							  <div class="controls">
									 <input type="date" class="span10" name="dto" value="{{ $dto }}"/>
							  </div>
							</div>
						</div>
						<div class="span4">
							<input type="submit" class="btn blue" value="Submit" />
							<a href="{{ url('sales/import') }}" class="btn green pull-right">
								Import Sales Data
							</a>
						</div>
					</div>
					<div class="portlet box green">
						<div class="portlet-title">
							<h4><i class="icon-money"></i> Cash Sales</h4>
						</div>
						<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>GL Account No.</th>
										<th class="jfp_align_right">Total</th>
										<th>{{ number_format($total, 2) }}</th>
									</tr>
								</thead>
								<tbody>
								@foreach($accts as $acct)
									<tr class="odd gradeX">
										<td>{{$acct->code}}</td>
										<td><b>{{$acct->name}}</b></td>
										<td><a href="{{url('ledger/level3', ['subclass'=>$acct->name, 'dfr' => $dfr, 'dto' => $dto])}}">{{$acct->amt}}</a></td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<div class="portlet box blue">
						<div class="portlet-title">
							<h4><i class="icon-money"></i> Credit Card Sales</h4>
						</div>
						<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
								<tr>
									<th>GL Account No.</th>
									<th class="jfp_align_right">Total</th>
									<th>{{ number_format($total, 2) }}</th>
								</tr>
								</thead>
								<tbody>
								@foreach($accts as $acct)
									<tr class="odd gradeX">
										<td>{{$acct->code}}</td>
										<td><b>{{$acct->name}}</b></td>
										<td><a href="{{url('ledger/level3', ['subclass'=>$acct->name, 'dfr' => $dfr, 'dto' => $dto])}}">{{$acct->amt}}</a></td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	
@endsection