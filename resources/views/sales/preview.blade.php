@extends('layouts.base')
@section('content')
    <style>
        .bookL {
            color: #000;
        }
        .bookM {
            color: #FF00FF;
        }
    </style>
    <div class="page-content">
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        Sales - Pre-Posting
                    </h3>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>

            <div class="row-fluid form-horizontal">
                <div class="span8">
                    <div class="control-group">
                        <label class="control-label">Posting Date:</label>
                        <div class="controls">

                            <form method="GET" name="pdateform" action="{{ url('sales/import') }}">
                                <input type="date" class="span4" name="pdate" id="pdate" value="{{ $pdate }}"/>
                                <input type="submit" value="Refresh Preview" name="action" class="btn green"/>
                            </form>

                        </div>
                    </div>
                </div>

                <form method="POST" action="{{url('sales')}}">
                {!! csrf_field() !!}
                <div class="span12">
                    <input type="submit" class="btn pull-right red" value="Post Sales">
                    <input type="hidden" name="pdate"  value="{{ $pdate }}">
                </div>
                @if (count($totals))
                    @foreach($totals as $tender => $tenderTotals)
                    @if( $tenderTotals['total'] > 0)
                    
                    <table class="table">
                        <thead>
                        <caption><h4>{{$tender}}  ---  <strong>Total: {{$tenderTotals['total']}}</strong></h4></caption>
                        <tr>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Amount</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($tenderTotals['data'] as $total)
                            <tr class="book">
                                <td>
                                    <input type="text" name="credit[]" value="{{$total->code}}">
                                </td>
                                <td>
                                    {{$total->name}}
                                </td>
                                <td>
                                    <input type="text" name="amount[]" value="{{$total->amount}}"/></td>
                                <td>
                                    
                                    <a class="btn blue" target="_blank"
                                        href="{{url('sales/details')}}?pdate={{$pdate}}&code={{$total->code}}">
                                    View Details
                                    </a>

                                    <input type="hidden" name="debit[]" value="{{$tenderTotals['debit']}}"/></td>
                                    <input type="hidden" name="book[]" value="{{$tenderTotals['book']}}"/></td>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    

                    @endif
                    @endforeach
                @endif

                </form>
            </div>
        </div>
    </div>
@endsection
@section('customjs')
<script>
$(document).ready(function(){
    $('.btnPost').on('click', function(e){

    });  
});
</script>
@endsection