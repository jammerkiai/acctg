@extends('layouts.base')
@section('content')
        <!-- BEGIN PAGE -->
<form method="post" action="{{ url('assetmanagement/store') }}">
    {!! csrf_field() !!}
<div class="page-content">
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Add New Asset
                </h3>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="form-horizontal">
            <div class="control-group">
                <label class="control-label">Asset Type</label>
                <div class="controls">
                    <select class="span6 " data-placeholder="Choose a Category" tabindex="1" name="code">
                        <option>Select...</option>
                        @foreach($coa as $c)
                            <option value="{{ $c->code }}"
                            @if(isset($itm->code) && $c->code == $itm->code) selected @endif >
                                {{ $c->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Asset Name</label>
                <div class="controls">
                    <input type="text" class="span6" name="name" value="{{ $itm->particular or '' }}"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Purchase Amount</label>
                <div class="controls">
                    <input type="text" class="span6" name="purchase_amount" id="purchase_amount" value="{{ $itm->price or 0 }}"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Useful Life</label>
                <div class="controls">
                    <input type="number" min="1" class="span3" placeholder="Number" id="useful_life" name="useful_life" value="1"/>
                    <select class="span3" data-placeholder="Choose a Category"
                            tabindex="1" name="unit_interval" id="unit_interval">
                        <option value="year">Year(s)</option>
                        <option value="month">Month(s)</option>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Purchase Date</label>
                <div class="controls">
                    <input type="date" name="purchase_date" id="purchase_date" value="{!! date('Y-m-d') !!}"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Start of Depreciation</label>
                <div class="controls asset_view_label">
                    <input type="date" name="start_depreciation" id="start_dep" value="{!! date('Y-m-d') !!}" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Depreciation Expense Per Month</label>
                <div class="controls">
                    <input type="text" class="span6"  name="monthly_depreciation" id="monthly_depreciation"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Remarks</label>
                <div class="controls">
                    <textarea class="span6" rows="3" name="remarks"></textarea>
                </div>
            </div>
            <div class="jfp_center"><input type="submit" class="btn green" value="Submit"></div>
        </div>
        <br>
        <!-- END PAGE CONTENT-->
    </div>
    <!-- END PAGE CONTAINER-->
</div>

</form>
<!-- END PAGE -->
@endsection
@section('customjs')
    <script>
        $(document).ready(function(){
            function recompute() {
                var unit = $('#unit_interval').val(),
                    life = $('#useful_life').val() || 0,
                    monthly = $('#monthly_depreciation').val() || 0,
                    cost = $('#purchase_amount').val() || 0;

                if (unit === 'year') {
                    monthly = cost / (life * 12);
                } else {
                    monthly = cost/life;
                }

                $("#monthly_depreciation").val(monthly.toFixed(2));

//                var start = Date.parse($('#start_depreciation').val());
//                    console.log(start);
            }

            $('#unit_interval, #useful_life, #purchase_amount').on('change', function(e) {
                e.preventDefault();
                recompute();
            });
        });
    </script>
@endsection