@extends('layouts.base')
@section('content')
        <!-- BEGIN PAGE -->
<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div id="portlet-config" class="modal hide">
        <div class="modal-header">
            <button data-dismiss="modal" class="close" type="button"></button>
            <h3>portlet Settings</h3>
        </div>
        <div class="modal-body">
            <p>Here will be a configuration form</p>
        </div>
    </div>
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Assets Management
                </h3>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>

        @if(Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif
        <div>
            <a href="{{ url('assetmanagement/create') }}"><button class="btn blue">
                    Add New <i class="icon-plus"></i>
                </button></a>
            <button class="btn green">Run Depreciation</button>
        </div>
        <div class="row-fluid marg_top_20">
            <div class="span12 ">
                <div class="form-horizontal">
                    <div class="control-group">
                        <label class="control-label">Select Asset</label>
                        <div class="controls">
                            <form method="get" action="{{ url('assetmanagement') }}">
                            <select class="medium" tabindex="1" name="code">
                                <option>Select...</option>
                                @foreach($coa as $c)
                                    <option value="{{ $c->code }}"
                                            @if(count($assets) > 0 && $c->code == $assets[0]->code) selected @endif>
                                        {{ $c->name }}
                                    </option>
                                @endforeach
                            </select>
                            <button type="submit" class="btn blue">Go</button>
                            </form>
                        </div>
                    </div>
                </div>
                @if (count($assets) > 0)
                <div class="portlet box green">
                    <div class="portlet-title">
                        <h4>{{ $assets[0]->coa->name }} </h4>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                            <tr>
                                <th>Asset Name</th>
                                <th>Amount</th>
                                <th>Useful Life</th>
                                <th>Purchase Date</th>
                                <th>Depreciation Expense</th>
                                <th>Remarks</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($assets as $asset)
                            <tr class="odd gradeX">
                                <td>{{$asset->name}}</td>
                                <td>{{$asset->purchase_amount}}</td>
                                <td>{{$asset->useful_life}} {{$asset->unit_interval}}</td>
                                <td>{{$asset->purchase_date}}</td>
                                <td>{{$asset->monthly_depreciation}}</td>
                                <td>{{$asset->remarks}}</td>
                                <td class="center hidden-480">
                                    <a href="{{ url('assetmanagement', [$asset->id]) }}">View</a> | Delete
                                </td>
                            </tr>
                            @empty
                                <div>No assets selected for viewing</div>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                @endif
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
    <!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->

@endsection