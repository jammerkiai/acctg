@extends('layouts.base')
@section('content')
        <!-- BEGIN PAGE -->
<form method="post" action="{{ url('assetmanagement/store') }}">
    {!! csrf_field() !!}
    <input type="hidden" name="id" id="am_id" value="{{ $am->id }}" />
    <div class="page-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        Asset Details
                    </h3>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="form-horizontal">
                <div class="control-group">
                    <label class="control-label">Asset Type</label>
                    <div class="controls">
                        <select class="span6 " data-placeholder="Choose a Category" tabindex="1" name="code">
                            <option>Select...</option>
                            @foreach($coa as $c)
                                <option value="{{ $c->code }}"
                                        @if($c->code == $am->code) selected @endif>{{ $c->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Asset Name</label>
                    <div class="controls">
                        <input type="text" class="span6" name="name" value="{{$am->name}}"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Purchase Amount</label>
                    <div class="controls">
                        <input type="text" class="span6" name="purchase_amount" id="purchase_amount" value="{{$am->purchase_amount}}"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Useful Life</label>
                    <div class="controls">
                        <input type="text" class="span3" placeholder="Number" name="useful_life" value="{{$am->useful_life}}"/>
                        <select class="span3" data-placeholder="Choose a Category"
                                tabindex="1" name="unit_interval" id="unit_interval">
                            <option value="year" @if($am->unit_interval=='year') selected @endif>Year(s)</option>
                            <option value="month" @if($am->unit_interval=='month') selected @endif>Month(s)</option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Purchase Date</label>
                    <div class="controls asset_view_label">
                        <input type="date" name="purchase_date" id="end_dep" value="{{ $am->purchase_date }}"" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Start Of Depreciation</label>
                    <div class="controls">
                        <input type="date" name="start_depreciation" id="start_dep" value="{{ $am->start_depreciation }}"/>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Depreciation Expense Per Month</label>
                    <div class="controls">
                        <input type="text" class="span6"  name="monthly_depreciation"
                               id="monthly_depreciation" value="{{ $am->monthly_depreciation }}"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Remarks</label>
                    <div class="controls">
                        <textarea class="span6" rows="3" name="remarks">{{$am->remarks}}</textarea>
                    </div>
                </div>
                <div class="jfp_center"><input type="submit" class="btn green" value="Update"></div>
            </div>
            <br>
            <h3>Depreciation Schedule</h3>

            <table class="table table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Date To Post</th>
                    <th>Remaining Value</th>
                </tr>
            </thead>
            <tbody>
                
                @foreach($am->schedule as $sched)
                <tr>
                    <td>{{ $sched->interval_count }}</td>
                    <td>{{ $sched->entry_date }}</td>
                    <td>{{ $sched->remaining_value }}</td>
                </tr>
                @endforeach
            </tbody>
            </table>
            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>

</form>
<!-- END PAGE -->
@endsection
