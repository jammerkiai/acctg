@extends('layouts.base')
@section('content')
<form method="post" action="{{ url('coa') }}">
    {!! csrf_field() !!}
<div class="page-content">
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Add New Account
                </h3>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="form-horizontal">
            <div class="control-group">
                <label class="control-label">Code</label>
                <div class="controls">
                	<input type="text" class="span6" name="code" value=""/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Name / Description</label>
                <div class="controls">
                    <input type="text" class="span6" name="name" value=""/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Root</label>
                <div class="controls">
                    <input type="text" class="span6" name="root" id="root" value=""/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Classification</label>
                <div class="controls">
                    <input type="text" class="span6" name="classification" id="classification" value=""/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Sub-classification</label>
                <div class="controls">
                    <input type="text" class="span6" name="subclass" id="subclass" value=""/>
                </div>
            </div>
            <div class="jfp_center"><input type="submit" class="btn green" value="Submit"></div>
        </div>
        <br>
        <!-- END PAGE CONTENT-->
    </div>
    <!-- END PAGE CONTAINER-->
</div>

</form>
@endsection