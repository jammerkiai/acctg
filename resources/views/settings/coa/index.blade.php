@extends('layouts.base')
@section('content')
    <div class="page-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        Chart of Accounts
                    </h3>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            @if (session()->has('message')) 
            <div class="alert alert-success">{{ session()->get('message') }}</div>
            @endif
            <!-- END PAGE HEADER-->
            <a href="{{ url('coa/create') }}" class="btn green">Add Account</a>
            <table class="table">
            <thead>
                <tr>
                    <th>Code</th>
                    <th>Description</th>
                    <th>Root</th>
                    <th>Classification</th>
                    <th>Sub-classification</th>
                </tr>
            </thead>
            
            <tbody>
            @foreach ($coa as $c) 
                <tr>
                    <td>{{ $c->code }}</td>
                    <td>{{ $c->name }}</td>
                    <td>{{ $c->root }}</td>
                    <td>{{ $c->classification }}</td>
                    <td>{{ $c->subclass }}</td>
                </tr>
            @endforeach
            </tbody>
            </table>
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
@endsection