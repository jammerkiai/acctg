@extends('layouts.base')
@section('content')
    <div class="page-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        Settings
                    </h3>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="tiles">
                <a href="{{ url('assetmanagement') }}">
                    <div class="tile bg-blue selected">
                        <div class="corner"></div>
                        <div class="tile-body">
                            <i class="icon-cog"></i>
                        </div>
                        <div class="tile-object">
                            <div class="name">
                                Assets Management
                            </div>
                            <div class="number">
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="tiles">
                <a href="{{ url('settings/settings') }}">
                    <div class="tile bg-red selected">
                        <div class="corner"></div>
                        <div class="tile-body">
                            <i class="icon-number icon-plus-sign-alt"></i>
                        </div>
                        <div class="tile-object">
                            <div class="name">Counter Settings
                            </div>
                            <div class="number">
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="tiles">
                <a href="{{ url('settings/glinit') }}">
                    <div class="tile bg-green selected">
                        <div class="corner"></div>
                        <div class="tile-body">
                            <i class="icon-number icon-file"></i>
                        </div>
                        <div class="tile-object">
                            <div class="name">Data Initialization
                            </div>
                            <div class="number">
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="tiles">
                <a href="{{ url('settings/users') }}">
                    <div class="tile bg-yellow selected">
                        <div class="corner"></div>
                        <div class="tile-body">
                            <i class="icon-number icon-group"></i>
                        </div>
                        <div class="tile-object">
                            <div class="name">Manage Users
                            </div>
                            <div class="number">
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="tiles">
                <a href="{{ url('settings/purchase-units') }}">
                    <div class="tile bg-purple selected">
                        <div class="corner"></div>
                        <div class="tile-body">
                            <i class="icon-number icon-cog"></i>
                        </div>
                        <div class="tile-object">
                            <div class="name">Manage Purchase Units
                            </div>
                            <div class="number">
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="tiles">
                <a href="{{ url('coa') }}">
                    <div class="tile bg-blue selected">
                        <div class="corner"></div>
                        <div class="tile-body">
                            <i class="icon-number icon-list"></i>
                        </div>
                        <div class="tile-object">
                            <div class="name">Chart of Accounts
                            </div>
                            <div class="number">
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <br>
            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
@endsection