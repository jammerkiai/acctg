@extends('layouts.base')
@section('content')
    <div class="page-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        Data Initialization
                    </h3>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <div class="alert alert-danger">
                <span class="icon icon-arrow-right"></span> Warning! Submitting this form will erase all records!
            </div>
            @if(Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif

            <!-- END PAGE HEADER-->
            <form method="post" action="{{ url('settings/glinit') }}" >
                {!! csrf_field() !!}


                <div>
                    Posting Date: <input type="date" name="postdate" value="{!! date('Y-m-d') !!}"/>
                    Type: <input type="checkbox" checked name="type" value="l">
                    <input type="submit" value="Submit" name="act" class="btn green"/>
                    <input type="submit" value="Start Over" name="act" class="btn red  pull-right" />
                </div>

                <div>

                </div>

                <div>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Debit</th>
                            <th>Credit</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($coa as $c)
                            <tr>
                                <td>
                                    {{ $c->code }}
                                    <input type="hidden" name="code[]" value="{{$c->code}}" />
                                </td>
                                <td>{{ $c->name }}</td>
                                <td><input type="text" name="debit[]" value="0" /></td>
                                <td><input type="text" name="credit[]" value="0" /></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div>
                    <input type="submit" value="Submit" name="act" class="btn green pull-right"/>
                </div>
            </form>
            <br />
            <br />
            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
@endsection
@section('customjs')
    <script>
        $(document).ready(function(){
            $('.currentValue').on('change', function(e){
                e.preventDefault();
                var url = "{{ url('settings/setValue') }}";
                $.post(
                        url,
                        {
                            id: $(this).data('id'),
                            counter: $(this).val()
                        },
                        function(resp) {
                            alert(resp.message);
                        }
                );
            });
        });
    </script>
@endsection