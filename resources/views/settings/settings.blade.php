@extends('layouts.base')
@section('content')
    <div class="page-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        Counters
                    </h3>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <h3>Available Counters</h3>
            <div>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Current Value</th>
                        <th>Type</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($settings as $set)
                    <tr>
                        <td>{{ $set->id }}</td>
                        <td>{{ $set->name }}</td>
                        <td><input type="text"
                                   value="@if($set->type=='counter'){!! number_format($set->counter, 0, '', '') !!}@else{{ $set->counter }}@endif"
                                   name="currentValue" class="currentValue"
                                   data-id="{{$set->id}}" /></td>
                        <td>{{ $set->type }}</td>
                    </tr>
                    @empty
                        <tr><td colspan="4">No counters found</td></tr>
                    @endforelse
                    </tbody>
                </table>
            </div>

            <hr />
            @if(Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif
            <h3>Add a new counter</h3>
            <form method="post" action="{{ url('settings/store') }}">
                {!! csrf_field() !!}
                <div class="form form-horizontal">
                    <div class="control-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" />
                    </div>
                    <div class="control-group">
                        <label for="counter">Start Value</label>
                        <input type="text" name="counter" id="counter" />
                    </div>
                    <div class="control-group">
                        <label for="counter">Type</label>
                        <select name="type">
                            <option value="constant">constant</option>
                            <option value="counter">counter</option>
                        </select>
                    </div>
                </div>
                <div>
                    <input type="submit" name="submitCtr" value="Submit" class="btn green"/>
                </div>
            </form>
            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
@endsection
@section('customjs')
<script>
    $(document).ready(function(){
        $('.currentValue').on('change', function(e){
            e.preventDefault();
            var url = "{{ url('settings/setValue') }}";
            $.post(
                url,
                {
                    id: $(this).data('id'),
                    counter: $(this).val()
                },
                function(resp) {
                    alert(resp.message);
                }
            );
        });
    });
</script>
@endsection