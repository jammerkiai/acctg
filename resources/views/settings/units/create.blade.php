@extends('layouts.base')
@section('content')
        <!-- BEGIN PAGE -->
<div class="page-content">
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <br/>
        <a href="{{ url('/settings/purchase-units') }}" class="btn btn-primary blue">
            <i class="icon-arrow-left"></i> Show Units
        </a>
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Manage Purchase Units
                </h3>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
            <div class="span12">
                   @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet box yellow">
                        <div class="portlet-title">
                            <h4><i class="icon-list-ul"></i>Add Unit Form</h4>
                        </div>
                        @if( session()->has('success') )
                            <div class="alert alert-success">
                                <strong>Success!</strong> {{ session('success')['message'] }}
                            </div>
                        @endif
                        <form id="add_user_form" method="POST" action="{{ url('/settings/purchase-unit/create') }}">
                            <div class="portlet-body">
                                <div class="row-fluid">
                                    <div class="span12">
                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                                        </div>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-default">Add</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <br>
        <!-- END PAGE CONTENT-->
    </div>
    <!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
@endsection