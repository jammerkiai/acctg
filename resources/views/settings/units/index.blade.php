@extends('layouts.base')
@section('content')
        <!-- BEGIN PAGE -->
<div class="page-content">
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Manage Purchase Units
                </h3>
                @if( session()->has('success') )
                    <div class="alert alert-success">
                        <strong>Success!</strong> {{ session('success')['message'] }}
                    </div>
                @endif
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box yellow">
                    <div class="portlet-title">
                        <h4><i class="icon-group"></i>Purchase Units</h4>
                    </div>
                    <div class="portlet-body">
                        <div class="clearfix">
                            <div class="btn-group">
                                <a href="{{ url('/settings/purchase-unit/create/form') }}"><button id="add_user_button" class="btn green">
                                    Add New Unit <i class="icon-plus"></i>
                                </button></a>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>name</th>
                                <th >Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($units as $unit)
                                <tr class="odd gradeX">
                                    <td>{{ $unit->id }}</td>
                                    <td>{{ $unit->name }}</td>
                                    <td class="center hidden-480">
                                        <a href="{{ url('/settings/purchase-unit/update/form/'.$unit->id) }}">Edit</a> |
                                        <a href="{{ url('/settings/purchase-unit/delete/'.$unit->id) }}">Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <br>
        <!-- END PAGE CONTENT-->
    </div>
    <!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
</div>
@endsection