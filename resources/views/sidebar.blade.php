<!-- BEGIN SIDEBAR -->
<div class="page-sidebar nav-collapse collapse">
    <!-- BEGIN SIDEBAR MENU -->         
    <ul>
        <li>
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <div class="sidebar-toggler hidden-phone"></div>
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        </li>
        <li class="hide">
            <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
            <form class="sidebar-search">
                <div class="input-box">
                    <a href="javascript:;" class="remove"></a>
                    <input type="text" placeholder="Search..." />               
                    <input type="button" class="submit" value=" " />
                </div>
            </form>
            <!-- END RESPONSIVE QUICK SEARCH FORM -->
        </li>
        <li class="start ">
            <a href="/home">
            <i class="icon-home"></i> 
            <span class="title">Dashboard</span>
            </a>
        </li>
        <li>
            <a href="/purchasing">
            <i class="icon-shopping-cart"></i> 
            <span class="title">Purchase</span>
            </a>
        </li>
        <li>
            <a href="/disbursement">
            <i class="icon-credit-card"></i> 
            <span class="title">Disbursement</span>
            </a>
        </li>
        <li>
            <a href="/releasing">
            <i class="icon-ok-sign"></i> 
            <span class="title">Cheque Releasing</span>
            </a>
        </li>
        <li>
            <a href="/sales">
            <i class="icon-money"></i> 
            <span class="title">Sales</span>
            </a>
        </li>
        <li>
            <a href="/journal">
            <i class="icon-list-alt"></i> 
            <span class="title">General Journal</span>
            </a>
        </li>
        <li>
            <a href="/ledger">
            <i class="icon-list-alt"></i> 
            <span class="title">General Ledger</span>
            </a>
        </li>
        @if(  Auth::user()->hasRole('Root') or Auth::user()->hasRole('Administrator') or Auth::user()->hasRole('Manager') )
        <li>
            <a href="/reports">
            <i class="icon-user"></i> 
            <span class="title">Admin</span>
            </a>
        </li>
        <li>
            <a href="/settings">
            <i class="icon-cogs"></i> 
            <span class="title">Settings</span>
            </a>
        </li>
        @endif
    </ul>
    <!-- END SIDEBAR MENU -->
</div>
<!-- END SIDEBAR -->