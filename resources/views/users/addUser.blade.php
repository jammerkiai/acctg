@extends('layouts.base')
@section('content')
    <!-- BEGIN PAGE -->
    <div class="page-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        Manage Users
                    </h3>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet box yellow">
                        <div class="portlet-title">
                            <h4><i class="icon-list-ul"></i>Add User Form</h4>
                        </div>
                        <form id="add_user_form" method="POST" action="{{ url('/settings/user/create') }}">
                            <div class="portlet-body">
                                <div class="row-fluid">
                                    <div class="span12">
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <label for="role">Role</label>
                                <div class="checkbox" style="margin-left:20px;">
                                    <label>
                                        <input type="checkbox" name="roles[]" value="Root"> Root
                                    </labeL>
                                </div>
                                <div class="checkbox" style="margin-left:20px;">
                                    <label>
                                        <input type="checkbox" name="roles[]" value="Administrator"> Administrator
                                    </labeL>
                                </div>
                                <div class="checkbox" style="margin-left:20px;">
                                    <label>
                                        <input type="checkbox" name="roles[]" value="Manager"> Manager
                                    </labeL>
                                </div>
                                <div class="checkbox" style="margin-left:20px;">
                                    <label>
                                        <input type="checkbox" name="roles[]" value="Supervisor"> Supervisor
                                    </labeL>
                                </div>
                                <div class="checkbox" style="margin-left:20px;">
                                    <label>
                                        <input type="checkbox" name="roles[]" value="User"> User
                                    </labeL>
                                </div>
                            </div>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button type="submit" class="btn btn-default">Submit</button>
                                        </div>
                                    </div>
                            </div>
                        </form>
                    </div>

                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
            <br>
            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
@endsection