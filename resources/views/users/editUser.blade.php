@extends('layouts.base')
@section('content')
    <!-- BEGIN PAGE -->
    <div class="page-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        Manage Users
                    </h3>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12">
                        @if( !Auth::user()->hasRole('Root') or !Auth::user()->hasRole('Administrator') )
                            <div class="alert alert-warning">
                                <strong>Note!</strong> Only Root and Administrators can edit user information.
                            </div>
                        @endif
                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet box yellow">
                            <div class="portlet-title">
                                <h4><i class="icon-list-ul"></i>User Information</h4>
                            </div>

                                <div class="portlet-body">
                                    <div class="row-fluid">
                                        <div class="span12">
                                            <form id="add_user_form" method="POST" action="{{ url('/settings/user/assign/role') }}">
                                                <div class="form-group">
                                                    <label for="email"><b>Email address</b></label>
                                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{ $user->email }}" @if( !Auth::user()->hasRole('Root') or !Auth::user()->hasRole('Administrator') ) disabled @endif>
                                                </div>
                                                <div class="form-group">
                                                    <label for="name"><b>Name</b></label>
                                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ $user->name }}" @if( !Auth::user()->hasRole('Root') or !Auth::user()->hasRole('Administrator') ) disabled @endif>
                                                </div>
                                                <div class="form-group">
                                                <label for="name"><b>Roles</b></label>
                                                @foreach($roleList as $item)

                                                    @if(in_array($item, $userRoles))

                                                        <div class="checkbox" style="margin-left:20px;">
                                                            <label>
                                                            <input type="checkbox" name="roles[]" checked value="{{$item}}" @if( !Auth::user()->hasRole('Root') or !Auth::user()->hasRole('Administrator') ) disabled @endif> {{ $item }}
                                                            </labeL>
                                                        </div>

                                                    @else

                                                        <div class="checkbox" style="margin-left:20px;">
                                                            <labeL>
                                                            <input type="checkbox" name="roles[]" value="{{$item}}" @if( !Auth::user()->hasRole('Root') or !Auth::user()->hasRole('Administrator') ) disabled @endif> {{ $item }}
                                                            </labeL>
                                                        </div>

                                                    @endif

                                                @endforeach
                                                </div>
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" name="user" value="{{ $user->id }}">
                                                @if(  Auth::user()->hasRole('Root') or Auth::user()->hasRole('Administrator') )
                                                <button type="submit" class="btn btn-default">Update</button>
                                                @endif
                                            </form>
                                        </div>
                                    </div>
                                </div>
                        </div>

                        <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
            <br>
            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
@endsection