@extends('layouts.base')
@section('content')
    <!-- BEGIN PAGE -->
    <div class="page-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        Manage Users
                    </h3>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet box yellow">
                        <div class="portlet-title">
                            <h4><i class="icon-group"></i>Users</h4>
                        </div>
                        <div class="portlet-body">
                            <div class="clearfix">
                                <div class="btn-group">
                                    <a href="{{ url('/settings/user/create/form') }}"<button id="add_user_button" class="btn green">
                                            Add New User <i class="icon-plus"></i>
                                        </button></a>
                                </div>
                            </div>
                            <table class="table table-striped table-bordered table-hover" id="sample_1">
                                <thead>
                                <tr>
                                    <th>User Number</th>
                                    <th>Name</th>
                                    <th class="hidden-480">Email</th>
                                    <th class="hidden-480">Role</th>
                                    <th class="hidden-480">Date Added</th>
                                    <th >Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                <tr class="odd gradeX">
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td class="hidden-480">{{ $user->email }}</td>
                                    <td class="hidden-480">
                                        @foreach($user->roles as $role)
                                            {{ $role->name }},
                                        @endforeach
                                    </td>
                                    <td ><span class="label label-success">{{ $user->created_at }}</span></td>
                                    <td class="center hidden-480"><a href="{{ url('/settings/user/'.$user->id.'/assign/role') }}">View/Edit</a> | Delete</td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
            <br>
            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
    </div>
@endsection